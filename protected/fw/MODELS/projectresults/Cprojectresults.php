<?php

class Cprojectresults {

    var $C;               //main object
    var $lang;
    var $lang2;

    var $DB;


    private function renderProject($id) {

    }

    private function renderPage() {

    }

    public function save_result_title()            {

        $title = $_POST['title_en'];
        $id    = $_POST['BLOCK_id'];

        $query = "UPDATE results SET projectName = '{$title}' where topicID = '{$id}' ";
        $this->DB->query($query);
        header("Location: ".$_SERVER['REQUEST_URI']);
        exit();
    }

    public function save_result_desc()            {

        $desc = $_POST['descriptionTXT_en'];
        $id   = $_POST['BLOCK_id'];

        $query = "UPDATE results SET description = '{$desc}' where topicID = '{$id}' ";
        $this->DB->query($query);
        header("Location: ".$_SERVER['REQUEST_URI']);
        exit();
    }

    public function save_result_details()            {

        $fsLicense    = $_POST['fsLicense_en'];
        $odLicense    = $_POST['odLicense_en'];
        $demoURL      = $_POST['demoURL_en'];
        $vcsURL       = $_POST['vcsURL_en'];
        $technologies = $_POST['technologies_en'];
        $archiveURL   = $_POST['archiveURL_en'];
        $id   = $_POST['BLOCK_id'];

        $query = "UPDATE results SET
                        fsLicense = '{$fsLicense}',
                        odLicense = '{$odLicense}',
                        demoURL = '{$demoURL}',
                        vcsURL = '{$vcsURL}',
                        archiveURL = '{$archiveURL}',
                        technologies = '{$technologies}'
                    where topicID = '{$id}' ";
        $this->DB->query($query);
        header("Location: ".$_SERVER['REQUEST_URI']);
        exit();
    }

    function DISPLAY() {
        $display = "";

        $result = $this->DB->query("SELECT results.*, 
                                           topics.description as descr, 
                                           topics.title as title
                                    FROM results
                                    LEFT JOIN topics
                                        ON (topics.id = results.topicID)
                                    LEFT JOIN teams
                                        ON (results.topicID = teams.id)
                                    ORDER BY results.active DESC, results.place ASC, resultID ASC;
        ");

        $separator = "<hr class='separator' /><p class='undeveloped'>Undeveloped projects: </p>";
        $align = array ( 1 => 'project-left',
                         2 => 'project-right');
        $i = 2;

        while ($row = $result->fetch_object()) {
            $activity = ($row->active == 1 ? 'activeProject' : 'inactiveProject');


            $participants = $this->DB->query(
                          "SELECT * FROM members WHERE team = '".$row->topicID."' ORDER BY role ASC");
            $title = ( $row->projectName == NULL ? $row->title : $row->projectName );
            $description = ( $row->description == NULL ? $row->descr : $row->description );
            $team = '';
            $place = $row->place;
            $fsLicenseHref = (strlen($row->fsLicenseURL) > 0 ? "href='".$row->fsLicenseURL."'" : '');
            $odLicenseHref = (strlen($row->odLicenseURL) > 0 ? "href='".$row->odLicenseURL."'" : '');
            $vcsHref = (strlen($row->vcsURL) > 0 ? "href='".$row->vcsURL."'" : '');
            $demoHref = (strlen($row->demoURL) > 0 ? "href='".$row->demoURL."'" : '');

                while ($p = $participants->fetch_object()) {
                    if($p->id != $row->topicID)
                        $team .= "<tr><td>".$p->name." (<i>".$p->role."</i>)</td></tr>";
                    else {
                        $teamLeader = $p->name." (<i>".$p->role."</i>)";
                        $team .= "<tr><td><u>".$p->name."</u> (<i>".$p->role."</i>)</td></tr>";
                    }
                }

            if($row->active == 0) {
                $display .= $separator;
                $separator = '';
            }
            $display .= "
                        <div class='result-wrapper $activity'>
                        <div class='ENT titlewrapper' id='titlewrapper_".$row->topicID."_en'>
                            <div class='EDtxt title'>" . $title . " </div>
                        </div>
                        <div class='top-$place'></div>
                        <div class='project-result ".$align[$i]."'>
                        <div class='ENT resultsummary' id='resultsummary_".$row->topicID."_en'>
                            <!-- <span class='description-toggle'>Description (click to toggle)</span> -->
                            <div class='description'>
                                <div class='EDeditor descriptionTXT'>
                                    ".$description."
                                </div>
                                <span class='team-leader'>Team Leader: $teamLeader</span>
                                <span class='click-to-expand' title='".$row->topicID."'><i>Click to expand</i></span><br/>
                            </div>
                        </div>
                        <div class='project-result-details' id='prd".$row->topicID."'>
                            <div class='ENT detailsTable' id='detailsTable_".$row->topicID."_en'>
                                <table class='project-result-details-table'>
                                    <tr>
                                        <td>Free software license</td>
                                        <td><div class='EDtxt fsLicense'><a $fsLicenseHref>".$row->fsLicense."</a></div></td>
                                    </tr>
                                    <tr>
                                        <td>Open data license</td>
                                        <td class='EDtxt odLicense'><a $odLicenseHref>".$row->odLicense."</a></td>
                                    </tr>
                                    <tr>
                                        <td>Online demo</td>
                                        <td class='EDtxt demoURL'><a $demoHref>".$row->demoURL."</a></td>
                                    </tr>
                                    <tr>
                                        <td>Source code</td>
                                        <td class='EDtxt vcsURL'><a $vcsHref>".$row->vcsURL."</a></td>
                                    </tr>
                                    <tr>
                                        <td>Technologies used</td>
                                        <td class='EDtxt technologies'>$row->technologies</td>
                                    </tr>
                                    <tr>
                                        <td>Archive</td>
                                        <td class='EDtxt archiveURL'>$row->archiveURL</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <table class='project-result-details-participants'>
                                <thead><th>Team members</th></thead>$team</table>
                        </div></div></div>";
            $i = ( $i == 1 ? $i += 1 : $i -= 1 );
        }

        return $display;
    }


    function __construct($C)
    {
        $this->C = &$C;
        $this->DB = $this->C->DB;

        $this->lang = &$C->lang;
        $this->lang2 = &$C->lang2;

        if(isset($_POST['title_en']))           $this->save_result_title();
        if(isset($_POST['descriptionTXT_en']))  $this->save_result_desc();
        if(isset($_POST['save_detailsTable']))    $this->save_result_details();

        //if(isset($_POST)) var_dump($_POST);

        if(isset($_POST)) {
            unset($_POST);
        }



    }

}
