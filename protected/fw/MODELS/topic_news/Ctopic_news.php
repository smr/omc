<?php
class Ctopic_news
{
    var $topicID;
    var $C;
    var $DB;

    var $admin_tmpl;




    public function save_newsArt()            {

        $title   = $_POST['artTitle_en'];
        $content = $_POST['artContent_en'];
        $topic_ARTid = $_POST['BLOCK_id'];

        $query = "UPDATE topic_news SET title = '{$title}' , content = '{$content}' where id = '{$topic_ARTid}' ";
        $this->DB->query($query);
    }
    public function delete_newsArt()          {

        $topic_ARTid = $_POST['BLOCK_id'];
        $query = "DELETE from topic_news  where id = '{$topic_ARTid}' ";
        $this->DB->query($query);
    }
    public function save_addnewsArt()         {

        $title   = $_POST['artTitle_en'];
        $content = $_POST['artContentN_en'];

        $query = "INSERT into topic_news  (id_topic, title, content) values ('{$this->topicID}','{$title}','{$content}') ";
        $this->DB->query($query);
    }
    public function processPOST()             {

        $myPOST = false;
        if(isset($_POST['save_newsArt']))     { $this->save_newsArt();       $myPOST= true;  }
        if(isset($_POST['delete_newsArt']))   { $this->delete_newsArt();     $myPOST= true;  }
        if(isset($_POST['save_addnewsArt']))  { $this->save_addnewsArt();    $myPOST= true;  }

        if($myPOST)
        {
            unset($_POST);
            header("Location: {$_SERVER['REQUEST_URI']}");
            exit;
        }
    }
    public function DISPLAY()                 {

          # $topicID = $this->projectID;                                     #topic id

           $display = "<div class='allENTS admNews' id='admNews_en'>";
       #___________________________________________________________________________________________________________________
           if($this->admin_tml == true || isset($this->C->admin))                                        #daca sunt admin pe acest topic
           {
               $display .= "
                   <div class='ENT newsArt' id='newsArt_new_en' style='display: none;'>
                       <div class='EDtxt artTitle'> titlu</div>
                       <div class='EDeditor artContentN'> Continut</div>
                   </div>
               ";

           }
       #___________________________________________________________________________________________________________________

           $query = "SELECT id,title,content from topic_news where id_topic = '{$this->topicID}' ORDER BY id DESC ";
           $res = $this->DB->query($query);

           while($row = $res->fetch_assoc())
           {
               $id      = $row['id'];
               $title   = $row['title'];
               $content = $row['content'];
               $display .= "
                   <div class='ENT newsArt' id='newsArt_{$id}_en' >
                       <div class='EDtxt artTitle'> {$title}</div>
                       <div class='EDeditor artContent'> {$content}</div>
                   </div>
               ";
           }
       #___________________________________________________________________________________________________________________
            $display .= "</div>";
            return $display;//.'caut news-uri pentru proiectul cu id-ul '.$this->topicID.' topicul '.$_SESSION['topic'];


       }
    public function set_admin_tml()           {

          if( $_SESSION['topic'] && !$this->C->admin)
          {
              if(isset($_REQUEST['prj']) &&  $_SESSION['topic'] == $_REQUEST['prj'] )
              {
                  $this->admin_tml = true;
                  $this->C->SET_INC_jsCss('EDITmode','MODULES','/ADMIN');   # SET_INC_jsCss($mod_name,$type_MOD,$ADMINstr='')
              }

              $this->C->SET_general_mod('TOOLbar','MODULES','/ADMIN','AC');
          }

        if($this->admin_tml || $this->C->admin) $this->processPOST();
      }

    public function __construct($C)           {
        $this->C = &$C;
        $this->DB = $this->C->DB;
        $this->topicID = (isset($_REQUEST['prj']) ? $_REQUEST['prj'] : '');

        if(isset($_SESSION['admin_tml'])) $this->set_admin_tml(); #seteaza admin_tml si js/css pentru acest project


    }
}