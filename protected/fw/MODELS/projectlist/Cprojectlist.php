<?php

class Cprojectlist {

    var $C;               //main object
    var $lang;
    var $lang2;

    var $DB;


    function DISPLAY() {
        $display = "";

        $result = $this->DB->query("SELECT * FROM members
                                    RIGHT JOIN topics
                                        ON (members.id = topics.submitter)
                                    LEFT JOIN (
                                	    SELECT id, team, count(id) AS c
                                    	    FROM members GROUP BY members.team
                                    	)AS x
                                        ON (members.team = x.team)
                                    WHERE status IS NOT NULL
                                    ORDER BY status DESC, topics.id ASC;
        ");

        $display .= "<p class='subtitle'>Ideas submitted so far (click to expand):</p>";

        $i=0;
        while ($a = $result->fetch_assoc()) {
            $id     = $a['id'];
            $title  = $a['title'];
            $name   = $a['name'];
            $role   = $a['role'];
            $built  = $a['built'];
            $progress = $a['progress'];
            $c      = $a['c'];
            $date   = $a['date'];
            $description  = $a['description'];
            $missed = (intval(str_replace('-','',$a['date'])) <= 20120714 ? 'good' : 'bad');


            $count = "<span class='membercount'>Members: <b>$c</b></span>";
            $icons = ($missed == 'good' ?
                                "<a class='icon details' href='/entry-list/$id'></a>
                                  <a class='icon preview' onclick='javascript:tables($id);'></a>"
                              : "<a class='icon preview' onclick='javascript:tables($id);'></a>
                                  <span class='smallnotice'>You cannot join this team yet</span>"
                     );

            if($date > '2012-07-15') {
                $count = "";
            }

            if($id == 999) {

                $display .= "<div class='submission' id='{$id}_submission_en'>
                                <div class='submission_top $missed' style='background: #ddd;'>
                                      <div class='title'>$title</div>
                                      <hr  class='separator_small' />
                                      <b>
                                        <div class='name'>$name &ndash; Staff</div>
                                      </b><div class='role'></div>

                                        $icons


                                </div>
                                <table class='submission_table'><tr>
                                    <td style='width:100%; height: 200px;'>
                                            <p class='projectinfo'>Info:</p>
                                            $description
                                    </td>
                                </tr></table>
                            </div>
                            <hr class='h_separator' />\n";
            }
            else {
            $display .= "<div class='submission' id='{$id}_submission_en'>
                            <div class='submission_top $missed'>
                                  <div class='title'>$title</div>
                                  <hr  class='separator_small' />
                                  <b>
                                    <div class='name'>$name</div>
                                  </b> &ndash;
                                  <div class='role'>$role</div>
                                    $count
                                    $icons
                                   <div class='progressPROC'>
                                           $progress
                                           <span style='font-size: 17px;'>%</span>
                                   </div>

                            </div>
                            <table class='submission_table'><tr>
                                <td style='width: 50%;'>
                                        <div class='description'>
                                        <p class='projectinfo'>Topic description:</p>
                                        $description</div>
                                </td><td>
                                        <div class='built'>
                                        <p class='projectinfo'>What part of the project have you already built?</p>
                                        $built</div>
                                </td>
                            </tr></table>
                        </div>
                        <hr class='h_separator' />\n";
            }
            $i++;
        }

        return $display;
    }


    function __construct($C)
    {
        $this->C = &$C;
        $this->DB = $this->C->DB;

        $this->lang = &$C->lang;
        $this->lang2 = &$C->lang2;


    }

}