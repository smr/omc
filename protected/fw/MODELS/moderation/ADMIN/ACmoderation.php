<?php

class ACmoderation {

    var $C;               //main object
    var $lang;
    var $lang2;

    var $DB;

    var $unmoderated = 0;
    var $approved = 0;
    var $rejected = 0;

    function DISPLAY() {
        $display = "";

        $result = $this->DB->query("SELECT * FROM members
                                    JOIN topics
                                        ON (members.id = topics.submitter)
                                    ORDER BY topics.status ASC;
        ");

        $display .= "<div class='pageCont' id='en'>"
                    ."<div class='allENTS projects' id='projects_en'>";

        $i=0;
        while ($a = $result->fetch_assoc()) {
            switch($a['status']){
                case 0:
                    $statusicon = "<div class='EDsel unmoderated statusmod'>unmoderated</div>";
                    $this->unmoderated++;
                    break;
                case 1:
                    $statusicon = "<div class='EDsel approved statusmod'>approved</div>";
                    $this->approved++;
                    break;
                case 2:
                    $statusicon = "<div class='EDsel rejected statusmod'>rejected</div>";
                    $this->rejected++;
                    break;
            }
            $id     = $a['id'];
            $title  = $a['title'];
            $name   = $a['name'];
            $email  = $a['email'];
            $role   = $a['role'];
            $built  = $a['built'];
            $description  = $a['description'];

            $display .= "<div class='ENT submission' id='{$id}_submission_en'>
                            <div class='submission_top' onclick='javascript:tables($id);'>$statusicon
                                  <div class='EDtxt title'>$title</div>
                                  <hr  class='separator_small' />
                                  <b>
                                    <div class='EDtxt name'>$name</div>
                                  </b> &ndash;
                                  <div class='EDsel role'>$role</div>
                                   (<a> <em>
                                  <div class='EDtxt email'>$email</div>
                                  </em></a>)
                            </div>
                            <table class='submission_table'><tr>
                                <td style='width: 50%;'>
                                        <div class='EDeditor description'>$description</div>
                                </td><td>
                                        <div class='EDeditor built'>$built</div>
                                </td>
                            </tr></table>
                        </div>
                        <hr />\n";
            $i++;
        }
        $display .= "<p class='info'>Total submissions: "
                            .$result->num_rows
                            ." | Approved so far: "
                            . $this->approved
                            ." | Rejected: "
                            . $this->rejected
                            ."</p></div></div>";


        return $display;
    }

    private function saveData() {
        $BLOCK_id=$title_en=$name_en=$email_en=$role_en=$description_en=$statusmod_en=$built_en='';  //needed so the editor doesn't get too confused
        foreach ($_POST as $k=>$v) { $$k=$v; }

        switch ($statusmod_en) {
            case 'unmoderated': $statusmod_en=0; break;
            case 'approved': $statusmod_en=1; break;
            case 'rejected': $statusmod_en=2; break;
        }

        $this->DB->query("UPDATE members SET name='$name_en',
                                             email='$email_en'
                                         WHERE id='$BLOCK_id'");
        $this->DB->query("UPDATE topics  SET title = '$title_en',
                                             description = '$description_en',
                                             built = '$built_en',
                                             status = '$statusmod_en'
                                         WHERE submitter='$BLOCK_id'");

    }

    private function deleteData() {
        $BLOCK_id=$title_en=$name_en=$email_en=$role_en=$description_en=$built_en='';  //needed so the editor doesn't get too confused
        foreach ($_POST as $k=>$v) { $$k=$v; }
//        print "DELETE <br/>";
//        print_r($_POST);

        $this->DB->query("DELETE FROM members WHERE id='$BLOCK_id'");
        $this->DB->query("DELETE FROM topics WHERE submitter='$BLOCK_id'");

    }

    function __construct($C)
    {
        $this->C = &$C;
        $this->DB = $this->C->DB;

        $this->lang = &$C->lang;
        $this->lang2 = &$C->lang2;

        $this->C->TOOLbar->ADDbuttons("<a class='button' href='?type=moderation'>Moderation</a>");

        if(isset($_POST['save_submission']) && isset($_POST['BLOCK_id'])) {
            $this->saveData();
            unset($_POST);
        } elseif(isset($_POST['delete_submission'])) {
        $this->deleteData();
        }

    }

}