<?php
class Csingle{

    var $C;               //main object
    var $nameF;
    var $LG;
    var $RESpath;

    function setDISPLAY() {
        if(isset($_POST['save_single']))
        {
            $content = $_POST['page_'.$this->LG];
            file_put_contents($this->RESpath,$content);
        }
    }

    function DISPLAY(){

        $LG = $this->LG;
        $idC = $this->C->idC;


        if(file_exists($this->RESpath))
             $pageContent = file_get_contents($this->RESpath);
        else
        {
            $pageContent = 'Nu exista continut la pagina <b>'.$this->RESpath.'</b>';
        }

        #_________________________________________________________________

        $display ="
                    <div class='SING single' id='single_$LG'>
                        <div class='EDeditor page'>
                            $pageContent

                        </div>
                    </div>
                   ";

        return  $display; //.'res: '.$this->RESpath;
    }

    function  setINI() {}
    function __construct($C){

        $this->C = &$C;
        $this->LG = &$C->lang;
        $this->nameF = $this->C->nameF;
        
	$this->RESpath = $this->C->GET_resPath();

        $this->setINI();
	$this->setDISPLAY();
	//$this->RESpath = $this->C->GET_resPath();
    }
}
