<?php
/**
 * Process the contact form's variables and prepares them for delivery (or return them to sender)
 * 
 * Hosted temporarily on Assembla Git
 * 
 * @author Victor Nitu
 * @desc Process the contact form's variables and prepares them for delivery (or return them to sender)
 *
 */
class Envelope extends Validation
{
    var $items;             // $_REQUEST items to be validated  and sent
    var $status = FALSE;    // Whether is a TRUE envelope or is a fake (FALSE)
    var $checked = array(); // Array containing checked items
    var $valid_items = 0;
    var $invalid_items = 0;
    var $errors = array();

    public function __construct($items) {
        $this->items = (is_array($items) && count($items)>0 ? $items : NULL);
        foreach ($this->items as $key => $value) {
            $this->items[$key] = array('desc' => $value, 'content' => $_POST[$key]);
        }
        $this->check($items);

        $this->mark();
    }

    protected function check($items) {
        if($this->items === NULL) return 0;
        else
            foreach ($items as $key => $item) {
                switch($key) {
                    case 'email':
                        $this->checked[$key] = parent::email($this->items[$key]['content']);
                        break;
                    case 'name':
                        if(parent::name($this->items[$key]['content'],40,'max') === 'valid')
                            $this->checked[$key] = parent::name($this->items[$key]['content'],3,'min');
                        break;
                    case 'built':
                    case 'description':
                        $this->checked[$key] = parent::text($this->items[$key]['content'],3,'min');
                        break;
                    case 'role':
                    case 'title':
                        $this->checked[$key] = 'valid';
                        break;
                    case 'accept':
                        $this->checked[$key] = parent::text($this->items[$key]['content'],1,'min');
                        break;
                    default:
                        break;
                }
            }
    }

    protected function mark() {
        $this->countValid($this->items);
        $this->status = ($this->valid_items == count($this->items) ? TRUE : FALSE );
    }

    protected function countValid($items) {
        foreach ($items as $key => $item) {
            if ($this->checked[$key]=='valid')
                $this->valid_items++;
            else {
                $this->invalid_items++;
                array_push($this->errors,$item['desc']);
            }
        }

    }

}
