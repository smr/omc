<?php
/**
 * Clasa de validare scrisa initial pentru CARP "Omenia" in cadrul SEAD
 *
 * @author    Victor Nitu <victor[at]serenitymedia[.]ro>
 *
 * @desc Clasa de validare scrisa pentru CARP "Omenia" in cadrul SEAD, neterminata
 *
 * TODO: refactoring
 * TODO: callbackuri neergonomice, restructurare necesara
 */
class Validation{
    var $string;

    var $pattern_email = '/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/';
    var $pattern_phone = '/^[0-9]{10}$/';
    var $pattern_numeric_punctuation = '/^[0-9\.\-\_\s]{6,}$/';
    var $pattern_ip    = '/([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/';
    //var $pattern_url   = '/^(http|https|ftp):\/\/(www\.)?.+\.([.]{2,4})$/';
    //var $pattern_url   = '/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/';
    var $pattern_url   = "#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie";
    var $pattern_cnp   = '/(^1|2)([0-9]{2})((([0]{1})([0-9]{1}))|(([1]{1})([1-2]{1})))((([0]{1})([0-9]{1}))|(([1-2]{1})([0-9]{1}))|(([3]{1})([0-1]{1})))([0-9]{6})(\b$)/';
    var $pattern_serieBI = '/^([a-zA-Z]{2})$/';
    var $pattern_nrBI    = '/^([0-9]{6})$/';

    public function __construct() {

    }

    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere ale alfabetului
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere ale alfabetului
     * @return bool
     */
    function alpha($string,$num_chars,$behave){
        if($behave=="min"){
            $pattern_alpha="/^[a-zA-Z]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_alpha="/^[a-zA-Z]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_alpha="/^[a-zA-Z]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_alpha,$string)>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice scrise cu litere mici
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice scrise cu litere mici
     * @return bool
     */
    function alphaLower($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_alphalow="/^[a-z]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_alphalow="/^[a-z]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_alphalow="/^[a-z]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_alphalow,$string)>0 ? 'valid' : 'invalid');

    }

    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice scrise cu litere mari
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice scrise cu litere mari
     * @return bool
     */
    function alphaUpper($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_alphaup="/^[A-Z]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_alphaup="/^[A-Z]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_alphaup="/^[A-Z]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_alphaup,$string)>0 ? 'valid' : 'invalid');

    }

    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere numerice
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere numerice
     * @return bool
     */
    function numeric($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_numeric="/^[0-9]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_numeric="/^[0-9]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_numeric="/^[0-9]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_numeric,$string)>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice
     * @return bool
     */
    function alphanum($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_alphanum="/^[0-9a-zA-Z]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_alphanum="/^[0-9a-zA-Z]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_alphanum="/^[0-9a-zA-Z]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_alphanum,$string)>0 ? 'valid' : 'invalid');
    }
    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice plus cratime, spatii si apostrof
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere alfanumerice plus cratime, spatii si apostrof
     * @return bool
     */
    function name($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_name="/^[0-9a-zA-Z-'\s]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_name="/^[0-9a-zA-Z-'\s]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_name="/^[0-9a-zA-Z-'\s]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_name,$string)>0 ? 'valid' : 'invalid');
    }
    /**
     * Valideaza stringul de intrare daca acesta este constituit din caractere specifice unei fraze (litere si punctuatie)
     * @param int $num_chars numarul de caractere permis
     * @param string $behave defineste comportamentul fata de numarul de caractere: min, max sau exact
     * @desc Valideaza stringul de intrare daca acesta este constituit din caractere specifice unei fraze (litere si punctuatie)
     * @return bool
     */
    function text($string,$num_chars,$behave="exactly"){
        if($behave=="min"){
            $pattern_name="/^[\S\s]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_name="/^[\S\s]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_name="/^[\S\s]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_name,$string)>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul daca reprezinta o adresa de email valida
     * @desc Valideaza stringul daca reprezinta o adresa de email valida
     * @return bool
     */
    function email(){
        //return (preg_match($this->pattern_email,func_get_arg(0))>0 ? 'valid' : 'invalid');
        return (filter_var(filter_var(func_get_arg(0), FILTER_SANITIZE_EMAIL),FILTER_VALIDATE_EMAIL) ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul daca reprezinta un numar de telefon valid
     * @desc Valideaza stringul daca reprezinta un numar de telefon valid
     * @return bool
     */
    function phone(){
        return (preg_match($this->pattern_phone,func_get_arg(0))>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul daca e alcatuit din numere si semne de punctuatie
     * @desc Valideaza stringul daca e alcatuit din numere si semne de punctuatie
     * @return bool
     */
    function numeric_punctuation($string,$num_chars,$behave="exactly"){
		if($behave=="min"){
            $pattern_name="/^[0-9\.\-\_\s]{".$num_chars.",}$/";
        }else if ($behave=="max"){
            $pattern_name="/^[0-9\.\-\_\s]{0,".$num_chars."}$/";
        }else if ($behave=="exactly"){
            $pattern_name="/^[0-9\.\-\_\s]{".$num_chars.",".$num_chars."}$/";
        }
        return (preg_match($pattern_name,$string)>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul daca reprezinta o adresa IP valida
     * @desc Valideaza stringul daca reprezinta o adresa IP valida
     * @return bool
     */
    function ip_address($ip){
        return (preg_match($this->pattern_ip,func_get_arg(0))>0 ? 'valid' : 'invalid');
    }

    /**
     * Valideaza stringul daca reprezinta o adresa URL valida
     * @desc Valideaza stringul daca reprezinta o adresa URL valida
     * @return bool
     */
    function url(){
        //return (preg_match($this->pattern_url,func_get_arg(0))>0 ? 'valid' : 'invalid');
        return (filter_var('http://'.filter_var(func_get_arg(0), FILTER_SANITIZE_URL),FILTER_VALIDATE_URL) ? 'valid' : 'invalid');
        //return func_get_arg(0);
    }
}
?>