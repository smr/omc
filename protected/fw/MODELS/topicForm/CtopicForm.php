<?php
/**
 *
 * @author
 * @desc
 *
 * @param
 * @return
 */


class CtopicForm
{
    var $C;               //main object
    var $nameF;
    var $LG;
    var $DB;
    var $msg;
    var $date;
    var $err = '';

    function DISPLAY(){

        $LG = $this->LG;
        if(isset($this->msg)) {
            $display = '<b>'
                      .$this->msg
                      .'</b>';
            unset($this->msg);
        }
        else
            $display = $this->err
                      .$this->getForm();


        return  $display;
    }

    function getForm() {
        $display = <<<FORM
        <div style="margin:0px auto; width: 80%; padding: 20px;">
            <p>Please use the form bellow and submit a brief description of your idea and your needs.
            The idea owner is also the team leader. Don't forget to mention who you are, what you do and do respect
            the limit of words.</p>
        <form action="" method="post" id="form" style="display:block; margin: 0px auto;">

            <input type="hidden" name="action" value="contact" />

            <table>
                <tr>
                    <td colspan="3">Topic title<br />
                    <input type="text" name="title" value="{$_POST['title']}" style="width:100%;" /></td>
                </tr>
                <tr>
                    <td colspan="3">Topic Description (max 100 words)<br />
                        <textarea id="editor_description" class='description' name="description">{$_POST['description']}</textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">What part of the project have you already built? (max 100 words)<br />
                        <textarea id="editor_built" class='built' name="built">{$_POST['built']}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="email" value="{$_POST['email']}" placeholder="email (required)" /><br/>
                        <input type="text" name="name" value="{$_POST['name']}" placeholder="name (required)" />
                    </td>
                    <td>

                    </td>
                    <td>Role:
                        <select name="role">
                            <option value="coder">Coder</option>
                            <option value="journalist">Journalist</option>
                            <option value="designer">Designer</option>
                            <option value="activist">Activist</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- <input type="checkbox" name="accept" />  I accept the <a href="#terms" id="terms">Terms of Submission</a> -->
                    </td>
                </tr><tr>
                <td colspan="3"><hr /></td>
                </tr><tr>
                    <td colspan="2">
                        <img id="captcha" src="MODULES/securimage/securimage_show.php" alt="CAPTCHA Image" /><br />
                        <input type="text" name="captcha_code" size="10" maxlength="6" />
                            <a href="#"
                                onclick="document.getElementById('captcha').src = 'MODULES/securimage/securimage_show.php?' + Math.random(); return false">
                                [ Different Image ]
                            </a>
                    </td>
                    <td>
                        <input type="hidden" name="type" value="topicForm" />
                        <input type="hidden" name="date" value="{$this->date}" />
                        <input type="submit" value="Send your idea!" />
                    </td>
                </tr>
            </table>

        </form>
        </div>
        <div id="terms-popup"><div class="close-button"><p>X</p></div><div id="terms-content"></div></div>
FORM;
        return $display;
    }

    function process() {
        if(isset($_POST['action'])) {

            include_once publicPath."MODULES/securimage/securimage.php";
        	$securimage = new Securimage();


            if($_POST['action'] == 'contact') {
                // urmeaza toata birocratia aferenta trimiterii unui mail //

                $envArgs = array();

                $envArgs['title']       = array('trusted'   ,'Title');
                $envArgs['name']        = array('name,3,40' ,'Full name (3 to 40 characters)');
                $envArgs['email']       = array('email'     ,'Email address');
                $envArgs['role']        = array('trusted'   ,'Role');
                $envArgs['built']       = array('text,3,n'  ,'Amount built (max 100 words)');
                $envArgs['description'] = array('text,3,n'  ,'Description (max 100 words)');
                //$envArgs['accept']      = array('checkbox,accept'  ,'You did not accept the Terms and Conditions!');
                //$envArgs['date']        = array('trusted,1,n'  ,'Current date');

                $env = new Envelope($envArgs);



                if($env->status == TRUE && $securimage->check($_POST['captcha_code']) == TRUE) {
                    $_SESSION['form']['error'] = 'Your message was sent, thank you!';

                // verifica daca "plicul" a iesit ok
                // daca da, new Mail(plicul), apoi redirect catre referrer
                // daca nu, break;

        		$messageContent = "From: ".$env->items['name']['content'].' <'.$env->items['email']['content'].">\r\n<br/>";
        		$messageContent .= "Post: "
        							."<div style='width:50%;'>"
        							.$env->items['description']['content']
        							."</div>"
        							."\r\n<br/>";

        	    $mail = new Mail(smtpServer);
        	    $mail->Username = smtpUser;
        	    $mail->Password = smtpPass;

        	    $mail->SetFrom("omc@thesponge.eu","Open Media Challenge");             // Name is optional
        	    $mail->AddTo($env->items['email']['content'],$env->items['name']['content']);   // Name is optional
        	    $mail->Subject = $env->items['title']['content'];
        	    $mail->Message = $messageContent;

        	    // Chestii optionale
        	    $mail->AddCc($env->items['email']['content'],$env->items['name']['content']); 	// Seteaza CC, numele e optional
        	    $mail->ContentType = "text/html";        		// Default in "text/plain; charset=iso-8859-1"
        	    $mail->ConnectTimeout = 30;		                // Socket timeout (sec)
        	    $mail->ResponseTimeout = 8;		                // CMD timeout (sec)

        	    $mail->Headers['Reply-To']=$env->items['email']['content'];




                    if($this->dbCheck($env->items['email']['content']) == 1) {
                        $success = $mail->Send();
                        $this->dbStore($env->items['email']['content'],
                                       $env->items['name']['content'],
                                       $env->items['role']['content'],
                                       $env->items['title']['content'],
                                       $env->items['description']['content'],
                                       $env->items['built']['content'],
                                       $_POST['date']
                                      );
                        unset ($_POST);
                        $this->msg =  "Thank you!";
                    } elseif($this->dbCheck($env->items['email']['content']) == 0) {
                        $this->msg =  "You already submitted your idea!";
                    }
                    unset($_POST);

        	}
                else {
                    $this->err .= '<b>Please correct the following fields:</b><br/><ul>';
                    if ($securimage->check($_POST['captcha_code']) == false) {
                        $this->err .='<li>Invalid Captcha code!</li>';
                    }
                    foreach ($env->errors as $value) {
                        $this->err .= '<li>'.$value."</li>\n";
                    }
                    $this->err .= '</ul>';

                }


                //header("Location: $referrer");
            } else {
                // revino la expeditor cu un mesaj corect dpdv tehnic, dar inutil
                $_SESSION['form']['error'] = "Mesaj corect si inutil.";
            }

        }
    }

    function dbStore($email,$name,$role,$title,$description,$built,$date) {
        $vars=array('email','name','role','title','description','built','date');
        foreach($vars as $arg)
          $clean[$arg] = $this->DB->real_escape_string($$arg);

        $qMemberInsert = "INSERT INTO members (email, name, role) VALUES ('{$clean['email']}', '{$clean['name']}', '{$clean['role']}');";

        $this->DB->query($qMemberInsert);
        $lastID = $this->DB->insert_id;

        $qTopicInsert  = "INSERT INTO topics  (title, description,built,submitter,date)
                                VALUES ('{$clean['title']}', '{$clean['description']}', '{$clean['built']}','$lastID','{$clean['date']}');";
        $qTeamInsert   = "INSERT INTO teams   (leader) VALUES ('$lastID');";

        $this->DB->query($qTopicInsert);
        $this->DB->query($qTeamInsert);

    }

    function dbCheck($email) {

        $qMemberCheck = "SELECT * FROM members WHERE email = '$email';";
        $result = $this->DB->query($qMemberCheck)->fetch_assoc();

        if($result != NULL)
            return 0; //not suitable for insert
        else
            return 1; //can be inserted safely

    }

    function __construct($C){

        $this->C = &$C;
        $this->LG = &$C->lang;
        $this->nameF = $this->C->nameF;
        $this->DB = &$this->C->DB;

        $this->date = date("Y-m-d");

        $this->process();


    }
}