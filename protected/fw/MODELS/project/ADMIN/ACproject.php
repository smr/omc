<?php

class ACproject extends Cproject {

    var $DB;
    var $C;
    var $member;
    var $memberID;
    var $message;
    var $msg;
    var $teamID;
    var $newteam;

    function sendMemberMoveNotice() {
        $mail = new Mail(smtpServer);
        $mail->Username = smtpUser;
        $mail->Password = smtpPass;

        $mail->SetFrom(smtpUser,"OMC Mailer");    // Name is optional
        $mail->AddTo($_POST['email'],$_POST['name']);       // Name is optional
        $mail->Subject = '[OMC]: Congratulations, you have joined a team!';
        $mail->Message = "Congratulations, you have joined team #{$_POST['project']} @ Open Media Challenge!";

        // Chestii optionale
        $mail->ContentType = "text/plain";        		    // Default in "text/plain; charset=iso-8859-1"
        $mail->Headers['Reply-To']=$_POST['email'];

        $mail->Send();
    }

    function sendLeaderMoveNotice() {
        $mail = new Mail(smtpServer);
        $mail->Username = smtpUser;
        $mail->Password = smtpPass;

        echo 'leader: '.$this->leader->email;

        $mail->SetFrom(smtpUser,"OMC Mailer");    // Name is optional
        $mail->AddTo($this->leader->email,$this->leader->name);       // Name is optional
        $mail->Subject = '[OMC]: New team member';
        $mail->Message = "Congratulations, your OMC team (#{$_POST['project']}) has a new member! See for yourself, at http://thesponge.eu/entry-list/{$_POST['project']}";

        // Chestii optionale
        $mail->ContentType = "text/plain";        		    // Default in "text/plain; charset=iso-8859-1"
        $mail->Headers['Reply-To']=$_POST['email'];

        $mail->Send();
    }

    function processMovePost() {
        $this->member  = $this->DB->query("select * from members where id = '{$_POST['member']}';")->fetch_object();
        $this->newteam = $this->DB->query("select * from topics where id = '{$_POST['teamM_en']}';")->fetch_object();
        $this->message = $this->member->name." was moved to team {$_POST['teamM_en']} ({$this->newteam->title})!";
                //$this->sendMemberNotice();
                //$this->sendLeaderNotice();
            $query = "UPDATE members
                                SET team = '{$_POST['teamM_en']}',
                                    name = '{$_POST['nameM_en']}',
                                    role = '{$_POST['roleM_en']}'
                            WHERE id = '{$this->member->id}';";
            $this->DB->query($query);
        unset($_POST);
        header("Location: {$_SERVER['REQUEST_URI']}");
        exit;
    }

    function processEditDescription () {
        $description = $_POST['EDeditor_en'];
        $id = $_GET['prj'];
        $query = "UPDATE topics SET description = '$description' WHERE id = '$id'";

        $this->DB->query($query);
                unset($_POST);
                header("Location: {$_SERVER['REQUEST_URI']}");
                exit;
    }

    function processEditBuilt () {
        $description = $_POST['EDeditor_en'];
        $id = $_GET['prj'];
        $query = "UPDATE topics SET built = '$description' WHERE id = '$id'";

        $this->DB->query($query);
                unset($_POST);
                header("Location: {$_SERVER['REQUEST_URI']}");
                exit;
    }

}