<?php

class Cproject {

    var $DB;
    var $C;
    var $LG;
    var $projectID;
    var $message;
    var $msg;
    var $leader;

    var $admin_news;

    private function getTeamList($default) {
        	$teams = array();
        	$teams[999] = 'Unaffiliated';
        	$teams[1]   = 'RICJn ';
        	$teams[2]   = 'Media Watch';
        	$teams[3]   = 'Publishing solution';
        	$teams[4]   = 'EU money for ';
        	$teams[5]   = 'Political colours';
        	$teams[6]   = 'Public records';
        	$teams[7]   = 'Map of arrests';
        	$teams[8]   = 'Mapping stray dogs';
        	$teams[9]   = 'Harta Politicii';
//        	$teams[10]  = 'Easy to set up';
//        	$teams[11]  = 'Public records';
//        	$teams[12]  = 'MediaWatch';
//        	$teams[13]  = 'free.ex';
//        	$teams[14]  = 'Map of arrests';
//        	$teams[15]  = 'Repression';
//        	$teams[16]  = 'Mapping the stray';
//        	$teams[17]  = 'Harta Politicii';

        	$options = '<select name="newteam">';
        	foreach ($teams as $id => $team) {
        		$extra = ($id == $default ? 'selected' : '');
        		$options .= "<option $extra value='$id'>$id. $team</option>";
        	}
            $options .= '</select>';

        	return $options;
    }

    public function DISPLAY() {
        $LG = $this->LG;


    $ED = ($this->C->admin ? '' : 'N');     #daca nu este admin nu vor fii editabile anumite parti (ex:membrii)
    #===================================================================================================================

        $id = $this->projectID;
        $project = $this->DB->query("SELECT * FROM topics WHERE id = '$id';")->fetch_object();

        if(intval(str_replace('-','',$project->date)) <= 20120714) {
                $membersQ = $this->DB->query("SELECT members.id AS id, members.email AS email,
                                                    members.name AS name, members.role AS role, teams.id AS team,
                                                    topics.title AS title
                                                FROM teams
                                                LEFT JOIN members
                                                    ON (teams.id = members.team )
                                                LEFT JOIN topics
                                                    ON (topics.id = teams.id)
                                                WHERE members.id <> teams.leader
                                                AND teams.leader = '$id';");

            $members = '<br/>Team members:<br/>';
            $members .= "<div  class='{$ED}allENTS membersTable' id='membersTable_en'>";
                if ($membersQ->num_rows>0) {
                    while ($m = $membersQ->fetch_object())
                    {
                        if($this->C->admin)
                        {
                                   $title = substr($m->title,0,20);
                                    // $title = $m->title;


                                   $mail= "<div class='emailM'>{$m->email}</div>";
                                   /*$team= $this->getTeamList($id);*/
                                   $team   = "<div class='EDsel teamM'>{$title}</div>";

                                   $hidden = "<input type='hidden' name='team' value='$id' />
                                              <input type='hidden' name='action' value='edit_member' />
                                              <input type='hidden' name='member' value='{$m->id}' />";

                        }
                        $members .= "<div class='{$ED}ENT member' id='member_{$m->id}_en'>
                                        <div class='EDtxt nameM'>{$m->name}</div>
                                        <div class='EDsel roleM'>{$m->role}</div>
                                            $team
                                            $mail
                                            $hidden
                                     </div>";
                    }
                }
                else {

                        $members .= "<div>&ndash;</div>";

                }
            $members .= '</div>';
        }
        else $members = 'You cannot join this team yet.';

        if(intval(str_replace('-','',$project->date)) <= 20120714 && !$this->C->admin) {
            $join_form = file_get_contents(publicPath.'fw/MODELS/project/RES/en/joinForm.html');
            $td = "<b>Join this team!</b> <br/>
                        <form action='' method='post'>
                            <input type='hidden' name='project' value='{$id}' />
                            <input type='hidden' name='action' value='add_member' />
                            $join_form
                        </form>";
        }

        if($project->id == 999) {
            $display = "<h3 class='project_title'> {$project->title}</h3>
                        <div class='project left'>{$project->description}</div>
                        <hr class='h_separator' />
                        <p class='error'>{$this->message}</p>
                        <div class='tabs_container'>
                                <div class='tab_content members'>
                                    <table id='bottom_table'><tr>
                                        <td>
                                            Team leader: <br/>
                                             <b>{$this->leader->name} (<em>Staff</em>)</b>
                                             <br/>
                                             $members
                                        </td>
                                        <td> $td</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class='tab_content join'>{$join_form}</div>

                        </div>
                                ";
        }
        else {
        $display = "<h3 class='project_title'> Project #{$project->id}: {$project->title}</h3>
                    <div class='SING project left' id='left_{$LG}'>
                        <div class='EDeditor'>
                            {$project->description}
                        </div>
                    </div>
                    <div class='SING project right' id='right_{$LG}'>
                        <div class='EDeditor'>
                            {$project->built}
                        </div>
                    </div>
                    <hr class='h_separator' />
                    <p class='error'>{$this->message}</p>
                    <div class='tabs_container'>
                        <div class='tab_content members'>
                            <table id='bottom_table'><tr>
                                <td>
                                    Team leader: <br/>
                                    <b>{$this->leader->name} (<em>{$this->leader->role}</em>)</b>
                                    <br/>
                                    $members
                                </td>
                                <td> $td</td>
                                </tr>
                            </table>
                        </div>
                            <div class='tab_content join'>{$join_form}</div>

                    </div>
                   <div class='SING progress' id='progress_en'>
                           Progress <span class='EDsel proc'>{$project->progress}</span> %
                   </div>


                            ";

        }

/*        $display .= "   <div class='SING progress' id='progress_en'>
                              Progress <span class='EDsel proc'>{$project->progress}</span> %
                        </div>
                    ";*/
        $display .= $this->C->topic_news->DISPLAY();

        unset($this->message);
        return $display;
    }

    private function sendMemberNotice()       {
        $mail = new Mail(smtpServer);
        $mail->Username = smtpUser;
        $mail->Password = smtpPass;

        $mail->SetFrom(smtpUser,"OMC Mailer");    // Name is optional
        $mail->AddTo($_POST['email'],$_POST['name']);       // Name is optional
        $mail->Subject = '[OMC]: Congratulations, you have joined a team!';
        $mail->Message = "Congratulations, you have joined team #{$_POST['project']} @ Open Media Challenge!";

        // Chestii optionale
        $mail->ContentType = "text/plain";        		    // Default in "text/plain; charset=iso-8859-1"
        $mail->Headers['Reply-To']=$_POST['email'];

        $mail->Send();
    }

    private function sendLeaderNotice()       {
        $mail = new Mail(smtpServer);
        $mail->Username = smtpUser;
        $mail->Password = smtpPass;

        echo 'leader: '.$this->leader->email;

        $mail->SetFrom(smtpUser,"OMC Mailer");    // Name is optional
        $mail->AddTo($this->leader->email,$this->leader->name);       // Name is optional
        $mail->Subject = '[OMC]: New team member';
        $mail->Message = "Congratulations, your OMC team (#{$_POST['project']}) has a new member! See for yourself, at http://thesponge.eu/entry-list/{$_POST['project']}";

        // Chestii optionale
        $mail->ContentType = "text/plain";        		    // Default in "text/plain; charset=iso-8859-1"
        $mail->Headers['Reply-To']=$_POST['email'];

        $mail->Send();
    }

    private function sendAdminNotice()        {
        $mail = new Mail(smtpServer);
        $mail->Username = smtpUser;
        $mail->Password = smtpPass;

        echo 'leader: '.$this->leader->email;

        $mail->SetFrom(smtpUser,"OMC Mailer");    // Name is optional
        $mail->AddTo('vnitu@ceata.org','OMC Administrator');       // Name is optional
        $mail->Subject = '[OMC]: New team member';
        $mail->Message = "http://thesponge.eu/entry-list/{$_POST['project']} \n ".$_POST['name'].' - '.$_POST['role'].' ('.$_POST['email'].')';

        // Chestii optionale
        $mail->ContentType = "text/plain";        		    // Default in "text/plain; charset=iso-8859-1"
        $mail->Headers['Reply-To']=$_POST['email'];

        $mail->Send();
    }

    private function processPost($id,$action) {
        include_once fw_pubPath."PLUGINS/securimage/securimage.php";
                	$securimage = new Securimage();


        $envArgs = array();
        $envArgs['name']        = array('name,3,40' ,'Full name (3 to 40 characters)');
        $envArgs['email']       = array('email'     ,'Email address');
        $env = new Envelope($envArgs);

        if($this->dbCheck($_POST['email']) == 1) {
            if($env->status == TRUE) {
                if ($securimage->check($_POST['captcha_code']) == false) {
                    $this->message = "<em>Wrong CAPTCHA code!</em><br/>\n";
                } else {
                    switch($action) {
                        default:
                        case 'add_member':
                        $this->message = 'You have joined a team, congratulations!';
                                $this->sendMemberNotice();
                                $this->sendLeaderNotice();
                                $this->sendAdminNotice();
                                $teamID = $this->DB->query("SELECT teams.id FROM topics
                                                        JOIN teams ON (topics.submitter = teams.leader)
                                                        WHERE topics.submitter = '$id'")->fetch_object()->id;
                                $this->DB->query("INSERT INTO members (name, email, role, team)
                                                            VALUES ('{$_POST['name']}', '{$_POST['email']}', '{$_POST['role']}', '$teamID');");
                                break;

                    }
                unset($_POST);
                header("Location: {$_SERVER['REQUEST_URI']}");
                exit;
                }
            }
            else {
                $this->message = '<b>Please correct the following fields:</b><br/>';
                foreach ($env->errors as $value)
                    $this->message .= "<em>$value</em><br/>\n";
            }
        } else {
            $this->message =  "You already joined a team!";
        }


    }

    private function processPROGRpost()       {

        $progress = $_POST['proc_en'];
        $query = "UPDATE topics set progress = '{$progress}' where id = '{$this->projectID}' ";
        $this->DB->query($query);
    }
    private function dbCheck($email)          {

        $qMemberCheck = "SELECT * FROM members WHERE email = '$email';";
        $result = $this->DB->query($qMemberCheck)->fetch_assoc();

        if($result != NULL)
            return 0; //not suitable for insert
        else
            return 1; //can be inserted safely

    }




    public function __construct($C)           {
        $this->C = &$C;
        $this->DB = $this->C->DB;
        $this->LG = 'en';
        $this->projectID = $_REQUEST['prj'];
        $this->memberID  = $_REQUEST['id'];
        $this->teamID    = $_REQUEST['team'];


        $this->leader  = $this->DB->query("SELECT topics.id, topics.title,
                                            members.id, members.name, members.role, teams.id, members.email
                                    FROM topics
                                    JOIN members
                                        ON (topics.submitter = members.id)
                                    JOIN teams
                                        ON (topics.submitter = teams.leader)
                                    WHERE topics.submitter= '".$this->projectID."'
                                        ;")->fetch_object();

        if(isset($_POST['action']) && $_POST['action']=='add_member') {
            $this->processPost($this->projectID,$_POST['action']);
        }
        if(isset($_POST['action']) && $_POST['action']=='edit_member') {
            $this->processMovePost();
        }
        if(isset($_POST['save_left']) && $_POST['save_left']=='s') {
            $this->processEditDescription();
        }
        if(isset($_POST['save_right']) && $_POST['save_right']=='s') {
            $this->processEditBuilt();
        }

        if(isset($_POST['save_progress'])) $this->processPROGRpost();
        #===============================================================================================================


    }

}