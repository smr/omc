<?php 
session_start(); //Starts the session.
define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

foreach($_GET as $key => $value) {
	$get[$key] = pmdb::connect()->escape($value);
}

/******** EMAIL ACTIVATION LINK**********************/
if(isset($get['user']) && !empty($get['activ_code']) && !empty($get['user']) && is_numeric($get['activ_code']) ) {

$err = array();
$msg = array();

$user = pmdb::connect()->escape($get['user']);
$activ = pmdb::connect()->escape($get['activ_code']);

//check if activ code and user is valid
$rs_check = pmdb::connect()->query("SELECT user_id FROM " . DB . "members WHERE md5_id='$user' AND activation_code='$activ'"); 
$num = $rs_check->num_rows;
  // Match row found with more than 1 results  - the user is authenticated. 
    if ( $num <= 0 ) { 
	$err[] = "Sorry no such account exists or activation code invalid.";
	//header("Location: pm-activate.php?msg=$msg");
	//exit();
	}

if(empty($err)) {
// set the approved field to 1 to activate the account
$rs_activ = pmdb::connect()->query("UPDATE " . DB . "members set active = '1' WHERE 
						 md5_id='$user' AND activation_code = '$activ' ");
$msg[] = "Thank you. Your account has been activated.";
//header("Location: pm-activate.php?done=1&msg=$msg");						 
//exit();
 }
}

/******************* ACTIVATION BY FORM**************************/
if ($_POST['doActivate'] == 'Activate') {
$err = array();
$msg = array();

$user_email = pmdb::connect()->escape($_POST['user_email']);
$activ = pmdb::connect()->escape($_POST['activ_code']);
//check if activ code and user is valid as precaution
$rs_check = pmdb::connect()->query("SELECT user_id FROM " . DB . "members WHERE email='$user_email' AND activation_code='$activ'"); 
$num = $rs_check->num_rows;
  // Match row found with more than 1 results  - the user is authenticated. 
    if ( $num <= 0 ) { 
	$err[] = "Sorry no such account exists or activation code invalid.";
	//header("Location: pm-activate.php?msg=$msg");
	//exit();
	}
//set approved field to 1 to activate the user
if(empty($err)) {
	$rs_activ = pmdb::connect()->query("UPDATE " . DB . "members SET active = '1' WHERE 
						 email='$user_email' AND activation_code = '$activ' ");
	$msg[] = "Thank you. Your account has been activated.";
 }
//header("Location: pm-activate.php?msg=$msg");						 
//exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title><?php _e( _( 'User Activation :: ' ) . get_pm_option( 'sitetitle' ) ); ?></title>
<link href="<?php _e( PM_URI ) ?>/css/form.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery.validate.js"></script>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
  <script>
  $(document).ready(function(){
    $("#actForm").validate();
  });
  </script>
</head>

<body>

<!-- Box Start-->
<div id="box_bg">
	<div id="pp-logo"><a href="<?php _e( PM_URI ); ?>"><?php _e( pp_logo( 80 ) ); ?></a></div>
<div id="content">
	<h1>Activate Account</h1>
	<p>&nbsp;</p>
	<p> 
        <?php
	  /******************** ERROR MESSAGES*************************************************
	  This code is to show error messages 
	  **************************************************************************/
	if(!empty($err))  {
	   echo "<p>&nbsp;</p><div class=\"msg\">";
	  foreach ($err as $e) {
	    echo "* $e <br>";
	    }
	  echo "</div>";	
	   }
	   if(!empty($msg))  {
	    echo "<p>&nbsp;</p><div class=\"msg\">" . $msg[0] . "</div>";

	   }	
	  /******************************* END ********************************/	  
	  ?>
      </p>
      
      <p>&nbsp;</p>
	
	<p>Please enter your email and activation code sent to you to your email 
        address to activate your account. Once your account is activated you can 
        <a href="pm-login.php">login here</a>.</p>
	
	<!-- Activation Fields -->
	<form action="pm-activate.php" method="post" name="actForm" id="actForm" >
	<div id="login">
	<input type="text" name='user_email' onblur="if(this.value=='')this.value='Email';" onfocus="if(this.value=='Email')this.value='';" value="Email" class="login user"/>
	<input type='text' name='activ_code' value='Activation Code'  onfocus="if(this.value=='' || this.value == 'Activation Code') {this.value='';this.type='password'}"  onblur="if(this.value == '') {this.type='text';this.value=this.defaultValue}" class="login password"/>
	</div>
	
	<!-- Green Button -->
	<div class="button input"><input class="button green" type="submit" value="Activate" name="doActivate" /></div>
	
	<!-- Checkbox -->
	<div class="checkbox">
	<li>
	<fieldset>
	<![if !IE | (gte IE 8)]><legend id="title2" class="desc"></legend><![endif]>
	<!--[if lt IE 8]><label id="title2" class="desc"></label><![endif]-->
	<div></div>
	</fieldset>
	</li>
	</div>
	</form>

</div>
</div>

<!-- Text Under Box -->
<div id="bottom_text">
	<?php _e( _( 'Have an account?' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-login.php"><?php _e( _( 'Login' ) ); ?></a><br/>
	<?php _e( _( 'Remind' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-forgot.php"><?php _e( _( 'Password' ) ); ?></a>
</div>

</body>
</html>