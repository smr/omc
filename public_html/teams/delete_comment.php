<?php
/**
 * ProjectPress delete wall feed comment
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(__FILE__) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');
	
	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

pmdb::connect()->delete( DB . "wall_posts_comments", "c_id ='".$_REQUEST['c_id']."' AND c_user ='".$_SESSION['username']."'" );