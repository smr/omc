<?php
/**
 * ProjectPress configuration file
 *
 * @package ProjectPress
 * @since 2.0
 */


// When system was installed?
$system = array();
$system['title'] = 'ProjectPress';
$system['company'] = '7 Media Web Solutions, LLC';
$system['version'] = '3.0.6';
$system['installed'] = '02:07:35, July 24, 2012';

define('DB_HOST','localhost');
define('DB_NAME','moz_teams');
define('DB_USER','root');
define('DB_PASS','kracatau');

//prefix for database tables. Caution: do not change this.
define('DB', 'omc_');
/*End database settings*/ 

/*Set the settings below for your install*/

/* Absolute directory path for file inclusion */
define( 'ABSPATH', dirname(__FILE__) . '/' );

// Set the absolute path of your install.
define('PM_DIR', ABSPATH);

// Set the url to your install, *do not* include the trailing slash '/'
define('PM_URI', 'http://thesponge.eu/teams');

// Allow users to upload an avatar
define('UPLOAD_AVATAR', TRUE);

/* Registration Type (Automatic or Manual) 
 1 -> Automatic Registration (Users will receive activation code and they will be automatically approved after clicking activation link)
 0 -> Manual Approval (Users will not receive activation code and you will need to approve every user manually)
*/
$user_registration = 0;  // set 0 or 1

// Basic Administrator information
$admin = array();
$admin['admin_email'] = 'vnitu@ceata.org';
