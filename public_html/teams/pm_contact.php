<?php
/**
 * ProjectPress contact form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(__FILE__) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if($_POST) {
$subject = pmdb::connect()->escape($_POST['subject']);
$email = pmdb::connect()->escape($_POST['email']);
$message = pmdb::connect()->escape($_POST['message']);

$to = get_pm_option('admin_email');
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; UTF-8' . "\r\n";
$headers .= 'From: ' . User::instance()->get_name( is_session_set('username') ) . ' <' . User::instance()->get_user_info( is_session_set('username'),'email' ) . '>' . "\r\n";
$body = "From: " . User::instance()->get_name( is_session_set('username') ) . "\r\n E-Mail: " . User::instance()->get_user_info( is_session_set('username'),'email' ) . "\r\n Message:\r\n " . $message;

pm_mail($to, $subject, $body, $headers);
}

	/**
	 * Creates a new template for the contact form.
	 */
	$pmcontact = new Template(PM_DIR . "pm-includes/tpl/pm_contact.tpl");
	$pmcontact->set("pmurl", get_pm_option('siteurl'));
	$pmcontact->set("contact", _( 'Contact' ));
	$pmcontact->set("success", PP::notices(1));
	$pmcontact->set("error", PP::notices(2));
	$pmcontact->set("form_message", get_pm_option('contact_form_message'));
	$pmcontact->set("firstname", User::instance()->get_user_info( is_session_set('username'),'first_name' ));
	$pmcontact->set("lastname", User::instance()->get_user_info( is_session_set('username'),'last_name' ));
	
	/**
	 * Outputs the contact form page.
	 */
	echo $pmcontact->output();

include(PM_DIR . 'pm-includes/footer.php');