<?php
/**
 * ProjectPress logout
 *
 * @package ProjectPress
 * @since 2.0
 */

session_start();

define('access',true);
include(dirname(__FILE__) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

userAccess::pm_logout();