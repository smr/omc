=== ProjectPress 3.0.4 ===
Developer(s): Joshua Parker
Personal Site: http://www.joshparker.us/
Company Site: http://www.7mediaws.org/
Support email: pp@7mediaws.org
Release Date: 04.29.2012

== Description ==
ProjectPress is a very small and lightweight collaboration system. You can use it as an intranet or you can 
install it for a small client based project, however it only supports one project at a time.


== Installation ==
1. Upload the files to your server.
2. Navigate to where you installed ProjectPress and the installer will start 
3. Make sure that the avatars folder in the /profile directory is writeable by the server 
if you plan to allow users to upload their own avatar.
4. Make sure the uploads folder in the projects directory is writeable by your server.
5. Update the baseurl in /js/chat.js on line 35.
6. The {dbprefix}online table will need to be cleaned out from time to time, so that it doesn't become too large. 
For your convenience, you can create a cron job to run the cron_user_online.php script.

== Upgrading ==
For instructions on upgrading from 2.x.x, please visit the forum @ http://projectpress.org/forums/topic/new-version-projectpress-3-0


== Bugging ==
If you find any bugs, please use the new Trac and Ticketing system @ http://trac.projectpress.org/projects/projectpress