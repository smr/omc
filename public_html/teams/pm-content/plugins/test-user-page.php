<?php
/*
Plugin Name: Test User Page
Plugin URI: http://projectpress.org/
Version: 1.0
Description: This is a plugin that you can use as an example. This example will give you guidance on creating non-plugin option pages, other plugin interface pages as well as other general user pages you would like to add to ProjectPress.
Author: Joshua Parker
Author URI: http://www.7mediaws.org
*/

// Register our plugin user page
add_action( 'plugins_loaded', 'test_user_page', 10 );

	function test_user_page() {
		// parameters: page slug, page title, function and icon that will display the page itself
		register_user_page( 'test_user', 'Test User', 'test_user_do_page' );
	}

	// Display Test User page
	function test_user_do_page() {
?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/cog.png" alt="" /><h1><?php _e('Test User Page'); ?></h1>
</div>
				
<div id="middle">
	<p>This is a test user page.</p>
</div>

<?php
}