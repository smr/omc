<?php
/*
Plugin Name: ProjectPress Recaptcha Plugin
Plugin URI: http://projectpress.org/plugins?plugins=2
Version: 1.0
Description: reCAPTCHA is an anti-spam method originating from <a href="http://www.cmu.edu/index.shtml">Carnegie Mellon University</a>, then acquired by <a href="http://www.google.com/recaptcha">Google</a> which uses <a href="http://www.google.com/recaptcha/captcha">CAPTCHAs</a> in a genius way. Enabling this plugin will integrate reCAPTCHA anti-spam methods into the ProjectPress registration, forget password, and login pages.
Author: Joshua Parker
Author URI: http://www.7mediaws.org/
*/

// Register our plugin admin page
add_action( 'plugins_loaded', 'pp_recaptcha_page', 10 );

	function pp_recaptcha_page() {
		// parameters: page slug, page title, and function that will display the page itself
		register_plugin_page( 'pp_recaptcha', 'PP Recaptcha Settings', 'pp_recaptcha_do_page' );
	}

	// Display Google Analytics Settings page
	function pp_recaptcha_do_page() {
		// Check if a form was submitted
		if( isset( $_POST['submit'] ) )
			update_pp_recaptcha_option();
?>

<div id="page-title">
	<img src="<?php _e(PM_URI); ?>/images/cog.png" alt="" /><h1><?php _e('ProjectPress Recaptcha Settings'); ?></h1>
</div>
				
		<div id="middle">
			<table cellspacing="5" cellpadding="0">
				<form name="options" method="post">
					<tr><td><?php _e('Public Key:'); ?></td> <td><input class="forminput" name="pp_recaptcha_publickey" size="40" value="<?php _e( get_pm_option('pp_recaptcha_publickey') ); ?>" maxlength="255" /></td></tr>
					<tr><td><?php _e('Private Key:'); ?></td> <td><input class="forminput" name="pp_recaptcha_privatekey" size="40" value="<?php _e( get_pm_option('pp_recaptcha_privatekey') ); ?>" maxlength="255" /></td></tr>
					<tr><td></td> <td><input type="submit" id="sub_button" name="submit" value="Update"></td>
				</form>
      		</table>
		</div>

<?php
	}

	function update_pp_recaptcha_option() {
		$pp_recaptcha_publickey = $_POST['pp_recaptcha_publickey'];
		$pp_recaptcha_privatekey = $_POST['pp_recaptcha_privatekey'];
	
		// Update value in database
		update_pm_option( 'pp_recaptcha_publickey', $pp_recaptcha_publickey);
		update_pm_option( 'pp_recaptcha_privatekey', $pp_recaptcha_privatekey);
		pm_redirect('plugins.php?page=' . $_GET['page']);
	}

	function recaptcha_lib_first() {
	/********************* RECAPTCHA CHECK *******************************
	This code checks and validates recaptcha
	****************************************************************/
	require_once(PM_DIR . 'pm-content/plugins/recaptcha/recaptchalib.php');
     
    	$resp = recaptcha_check_answer (get_pm_option('pp_recaptcha_privatekey'),
                                      	$_SERVER["REMOTE_ADDR"],
                                      	$_POST["recaptcha_challenge_field"],
                                      	$_POST["recaptcha_response_field"]);

    	if (!$resp->is_valid) {
        	die ("<h3>Image Verification failed!. Go back and try again.</h3>" .
            "(reCAPTCHA said: " . $resp->error . ")");                 
		}
	/************************ SERVER SIDE VALIDATION **************************************/
	}

	function recaptcha_lib_second() {
		require_once(PM_DIR . 'pm-content/plugins/recaptcha/recaptchalib.php'); _e( recaptcha_get_html( get_pm_option('pp_recaptcha_publickey') ) );
	}

add_action('pm_reg_form_script','recaptcha_lib_first',10);
add_action('pm_reg_form_bottom','recaptcha_lib_second',10);
add_action('pm_login_form_script','recaptcha_lib_first',10);
add_action('pm_login_form_bottom','recaptcha_lib_second',10);
add_action('pm_forgot_form_script','recaptcha_lib_first',10);
add_action('pm_forgot_form_bottom','recaptcha_lib_second',10);