<?php
/*
Plugin Name: Google Analytics
Plugin URI: http://projectpress.org/plugins?plugins=1
Version: 1.0
Description: A simple plugin to add Google Analytics code to the footer of all your pages. Simply enter your ID on the options page and you're ready to go.
Author: Joshua Parker
Author URI: http://www.7mediaws.org
*/

// Register our plugin admin page
add_action( 'plugins_loaded', 'google_analytics_page', 10 );

function google_analytics_page() {
// parameters: page slug, page title, and function that will display the page itself
register_plugin_page( 'google_analytics', 'Google Analytics Settings', 'google_analytics_do_page' );
}

// Display Google Analytics Settings page
function google_analytics_do_page() {
	// Check if a form was submitted
	if( isset( $_POST['submit'] ) )
		update_google_option();
?>

				<div id="page-title">
					<img src="<?php _e(PM_URI); ?>/images/cog.png" alt="" /><h1><?php _e('Google Analytics Settings'); ?></h1>
				</div>
				
		<div id="middle">
			<table cellspacing="5" cellpadding="0">
				<form name="options" method="post">
					<tr><td><?php _e('Google Analytics Code:'); ?></td> <td><input class="forminput" name="google_analytics" size="40" value="<?php echo get_pm_option('google_analytics'); ?>" maxlength="255"></td></tr>
					<tr><td></td> <td><input type="submit" id="sub_button" name="submit" value="Update"></td>
				</form>
      		</table>
		</div>

<?php
}

// Update option in database
function update_google_option() {
	$google_analytics = $_POST['google_analytics'];
	// Update value in database
	update_pm_option( 'google_analytics', $google_analytics);
	header('Location: plugins.php?page=' . $_GET['page']);
}

function google_analytics() {
	echo "<script type=\"text/javascript\">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '" . get_pm_option('google_analytics') . "']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>";
}
add_action('pm_footer','google_analytics',10);