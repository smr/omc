<?php
session_start(); //Starts the session.
define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');
?>

<html>
<head>
<title>Thank you</title>
<meta http-equiv="Content-Type" content="text/html; UTF-8">

<link href="<?php _e( PM_URI ); ?>/css/login-style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="register">
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="main">
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="160" valign="top"><p>&nbsp;</p>
      <p>&nbsp; </p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
    <td width="732" valign="top">
<h3 class="titlehdr">Thank you</h3>

      <h4>Your registration is now complete!</h4>

      <p><font size="2" face="Arial, Helvetica, sans-serif">An activation email 
        has been sent to your email address (dont forget to check your spam folder). 
        Please check your email and click on the activation link. You can <a href="pm-login.php">login 
        here</a> if you have already activated your account.</font></p>
      <p>&nbsp;</p>
	   
      <p align="right">&nbsp; </p></td>
    <td width="196" valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</div>
</body>
</html>