<?php 
/**
 * ProjectPress list of members
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(__FILE__) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$query = pmdb::connect()->select( DB . 'members', 'username,email,first_name,last_name', 'privacy = "0" AND active = "1"', 'last_name ASC' );

	/**
	 * Loop through the members and create a template for each one.
	 * Because each user is an array with key/value pairs defined, 
	 * we made our template so each key matches a tag in the template,
	 * allowing us to directly replace the values in the array.
	 * We save each template in the $usersTemplates array.
	 */
	while($row = $query->fetch_object()) {
	$members = new Template(PM_DIR . "pm-includes/tpl/list_members_row.tpl");
	$membersTemplates[] = $members;
	$members->set("pmurl", get_pm_option('siteurl'));
    $members->set("avatar", get_user_avatar($row->username,$row->email,30));
	$members->set("username", $row->username);
	$members->set("firstname", $row->first_name);
	$members->set("lastname", $row->last_name);
	$members->set("isonline", userOnline($row->username));
	}
	
	/**
	 * Merges all our members' templates into a single variable.
	 * This will allow us to use it in the main template.
	 */
	$membersContents = Template::merge($membersTemplates);
	
	/**
	 * Defines the main template and sets the members' content.
	 */
	$membersList  = new Template(PM_DIR . "pm-includes/tpl/list_members.tpl");
	$membersList->set("members", $membersContents);
	
	/**
	 * Loads our layout template, setting its site url, site title
	 * and content.
	 */
	$layout = new Template(PM_DIR . "pm-includes/tpl/members.tpl");
	$layout->set("pmurl", get_pm_option('siteurl'));
	$layout->set("sitetitle", get_pm_option('sitetitle'));
	$layout->set("content", $membersList->output());
	
	/**
	 * Outputs the page with the members list.
	 */
	echo $layout->output();

include(PM_DIR . 'pm-includes/footer.php');