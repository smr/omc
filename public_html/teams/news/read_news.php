<?php 
/**
 * ProjectPress news/announcement page
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

	$results = pmdb::connect()->get_row( "SELECT * FROM " . DB . "news WHERE newsid = '" . $_GET['id'] . "'" );

	/**
	 * Creates a new template for the news page.
	 */
	$readnews = new Template(PM_DIR . "pm-includes/tpl/read_news.tpl");
	$readnews->set("pmurl", get_pm_option('siteurl'));
	$readnews->set("newstitle", $results->title);
	$readnews->set("newsdate", date('F d, Y h:i A',strtotime($results->dtime)));
	$readnews->set("newstext", clickable_link(nl2br($results->text)));
	
	/**
	 * Outputs the page with full news/announcement.
	 */
	echo $readnews->output();

include(PM_DIR . 'pm-includes/footer.php');