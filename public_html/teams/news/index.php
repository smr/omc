<?php 
/**
 * ProjectPress redirects to the main news page
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// Enable for error checking and troubleshooting.
# display_errors();

pm_redirect(PM_URI . "/news/news.php"); ?>