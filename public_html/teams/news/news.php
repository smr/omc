<?php 
/**
 * ProjectPress main news page
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

//Generate the query so we can retrieve all titles in the DB in descending ID order
$query = pmdb::connect()->select( DB .'news', '*', null, 'newsid ASC LIMIT 15' );

	/**
	 * Loop through the news and create a template for each one.
	 */
	if($query->num_rows > 0) {
	while($row = $query->fetch_object()) {
		$news = new Template(PM_DIR . "pm-includes/tpl/list_news_row.tpl");
		$newsTemplates[] = $news;
	$news->set("pmurl", get_pm_option('siteurl'));
    $news->set("more", truncNewsText($row->text, 50));
	$news->set("newsid", $row->newsid);
	$news->set("newstitle", $row->title);
	$news->set("newsdate", date('F d, Y h:i A',strtotime($row->dtime)));
	
	/**
	 * Merges all our news templates into a single variable.
	 * This will allow us to use it in the main template.
	 */
	$newsContents = Template::merge($newsTemplates);
	
	/**
	 * Defines the main template and sets the news content.
	 */
	$newsList  = new Template(PM_DIR . "pm-includes/tpl/list_news.tpl");
	$newsList->set("news", $newsContents);
	
	/**
	 * Loads our layout template, setting its site url, site title
	 * and content.
	 */
	$layout = new Template(PM_DIR . "pm-includes/tpl/news.tpl");
	$layout->set("pmurl", get_pm_option('siteurl'));
	$layout->set("sitetitle", get_pm_option('sitetitle'));
	$layout->set("content", $newsList->output());
	}
	} else {
		$no_news = new Template(PM_DIR . "pm-includes/tpl/no_news_row.tpl");
		$no_newsTemplates[] = $no_news;
		$no_news->set("no_news", "Currently, no news items have been published.");
		
		$no_newsContents = Template::merge($no_newsTemplates);
		
		$no_newsList  = new Template(PM_DIR . "pm-includes/tpl/list_no_news.tpl");
		$no_newsList->set("nonews", $no_newsContents);
		
		$layout = new Template(PM_DIR . "pm-includes/tpl/no_news.tpl");
		$layout->set("pmurl", get_pm_option('siteurl'));
		$layout->set("sitetitle", get_pm_option('sitetitle'));
		$layout->set("content", $no_newsList->output());
	}
	
	/**
	 * Outputs the page with news/announcements.
	 */
	echo $layout->output();

include(PM_DIR . 'pm-includes/footer.php');