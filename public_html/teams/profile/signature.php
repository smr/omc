<?php
/**
 * ProjectPress user edit profile forum signature
 *
 * @package ProjectPress
 * @since 2.0
 */

// Enable for error checking and troubleshooting.
# display_errors();

if (!$framePage) {
  //We'll redirect the user to the proper page
  header("location: editprofile.php?p=".basename($_SERVER['SCRIPT_FILENAME'], '.php'));
}

if (isset($_POST['sig']) && $_POST['sig'] == 'Submit') {
	
	$sql = pmdb::connect()->update( DB . 'members', array('signature' => pmdb::connect()->escape($_POST['signature'])), array('username',$_SESSION['username']) );
	
	if($sql) {
		$message = '<div class="success">' . PP::notices(29) . '</div>';
	} else {
		$message = '<div class="error">' . PP::notices(30) . '</div>';
	}					
}

	$results = pmdb::connect()->get_row("SELECT signature FROM ". DB ."members WHERE username = '" . $_SESSION['username'] . "'");
	$basename = basename($_SERVER["REQUEST_URI"]);
	
	/**
	 * Creates a new template for the add member page.
	 */
	$editprofile = new Template(PM_DIR . "pm-includes/tpl/edit_profile_signature.tpl");
	$editprofile->set("pmurl", get_pm_option('siteurl'));
	$editprofile->set("signature", $results->signature);
	$editprofile->set("message", $message);
	
	if($basename = "editprofile.php?p=signature") {
		$editprofile->set("forumsignature", "class='active_link'");
	}
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $editprofile->output();
?>