<?php
/**
 * ProjectPress individual member's wall posts
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// User is logged in.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

	$next_records = 10;
	$show_more_button = 0;
	if(pmdb::connect()->escape($_REQUEST['value'])) {
		
		//pmdb::connect()->query("INSERT INTO ". DB ."wall_posts (post,date_created,p_user,email) VALUES('".pmdb::connect()->escape($_REQUEST['value'])."','".strtotime(date("Y-m-d H:i:s"))."','".$username."', '".$user->get_user_info($username,'email')."')");
		pmdb::connect()->insert( DB . 'wall_posts', array( pmdb::connect()->escape($_REQUEST['value']),
														   strtotime(date("Y-m-d H:i:s")),
														   $_SESSION['username'],
														   User::instance()->get_user_info($_SESSION['username'],'email')
														  ),
														  'post,
														  date_created,
														  p_user,
														  email'
								);
		
		$result = pmdb::connect()->select( DB . 'wall_posts', '*, UNIX_TIMESTAMP() - date_created AS TimeSpent', 'p_user = "' . $_SESSION['username'] . '"', 'p_id DESC LIMIT 1'  );
	
	}
	elseif($_REQUEST['show_more_post']) {
		$next_records = $_REQUEST['show_more_post'] + 10;
		
		$result = pmdb::connect()->select( DB . 'wall_posts', '*, UNIX_TIMESTAMP() - date_created AS TimeSpent', 'p_user = "' . $_GET['username'] . '"', 'p_id DESC LIMIT ' . $_REQUEST['show_more_post'] . ', 10' );
		$check_res = pmdb::connect()->select( DB . 'wall_posts', '*', 'p_user = "' . $_GET['username'] . '"', 'p_id DESC LIMIT ' . $next_records . ', 10' );
		
		$show_more_button = 0; // button in the end
		
		$check_result = $check_res->num_rows;
		if($check_result > 0) {
			$show_more_button = 1;
		}
	} else {	
		$show_more_button = 1;
		$result = pmdb::connect()->select( DB . 'wall_posts', '*, UNIX_TIMESTAMP() - date_created AS TimeSpent', 'p_user = "' . $_GET['username'] . '"', 'p_id DESC LIMIT 0,10' );
		
	}
	
	while ($row = $result->fetch_object()) {
		$username = $_SESSION['username'];
		
		$like_ip = pmdb::connect()->select( DB . 'wall_likes_username', 'count(*)', 'post_id = ' . $row->p_id . ' AND l_user = "' . $_SESSION['username'] . '"', null );
		$like_ip_num = $like_ip->num_rows;
		
		$user = pmdb::connect()->select( DB . 'wall_likes_username', 'l_user', 'post_id = ' . $row->p_id . ' AND l_user = "' . $_SESSION['username'] . '"', null );
		$user_likes = $user->num_rows;
		
		$total_comments = pmdb::connect()->select( DB . 'wall_posts_comments', 'count(*)', 'post_id = "' . $row->p_id . '"', null );
		$records = $total_comments->fetch_array();
		$records = $records[0];
		
		$total_likes = pmdb::connect()->select( DB . 'wall_posts', '*', 'p_id = "' . $row->p_id . '"', null );
		$likes = $total_likes->fetch_array();
		$likes = $likes['likes'];
		
		$comments = pmdb::connect()->select( DB . 'wall_posts_comments', '*, UNIX_TIMESTAMP() - date_created AS CommentTimeSpent', 'post_id = "' . $row->p_id . '"', 'date_created ASC LIMIT 0,2' );
		
		//$comments = pmdb::connect()->query("SELECT *, UNIX_TIMESTAMP() - date_created AS CommentTimeSpent FROM ".$chm."wall_posts_comments where post_id = ".$row->p_id." order by c_id asc");
		$comment_num_row = $comments->num_rows ;?>

<div class="friends_area" id="record-<?php _e( $row->p_id ); ?>">

	   <?php _e( get_user_avatar($row->p_user,$row->email) ); ?>

		   <label style="float:left" class="name">

		   <b><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row->p_user ); ?>"><?php _e( User::instance()->get_name($row->p_user) ); ?></a></b>

		   <em><?php _e( clickable_link($row->post) );?></em>
		   <br clear="all" />

		   <span>
		   <?php  
		   
		    // echo strtotime($row->date_created,"Y-m-d H:i:s");
   		    
		    $days = floor($row->TimeSpent / (60 * 60 * 24));
			$remainder = $row->TimeSpent % (60 * 60 * 24);
			$hours = floor($remainder / (60 * 60));
			$remainder = $remainder % (60 * 60);
			$minutes = floor($remainder / 60);
			$seconds = $remainder % 60;
			
			if($days > 0)
			echo date('F d Y', $row->date_created);
			elseif($days == 0 && $hours == 0 && $minutes == 0)
			echo "few seconds ago";		
			elseif($days == 0 && $hours == 0)
			echo $minutes.' minutes ago';
			elseif($days == 0 && $hours > 0)
			echo $hours.' hour ago';
			else
			echo "few seconds ago";	
			
		   ?>
		   
		   </span>
		   <a href="javascript: void(0)" id="post_id<?php _e( $row->p_id ); ?>" class="showCommentBox"><?php _e( _( 'Comments' ) ); ?></a>

			<span id="like-panel-<?php _e( $row->p_id ); ?>">
				
			<?php
			if($user_likes){?>
				<a href="javascript: void(0)" id="post_id<?php _e( $row->p_id ); ?>" class="Unlike"><?php _e( _( 'Unlike' ) ); ?></a>
			<?php }else{?>
				<a href="javascript: void(0)" id="post_id<?php _e( $row->p_id ); ?>" class="LikeThis"><?php _e( _( 'Like' ) ); ?></a>
			<?php }?>
				
			</span>

		   </label>
		   <?php
			if($row->p_user == $_SESSION['username']){?>
		  	<a href="#" class="delete"> <?php _e( _( 'Remove' ) ); ?></a>
		   <?php
			}?>

			<input type="hidden" value="<?php _e( $records ); ?>" id="totals-<?php _e( $row->p_id ); ?>" />

		    <br clear="all" />
		    
		    <div class="commentPanel" align="left">
				<img src="<?php _e( PM_URI ); ?>/images/like.png" style="float:left;" alt="" />
				
				<span class="likes" id="like-stats-<?php _e( $row->p_id ); ?>"> <?php _e( $likes ); ?> </span> <span class="likes"><?php _e( _( 'people like this.' ) ); ?></span>
				
				<span id="like-loader-<?php _e( $row->p_id ); ?>">&nbsp;</span>
			</div>
		    
		    <?php
			if ($records > 2) {
				$collapsed = true;?>
			<div class="commentPanel" id="collapsed-<?php _e( $row->p_id ); ?>" align="left">
				<img src="<?php _e( PM_URI ); ?>/images/cicon.png" style="float:left;" alt="" />
				<a href="javascript: void(0)" class="ViewComments">
				View all <?php _e( $records ); ?> comments 
				</a>
				<span id="loader-<?php _e( $row->p_id ); ?>">&nbsp;</span>
			</div>
			<?php
			}?>
			<div id="CommentPosted<?php _e( $row->p_id ); ?>">
				<?php
				//$comment_num_row = mysql_num_rows(@$comments);
				if($comment_num_row > 0) {
					while ($rows = $comments->fetch_object()) {
						$days2 = floor($rows->CommentTimeSpent / (60 * 60 * 24));
						$remainder = $rows->CommentTimeSpent % (60 * 60 * 24);
						$hours = floor($remainder / (60 * 60));
						$remainder = $remainder % (60 * 60);
						$minutes = floor($remainder / 60);
						$seconds = $remainder % 60;	?>

					<div class="commentPanel" id="record-<?php _e( $rows->c_id ); ?>" align="left">
						<?php _e( get_user_avatar($rows->c_user,$rows->email) ); ?>
						<label class="postedComments">
							<a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $rows->c_user ); ?>"><span id="name"><?php _e( User::instance()->get_name($rows->c_user) ); ?></span></a> <?php _e( clickable_link($rows->comments) );?>
						</label>
						<br clear="all" />
						<span style="margin-left:43px; color:#666666; font-size:11px">
						<?php
						
						if($days2 > 0)
						echo date('F d Y', $rows->date_created);
						elseif($days2 == 0 && $hours == 0 && $minutes == 0)
						echo "few seconds ago";		
						elseif($days2 == 0 && $hours == 0)
						echo $minutes.' minutes ago';
						elseif($days2 == 0 && $hours > 0)
						echo $hours.' hour ago';
						else
						echo "few seconds ago";	
						?>
						</span>
						<?php
						if($rows->c_user == $_SESSION['username']){?>
						&nbsp;&nbsp;<a href="#" id="CID-<?php _e( $rows->c_id ); ?>" class="c_delete"><?php _e( _( 'Delete' ) ); ?></a>
						<?php
						}?>
					</div>
					<?php
					}?>				
					<?php
				}?>
			</div>
			<div class="commentBox" align="right" id="commentBox-<?php _e( $row->p_id ); ?>" <?php _e( (($comment_num_row) ? '' :'style="display:none"') ); ?>>
				<?php _e( get_user_avatar($_SESSION['username'], User::instance()->get_email($_SESSION['username']),30) ); ?>
				<label id="record-<?php _e( $row->p_id ); ?>">
					<textarea class="commentMark" id="commentMark-<?php _e( $row->p_id ); ?>" name="commentMark" cols="60"></textarea>
				</label>
				<br clear="all" />
				<a id="SubmitComment" class="small button comment"> <?php _e( _( 'Comment' ) ); ?></a>
			</div>
	   </div>
	   <!--<div class="break"></div>-->
	<?php
	}
	if($show_more_button == 1){?>
	<div id="bottomMoreButton">
	<a id="more_<?php _e( @$next_records ); ?>" class="more_records" href="javascript: void(0)"><?php _e( _( 'Older Posts' ) ); ?></a>
	</div>
	<?php } ?>
<script type='text/javascript' src='<?php _e(get_javascript_directory_uri()); ?>/jquery.oembed.min.js'></script>
<script type='text/javascript' src='<?php _e(get_javascript_directory_uri()); ?>/jquery.oembed.js'></script>
<script type='text/javascript' src='<?php _e(get_javascript_directory_uri()); ?>/functions.js'></script>