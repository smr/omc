<?php
/**
 * ProjectPress add a wall comment
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(__FILE__)). '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

	if(pmdb::connect()->escape($_REQUEST['comment_text']) && $_REQUEST['post_id']) {
		
		pmdb::connect()->insert( DB . 'wall_posts_comments', array( 'c_id',
																	pmdb::connect()->escape( $_SESSION['username'] ),
																	pmdb::connect()->escape( $_REQUEST['comment_text'] ),
																	strtotime(date("Y-m-d H:i:s")),
																	pmdb::connect()->escape( $_REQUEST['post_id'] ),
																	pmdb::connect()->escape( User::instance()->get_user_info($_SESSION['username'],'email') )
																  ) 
							    );
		
		$result = pmdb::connect()->select( DB . 'wall_posts_comments', '*, UNIX_TIMESTAMP() - date_created AS CommentTimeSpent', null, 'c_id DESC LIMIT 1' );
		
		send_wall_comment_email();
	}

	while ($rows = $result->fetch_object()) {
		$days2 = floor($rows->CommentTimeSpent / (60 * 60 * 24));
		$remainder = $rows->CommentTimeSpent % (60 * 60 * 24);
		$hours = floor($remainder / (60 * 60));
		$remainder = $remainder % (60 * 60);
		$minutes = floor($remainder / 60);
		$seconds = $remainder % 60;	?>
		<div class="commentPanel" id="record-<?php _e( $rows->c_id ); ?>" align="left">
			<?php _e(get_user_avatar($rows->c_user,$rows->email)); ?>
			<label class="postedComments">
				<a href="<?php _e(PM_URI); ?>/profile/profile.php?username=<?php _e($rows->c_user); ?>"><span id="name"><?php _e( User::instance()->get_name($rows->c_user) ); ?></span></a> <?php _e(clickable_link($rows->comments));?>
			</label>
			<br clear="all" />
			
			<span style="margin-left:43px; color:#666666; font-size:11px">
			<?php
			
			if($days2 > 0)
			echo date('F d Y', $rows->date_created);
			elseif($days2 == 0 && $hours == 0 && $minutes == 0)
			echo "few seconds ago";		
			elseif($days2 == 0 && $hours == 0)
			echo $minutes.' minutes ago';
			else
			echo "few seconds ago";	
			
			?>
			</span>
			
			<?php
			$username = $_SESSION['username'];
			if($rows->c_user == $username){?>
			&nbsp;&nbsp;<a href="#" id="CID-<?php _e( $rows->c_id ); ?>" class="c_delete"><?php _e( _( 'Delete' ) ); ?></a>
			<?php
			}?>
		</div>
	<?php
	}