<?php 
/**
 * ProjectPress member's projects
 *
 * @package ProjectPress
 * @since 2.2
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// User is logged in.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_GET['username']) && !is_numeric($_GET['username'])) {
	if($_GET['username'] == is_session_set('username')) {

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/user.png" alt="" /><h1><?php _e( _( 'My Projects' ) ); ?></h1>
</div>
		
	<?php _e( User::instance()->get_my_profile_tabs() ); ?>

<div id="middle">
	<div id="directory">
	<table>
		<thead>
			<tr>
				<th><?php _e('Project Name'); ?></th>
				<th><?php _e('Type'); ?></th>
			</tr>
		</thead>		
		<?php _e( User::instance()->get_my_projects() ); ?>
	</table>
	</div>
</div>


<?php
	}
} else {
	_e( '<div class="error">This is farse. Be aware or you might get banned.</div>' );
}

include(PM_DIR . 'pm-includes/footer.php');