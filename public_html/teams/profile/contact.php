<?php
/**
 * ProjectPress user edit profile basic info
 *
 * @package ProjectPress
 * @since 2.0
 */

// Enable for error checking and troubleshooting.
# display_errors();

if (!$framePage) {
  //We'll redirect the user to the proper page
  header("location: editprofile.php?p=".basename($_SERVER['SCRIPT_FILENAME'], '.php'));
}

if (isset($_POST['contact']) && $_POST['contact'] == 'Submit') {
	
	$contacts = array( 'email' => pmdb::connect()->escape($_POST['email']), 
					   'aim' => pmdb::connect()->escape($_POST['aim']), 
					   'msn' => pmdb::connect()->escape($_POST['msn']), 
					   'gtalk' => pmdb::connect()->escape($_POST['gtalk']), 
					   'ichat' => pmdb::connect()->escape($_POST['ichat']) 
					 );
	
	$sql = pmdb::connect()->update( DB . "members", $contacts, array('username', $username) );
	
	$query1 = pmdb::connect()->update( DB . "wall_posts", array('email' => $_POST['email']), array('p_user', $username) );
	
	$query2 = pmdb::connect()->update( DB . "wall_posts_comments", array('email' => $_POST['email']), array('c_user', $username) );
	
	if($sql) {
		$message = '<div class="success">' . PP::notices(29) . '</div>';
	} else {
		$message = '<div class="error">' . PP::notices(30) . '</div>';
	}
}

	$results = pmdb::connect()->get_row("SELECT * FROM ". DB ."members WHERE username = '" . $_SESSION['username'] . "'");
	
	$basename = basename($_SERVER["REQUEST_URI"]);
	
	/**
	 * Creates a new template for the add member page.
	 */
	$editprofile = new Template(PM_DIR . "pm-includes/tpl/edit_profile_contact.tpl");
	$editprofile->set("pmurl", get_pm_option('siteurl'));
	$editprofile->set("email", $results->email);
	$editprofile->set("aim", $results->aim);
	$editprofile->set("msn", $results->msn);
	$editprofile->set("gtalk", $results->gtalk);
	$editprofile->set("ichat", $results->ichat);
	$editprofile->set("message", $message);
	
	if($basename = "editprofile.php?p=contact") {
		$editprofile->set("contact", "class='active_link'");
	}
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $editprofile->output();
?>