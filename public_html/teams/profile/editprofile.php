<?php
/**
 * ProjectPress edit profile basic
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// User is logged in.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

$framePage = true;

// Enable for error checking and troubleshooting.
# display_errors();

switch($_REQUEST['p']) {

case "basic":
include(PM_DIR . "profile/basic.php");
break;
// this should be your page name e.g, index.php?page=page1

case "contact":
include(PM_DIR . "profile/contact.php");
break;

case "signature":
include(PM_DIR . "profile/signature.php");
break;

case "avatar":
include(PM_DIR . "profile/avatar.php");
break;

default: // this should be index.php which is default
include(PM_DIR . "profile/basic.php");
}

include(PM_DIR . 'pm-includes/footer.php');