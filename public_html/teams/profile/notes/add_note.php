<?php
/**
 * ProjectPress add note
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(dirname(__FILE__))). '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

if( $_POST ) {

pmdb::connect()->insert(DB . "notes", array( 'n_id',
											 pmdb::connect()->escape( $_POST['note'] ),
											 pmdb::connect()->escape( $_SESSION['username'] ),
											 pmdb::connect()->escape( $_SESSION['username'] ),
											 pmdb::connect()->escape( User::instance()->get_user_info($_SESSION['username'], 'first_name') ),
											 pmdb::connect()->escape( User::instance()->get_user_info($_SESSION['username'], 'last_name') ),
											 pmdb::connect()->escape( User::instance()->get_user_info($_SESSION['username'], 'email') ),
											 strtotime(date('Y-m-d H:i:s'))
										   )
						);

}
else { }
?>

<li class="noteBox">
	<div class="avatar">
		<?php _e( get_user_avatar($_SESSION['username'], User::instance()->get_user_info($_SESSION['username'], 'email'),50) ); ?>
	</div>
	
	<div class="note-list">
		<a href="<?php _e(PM_URI); ?>/profile/profile.php?username=<?php _e( $_SESSION['username'] ); ?>"><?php _e( User::instance()->get_name($_SESSION['username']) ); ?></a>&nbsp;
		<?php _e($_POST['note']); ?> <br /><br />
	</div>
	
	<label class="name">
		<span style="float:left;padding-right:5px;">
			<?php _e( (date("Y-m-d H:i:s", $_POST[strtotime(date('Y-m-d H:i:s'))])) ); ?>
		</span>
	</label>
	<a href="#" nu="n_user-<?php _e($r->n_user); ?>" id="NID-<?php _e($r->n_id);?>" class="n_delete"><?php _e( _( 'Delete' ) ); ?></a>
</li>