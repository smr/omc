<?php 
/**
 * ProjectPress notes
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// User is logged in.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$sql = pmdb::connect()->select( DB . 'notes', '*', 'n_user = "' . $_GET['username'] . '"' );

?>

<script type="text/javascript" >
$(function() {
$(".n-submit").click(function() 
{
var first_name = $("#first_name").val();
var last_name = $("#last_name").val();
var email = $("#email").val();
var note = $("#note").val();
var n_user = $("#n_user").val();
var dataString = 'first_name='+ first_name + '&last_name='+ last_name + '&email=' + email + '&note=' + note + '&n_user=' + n_user;
if(note=='')
{
alert('You need to add a note');
}
else
{
$("#flash").show();
$("#flash").fadeIn(400).html('<img src="../../images/ajax-loader.gif" />Loading note...');
$.ajax({
type: "POST",
url: "add_note.php",
data: dataString,
cache: false,
success: function(html){
$("ul#update").append(html);
$("ul#update li:last").fadeIn("slow");
$("#flash").hide();
}
});
}return false;
});

		//deleteUserType
		$('a.n_delete').livequery("click", function(e){
			
			if(confirm('Are you sure you want to delete this note?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var n_id =  $(this).attr('id').replace('NID-','');
			var n_user = $(this).attr('nu').replace('n_user-','');
			
			$.ajax({

				type: 'get',

				url: 'delete_note.php?n_id='+ n_id + '&n_user='+ n_user,

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						parent.remove();

					});

				}

			});
		});
});
</script>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/note.png" alt="" /><h1><?php _e( _( 'Notes' ) ); ?></h1>
</div>

	<?php _e( User::instance()->get_my_profile_tabs() ); ?>
				
				<div id="middle">
				<p><?php _e( _( 'Use the form below to add important notes or to-do items.' ) ); ?></p>

				<ul id="update" class="timeline">
						<?php while($r = $sql->fetch_object()) { ?>
					<li class="noteBox">
						<div class="avatar">
						<?php _e( get_user_avatar($r->author,$r->email,50) ); ?>
						</div>

						<div class="note-list">
						<a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $r->author ); ?>"><?php _e( User::instance()->get_name($r->author) ); ?></a>&nbsp;
						<?php _e( $r->note ); ?> <br /><br />
						</div>
						
						<label class="name">
						<span style="float:left;padding-right:5px;">
						<?php _e( date("Y-m-d H:i:s",$r->date_created) ); ?>
						</span>
						</label>
						<a href="#" nu="n_user-<?php _e( $r->n_user ); ?>" id="NID-<?php _e( $r->n_id );?>" class="n_delete"><?php _e( _( 'Delete' ) ); ?></a>
					</li>
						<?php } ?>
				</ul>
				
				<div id="flash"></div>
				
				<div id="note-form">
					<form action="#" method="post">
						<input type="hidden" id="first_name" value="<?php _e( $_SESSION['first_name'] ); ?>" /><br />
						<input type="hidden" id="last_name" value="<?php _e( $_SESSION['last_name'] ); ?>" /><br />
						<input type="hidden" id="n_user" name="n_user" value="<?php _e( $_GET['username'] ); ?>"/><br />
						<textarea id="note" name="note" class="forminput"></textarea>
						<input type="submit" class="n-submit" id="sub_button" value="Submit Comment " />
					</form>
				</div>
				</div> <!--Ends notes-->


<?php include(PM_DIR . 'pm-includes/footer.php');