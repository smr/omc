<?php
/**
 * ProjectPress delete note
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

pmdb::connect()->delete(DB . "notes", "n_id ='".pmdb::connect()->escape($_REQUEST['n_id'])."' AND n_user = '".pmdb::connect()->escape($_REQUEST['n_user'])."'");