<?php
/**
 * ProjectPress upload avatar
 *
 * @package ProjectPress
 * @since 2.0
 */

if (!$framePage)
{
  //We'll redirect the user to the proper page
  header("location: editprofile.php?p=".basename($_SERVER['SCRIPT_FILENAME'], '.php'));
} else {

?>
				<div id="page-title">
					<img src="<?php echo PM_URI ?>/images/picture.png" alt="avatar" /><h1>Avatar</h1>
				</div>
				
<div id="tabs">
	<ul>
		<li <?php if (basename($_SERVER["REQUEST_URI"]) == "editprofile.php?p=basic") echo "class='active_link'";?>>
			<a href="editprofile.php?p=basic"><span>Basic</span></a>
		</li>
		<li <?php if (basename($_SERVER["REQUEST_URI"]) == "editprofile.php?p=contact") echo "class='active_link'";?>>
			<a href="editprofile.php?p=contact"><span>Contact</span></a>
		</li>
		<li <?php if (basename($_SERVER["REQUEST_URI"]) == "editprofile.php?p=signature") echo "class='active_link'";?>>
			<a href="editprofile.php?p=signature"><span>Forum Signature</span></a>
		</li>
		<?php if (defined('UPLOAD_AVATAR')) { ?>
		<li <?php if (basename($_SERVER["REQUEST_URI"]) == "editprofile.php?p=avatar") echo "class='active_link'";?>>
			<a href="editprofile.php?p=avatar"><span>Avatar</span></a>
		</li>
		<?php } ?>
	</ul>
</div>
<div id="middle">
<?php

if(isset($_POST['submit'])) { //see if submit button is pressed.

$username = $_SESSION['username'];

$name = $_FILES['uploadedfile']['name'];
$type = $_FILES['uploadedfile']['type'];
$size = $_FILES['uploadedfile']['size'];
$max_file_size = 358400;

//Now set allowed file types
$allow_type_1 = "image/jpeg";
$allow_type_2 = "image/gif";
$allow_type_3 = "image/png";
$allow_type_4 = "image/bmp";

$ext = substr($name, strripos($name, '.')); // strip name
$ext = '.gif';

$target_path = "avatars/";

$target_path = $target_path . $username.$ext; //This combines the directory, the random file name, and the extension

if($type == $allow_type_1 && $size < $max_file_size || $type == $allow_type_2 && $size < $max_file_size || $type == $allow_type_3 && $size < $max_file_size || $type == $allow_type_4 && $size < $max_file_size) {
	if(move_uploaded_file($_FILES ['uploadedfile']['tmp_name'], $target_path)) {
		chmod("$target_path",0444);
    
    echo "<div class=\"success\">The avatar " . $username.$ext . 
    " has been uploaded. <a href=\"editprofile.php?p=avatar\">Click here</a> to view it.</div></div>";
	} else {
    _e( '<div class="error">' . PP::notices(27) . '</div></div>' );
} 
	} else {
	_e( '<div class="error">' . PP::notices(28) . '</div></div>' );
	}

	
} //Ends post submit

if(isset($_POST['delete'])) 
{ 
  $delete_file = is_session_set('username') . '.gif'; 
  unlink(PM_DIR . "profile/avatars/$delete_file");
  echo '<meta http-equiv="Refresh" content="0;url=editprofile.php?p=avatar">';

}

if(!isset($_POST['submit'])) {
$max_file_size = 358400;
$pic = "SELECT username,email FROM ". DB ."members WHERE username = '$_SESSION[username]' LIMIT 1";
$result = pmdb::connect()->query($pic);
$row = $result->fetch_object();

echo '<div id="memleft">';
echo '<p>';
echo get_user_avatar($row->username,$row->email);
echo '</p>';
echo '</div>';

$avatar = PM_DIR . 'profile/avatars/' . is_session_set('username') . '.gif';

?>

<p>Upload a new pic if you would like to change your avatar or delete your current avatar 
if you would like to use a gravatar.</p>
<form enctype="multipart/form-data" action="editprofile.php?p=avatar" method="POST">
<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size; ?>" class="textBox" />
<p>You can upload a JPG, GIF, PNG, or BMP file.</p>
<p><input name="uploadedfile" type="file" /></p>
<p><input type="submit" name="submit" value="Upload File" class="sub_button" /></p>
</form>

<?php if(file_exists($avatar)) { ?>
<form action="editprofile.php?p=avatar" method="post">
<p><input type="submit" name="delete" class="sub_button" value="Delete Avatar"></p>
</form>
<?php } ?>
</div>

<?php
}
	}