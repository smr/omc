<?php 
/**
 * ProjectPress change password form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

$hasher = new PasswordHash(8, FALSE);
$hash = $hasher->HashPassword(pmdb::connect()->escape($_POST['password']));

// Enable for error checking and troubleshooting.
# display_errors();

if($_POST) {
	pmdb::connect()->update( DB . "members", array('password' => $hash), array('username',$_SESSION['username']) );
}

	/**
	 * Creates a new template for the add member page.
	 */
	$pass = new Template(PM_DIR . "pm-includes/tpl/changepassword.tpl");
	$pass->set("pmurl", get_pm_option('siteurl'));
	$pass->set("success", PP::notices(31));
	$pass->set("error", PP::notices(32));
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $pass->output();

include(PM_DIR . 'pm-includes/footer.php');