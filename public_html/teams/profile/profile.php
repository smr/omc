<?php 
/**
 * ProjectPress member's profile
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// User is logged in.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_GET['username']) && !is_numeric($_GET['username'])) {

	$results = pmdb::connect()->get_row( "SELECT * FROM " . DB . "members WHERE username = '" . $_GET['username'] . "' AND active = '1'" );

	if($_GET['username'] == $results->username) {

	if($results->privacy == 0 || $_GET['username'] == $_SESSION['username']) {

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/user.png" alt="" /><h1><?php _e( User::instance()->get_name( $_GET['username'] ) ); ?>'s Wall</h1>
</div>

	<?php _e( User::instance()->get_my_profile_tabs() ); ?>

				
<?php
if($results->privacy == 1) {
	_e( '<div class="error">' . User::instance()->get_name( $_GET['username'] ) . ', your profile is private, and you are the only one who is able to view it.</div>' );
}
?>

<?php if($_GET['username'] == $_SESSION['username']) { ?>
				<div id="wall-input" class="UIComposer_Box">
					<form action="" method="post">
					<fieldset>
					<textarea class="input" rows="2" cols="15" id="post" name="post"></textarea>
					<a id="shareButton" style="float:right" class="small button Detail"> Share</a>
					</fieldset>
					</form>
				</div>
<?php } ?>

				<div class="wall">
				<div id="posting" align="center">
						<?php include_once(PM_DIR . 'profile/posts.php');?>
				</div>
				</div> <!--Ends wall-->


<?php
		}
	} else {
		_e( '<div class="error">The user\'s profile is private or the user does not exist.</div>' );
	}
}

include(PM_DIR . 'pm-includes/footer.php');