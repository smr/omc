<?php
/**
 * ProjectPress view wall post comments
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();
	
	$comments = pmdb::connect()->select( DB . 'wall_posts_comments', '*, UNIX_TIMESTAMP() - date_created AS CommentTimeSpent', 'post_id =  ' . $_REQUEST['postId'], 'date_created ASC LIMIT 2, ' . $_REQUEST['totals'] );	
	$comment_num_row = $comments->num_rows;
	
	if($comment_num_row > 0) {
		while ($rows = $comments->fetch_object()) {
			$days2 = floor($rows->CommentTimeSpent / (60 * 60 * 24));
			$remainder = $rows->CommentTimeSpent % (60 * 60 * 24);
			$hours = floor($remainder / (60 * 60));
			$remainder = $remainder % (60 * 60);
			$minutes = floor($remainder / 60);
			$seconds = $remainder % 60;	?>
			
			<div class="commentPanel" align="left">
				<?php _e( get_user_avatar($rows->c_user,$rows->email) ); ?>
				<label class="postedComments">
					<a href="<?php _e(PM_URI); ?>/profile/profile.php?username=<?php _e($rows->c_user); ?>"><span id="name"><?php _e( User::instance()->get_name($rows->c_user) ); ?></span></a> <?php _e(clickable_link($rows->comments));?>
				</label>
				<br clear="all" />
				<span style="margin-left:43px; color:#666666; font-size:11px">
				<?php
				
				if($days2 > 0)
				echo date('F d Y', $rows->date_created);
				elseif($days2 == 0 && $hours == 0 && $minutes == 0)
				echo "few seconds ago";		
				elseif($days2 == 0 && $hours == 0)
				echo $minutes.' minutes ago';
				elseif($days2 == 0 && $hours > 0)
				echo $hours.' hour ago';
				else
				echo "few seconds ago";	
				?>
				<?php
				if($rows->c_user == $_SESSION['username']){?>
				&nbsp;&nbsp;<a href="#" id="CID-<?php _e( $rows->c_id ); ?>" class="c_delete"><?php _e( _( 'Delete' ) ); ?></a>
				<?php
				}?>
				</span>
				
			</div>
		<?php
		}
	}