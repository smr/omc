<?php
/**
 * ProjectPress user edit profile contact info
 *
 * @package ProjectPress
 * @since 2.0
 */

if (!$framePage) {
  //We'll redirect the user to the proper page
  header("location: editprofile.php?p=".basename($_SERVER['SCRIPT_FILENAME'], '.php'));
}

// Enable for error checking and troubleshooting.
# display_errors();

if (isset($_POST['basic']) && $_POST['basic'] == 'Submit') {
	
	$profile = array( 'first_name' => pmdb::connect()->escape($_POST['first_name']), 
					   'last_name' => pmdb::connect()->escape($_POST['last_name']), 
					   'twitter' => pmdb::connect()->escape($_POST['twitter']), 
					   'facebook' => pmdb::connect()->escape($_POST['facebook']), 
					   'dob' => pmdb::connect()->escape($_POST['dob']),
					   'bio' => pmdb::connect()->escape($_POST['bio'])
					 );

	$sql = pmdb::connect()->update(DB . "members", $profile, array('username',$_SESSION['username']));
	
	if($sql) {
		$message = '<div class="success">' . PP::notices(29) . '</div>';
	} else {
		$message = '<div class="error">' . PP::notices(30) . '</div>';
	}
}

	$results = pmdb::connect()->get_row("SELECT * FROM ". DB ."members WHERE username = '" . $_SESSION['username'] . "'");

	$basename = basename($_SERVER["REQUEST_URI"]);

	/**
	 * Creates a new template for the add member page.
	 */
	$editprofile = new Template(PM_DIR . "pm-includes/tpl/edit_profile_basic.tpl");
	$editprofile->set("pmurl", get_pm_option('siteurl'));
	$editprofile->set("firstname", $results->first_name);
	$editprofile->set("lastname", $results->last_name);
	$editprofile->set("twitter", $results->twitter);
	$editprofile->set("facebook", $results->facebook);
	$editprofile->set("dob", $results->dob);
	$editprofile->set("bio", $results->bio);
	//$editprofile->set("privacy", $results->privacy);
	$editprofile->set("message", $message);
	
	if($basename = "editprofile.php?p=basic") {
		$editprofile->set("basic", "class='active_link'");
	}
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $editprofile->output();
?>