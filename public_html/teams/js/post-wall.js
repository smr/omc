	var posting=false;
	$(document).ready(function(){
	
		$(".friends_area a").oembed(null, {
		embedMethod: "append",
		maxWidth: 400
		});
	
		$('#shareButton').click(function(){

			var a = $("#post").val();
			if((a != "What are you working on?")&&!posting)
			{
				posting=true;
				$("#watermark").attr("disabled", true);
				$.post("posts.php?value="+a, {
	
				}, function(response){
				posting=false;
					
					$('#posting').prepend($(response).fadeIn('slow'));
					$(".friends_area:first a").oembed(null, {maxWidth: 400, embedMethod: "append"});
					$("#post").val("What are you working on?");
				});
			}
		});	
		
		
		$('.commentMark').livequery("focus", function(e){
			
			var parent  = $(this).parent();
			$(".commentBox").children(".commentMark").css('width','320px');
			$(".commentBox").children("a#SubmitComment").hide();
			$(".commentBox").children(".CommentImg").hide();			
		
			var getID =  parent.attr('id').replace('record-','');			
			$("#commentBox-"+getID).children("a#SubmitComment").show();
			$('.commentMark').css('width','300px');
			$("#commentBox-"+getID).children(".CommentImg").show();			
		});	
		
		//showCommentBox
		$('a.showCommentBox').livequery("click", function(e){
			
			var getpID =  $(this).attr('id').replace('post_id','');	
			
			$("#commentBox-"+getpID).css('display','');
			$("#commentMark-"+getpID).focus();
			$("#commentBox-"+getpID).children("img.CommentImg").show();			
			$("#commentBox-"+getpID).children("a#SubmitComment").show();		
		});
		
		$(".commentPanel a").oembed(null, {
		embedMethod: "append",
		maxWidth: 295
		});
		
		//SubmitComment
		$('a.comment').livequery("click", function(e){
			
			var getpID =  $(this).parent().attr('id').replace('commentBox-','');	
			var comment_text = $("#commentMark-"+getpID).val();
			
			if((comment_text != "Write a comment...")&&(comment_text.length>0)
			&& (posting == false))
			{
				posting=true;
				$("#commentMark-"+getpID).attr("disabled", true);
				$.post("add_comment.php?comment_text="+comment_text+"&post_id="+getpID, {
	
				}, function(response){
				posting=false;
					
					$('#CommentPosted'+getpID).append($(response).fadeIn('slow'));
					$(".commentPanel:last a").oembed(null, {maxWidth: 295, embedMethod: "append"});
					$("#commentMark-"+getpID).val("Write a comment...");					
				});
			}
			
		});	
		
		//more records show
		$('a.more_records').livequery("click", function(e){
			
			var next =  $(this).attr('id').replace('more_','');
			
			$.post("posts.php?show_more_post="+next, {

			}, function(response){
				$('#bottomMoreButton').remove();
				$('#posting').append($(response).fadeIn('slow'));

			});
			
		});	
		
		//deleteComment
		$('a.c_delete').livequery("click", function(e){
			
			if(confirm('Are you sure you want to delete this comment?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var c_id =  $(this).attr('id').replace('CID-','');	
			
			$.ajax({

				type: 'get',

				url: 'delete_comment.php?c_id='+ c_id,

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						parent.remove();

					});

				}

			});
		});	
		
		/// hover show remove button
		$('.friends_area').livequery("mouseenter", function(e){
			$(this).children("a.delete").show();	
		});	
		$('.friends_area').livequery("mouseleave", function(e){
			$('a.delete').hide();	
		});	
		/// hover show remove button
		
		
		$('a.delete').livequery("click", function(e){

		if(confirm('Are you sure you want to delete this post?')==false)

		return false;

		e.preventDefault();

		var parent  = $(this).parent();

		var temp    = parent.attr('id').replace('record-','');

		var main_tr = $('#'+temp).parent();

			$.ajax({

				type: 'get',

				url: 'delete.php?id='+ parent.attr('id').replace('record-',''),

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						main_tr.remove();

					});

				}

			});

		});

		$('textarea').elastic();

		jQuery(function($){

		   $("#post").Watermark("What are you working on?");
		   $(".commentMark").Watermark("Write a comment...");

		});

		jQuery(function($){

		   $("#post").Watermark("watermark","#369");
		   $(".commentMark").Watermark("watermark","#EEEEEE");

		});	

		function UseData(){

		   $.Watermark.HideAll();

		   //Do Stuff

		   $.Watermark.ShowAll();

		}
		
	
	$(document).ready(function(){	
	
		$('.ViewComments').livequery("click",function(e){
			
			var parent  = $(this).parent();
			var getID   =  parent.attr('id').replace('collapsed-','');
			
			var total_comments = $("#totals-"+getID).val();
						
			$("#loader-"+getID).html('<img src="../images/loader.gif" alt="" />');
			
			$.post("view_comments.php?postId="+getID+"&totals="+total_comments, {
	
			}, function(response){
				
				$('#CommentPosted'+getID).prepend($(response).fadeIn('slow'));
				$('#collapsed-'+getID).hide();
				
			});
		});


		/// like 
		
		$('.LikeThis').livequery("click",function(e){
			
			var getID   =  $(this).attr('id').replace('post_id','');
			
			$("#like-loader-"+getID).html('<img src="../images/loader.gif" alt="" />');
			
			$.post("like.php?postId="+getID, {
	
			}, function(response){
				
				$('#like-stats-'+getID).html(response);
				
				$('#like-panel-'+getID).html('<a href="javascript: void(0)" id="post_id'+getID+'" class="Unlike">Unlike</a>');
				
				$("#like-loader-"+getID).html('');
			});
		});	
		
		/// unlike 
		
		$('.Unlike').livequery("click",function(e){
			
			var getID   =  $(this).attr('id').replace('post_id','');
			
			$("#like-loader-"+getID).html('<img src="../images/loader.gif" alt="" />');
			
			$.post("unlike.php?postId="+getID, {
	
			}, function(response){
				
				$('#like-stats-'+getID).html(response);
				
				$('#like-panel-'+getID).html('<a href="javascript: void(0)" id="post_id'+getID+'" class="LikeThis">Like</a>');
				
				$("#like-loader-"+getID).html('');
			});
		});
	
		
	});
		
		
		
		

	});