$(document).ready(function(){

		// click to submit an event
		$('#CreateEvent').click(function(){

			var a = $("#EventInput").val();
			if(a != "What are you planning?")
			{
				$.post("/event/event.php?val=1&"+$("#EventForm").serialize(), {
	
				}, function(response){
					$('#ShowEvents').prepend($(response).fadeIn('slow'));
					clearForm();
				});
			}
			else
			{
				alert("Enter event name.");
				$("#EventInput").focus();
			}
		});
		
		// delete event
		$('a.remove').livequery("click", function(e){
			if(confirm('Are you sure you want to delete?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var id =  $(this).attr('id').replace('record-','');	
			
			$.ajax({
				type: 'get',
				url: '/event/delete.php?id='+ id,
				data: '',
				beforeSend: function(){
				},
				success: function(){
					parent.fadeOut(200,function(){
						parent.remove();
					});
				}
			});
		});	
		
		// show form when input get focus
		$('#EventInput').focus(function(){
			$('#EventBox').fadeIn();
			$('a.cancel').fadeIn();
		});	
		
		// hide form when click on cancel
		$('a.cancel').click(function(){
			$('#EventBox').fadeOut();
			$('a.cancel').hide();
		});	
		
		// watermark input fields
		jQuery(function($){
		   
		   $("#EventInput").Watermark("What are you planning?");
		   $("#Where").Watermark("Where?");
		   $("#WhoInvited").Watermark("Who's Invited?");
		});
		jQuery(function($){

		    $("#EventInput").Watermark("watermark","#369");
			$("#Where").Watermark("watermark","#369");
			$("#WhoInvited").Watermark("watermark","#369");
			
		});	
		function UseData(){
		   $.Watermark.HideAll();
		   $.Watermark.ShowAll();
		}

	});	
	
	// show jquery calendar
	$(function() {
		$("#datepicker").datepicker();
	});
	
	function clearForm()
	{
		$('#EventInput').val("What are you planning?");
		$('#datepicker').val("Today");
		$('#WhoInvited').val("Who's Invited?");
		$('#Where').val("Where?");
		
		$('#EventBox').hide();
		$('a.cancel').hide();
	}