$(document).ready(function() {
$(".uut-submit").click(function() {
var uut_user = $("#uut_user").val();
var ut_id = $("#ut_id").val();
var dataString = 'uut_user=' + uut_user + '&ut_id='+ ut_id;

if(uut_user=='' || ut_id=='')
{
alert('No field can be blank.');
}
else
{
$("#flash").show();
$("#flash").fadeIn(400).html('<img src="images/ajax-loader.gif" />Loading User User Type...');
$.ajax({
type: "POST",
url: "../pm-admin/add_uut.php",
data: dataString,
cache: false,
success: function(html) {
$('#uut_user').val('');
$('#ut_id').val('');
$("ul#uuType").append(html);
$("ul#uuType li:last").fadeIn("slow");
$("#flash").hide();
}
});
}
return false;
});

		//deleteUserType
		$('a.uut_delete').livequery("click", function(e){
			
			if(confirm('Are you sure you want to delete this user type?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var ut_id =  $(this).attr('id').replace('UUTID-','');
			var uut_user = $(this).attr('ut').replace('uut_user-','');
			
			$.ajax({

				type: 'get',

				url: '../pm-admin/delete_uut.php?ut_id='+ ut_id + '&uut_user='+ uut_user,

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						parent.remove();

					});

				}

			});
		});
});