<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress access control levels
 *
 * @package ProjectPress
 * @since 2.1
 */

class pms {
    var $userid = '';
    var $messages = array();
    var $dateformat = '';

    // Constructor gets initiated with userid
    function pms($user,$date="d.m.Y - H:i") {
        // defining the given userid to the classuserid
        $this->userid = $user; 
        // Define that date_format
        $this->dateformat = $date;
    }
    
    // Fetch all messages from this user
    function getmessages($type=0) {
        // Specify what type of messages you want to fetch
        switch($type) {
			case "0": $sql = pmdb::connect()->select( DB . 'private_messages', '*', '`to` = "'. $this->userid .'" && `to_viewed` = "0" && `to_deleted` = "0"', '`created` DESC' ) or die(pmdb::connect()->is_error()); break; // New messages
			case "1": $sql = pmdb::connect()->select( DB . 'private_messages', '*', '`to` = "'. $this->userid .'" && `to_viewed` = "1" && `to_deleted` = "0"', '`to_vdate` DESC' ) or die(pmdb::connect()->is_error()); break; // Read messages
			case "2": $sql = pmdb::connect()->select( DB . 'private_messages', '*', '`from` = "'. $this->userid .'"', '`created` DESC' ) or die(pmdb::connect()->is_error()); break; // Send messages
			case "3": $sql = pmdb::connect()->select( DB . 'private_messages', '*', '`to` = "'. $this->userid .'" && `to_deleted` = "1"', '`to_ddate` DESC' ) or die(pmdb::connect()->is_error()); break; // Deleted messages
			default: $sql = pmdb::connect()->select( DB . 'private_messages', '*', '`to` = "'. $this->userid .'" && `to_viewed` = "0"', '`created` DESC' ) or die(pmdb::connect()->is_error()); break; // New messages
        }
        //$result = pmdb::connect()->query($sql);
        
        // Check if there are any results
        if($sql->num_rows) {
            $i=0;
            // reset the array
            $this->messages = array();
            // if yes, fetch them!
            while($row = $sql->fetch_assoc()) {
                $this->messages[$i]['id'] = $row['id'];
                $this->messages[$i]['title'] = $row['title'];
                $this->messages[$i]['message'] = $row['message'];
                $this->messages[$i]['fromid'] = $row['from'];
                $this->messages[$i]['toid'] = $row['to'];
                $this->messages[$i]['from'] = $this->getusername($row['from']);
                $this->messages[$i]['to'] = $this->getusername($row['to']);
                $this->messages[$i]['from_viewed'] = $row['from_viewed'];
                $this->messages[$i]['to_viewed'] = $row['to_viewed'];
                $this->messages[$i]['from_deleted'] = $row['from_deleted'];
                $this->messages[$i]['to_deleted'] = $row['to_deleted'];
                $this->messages[$i]['from_vdate'] = date($this->dateformat, strtotime($row['from_vdate']));
                $this->messages[$i]['to_vdate'] = date($this->dateformat, strtotime($row['to_vdate']));
                $this->messages[$i]['from_ddate'] = date($this->dateformat, strtotime($row['from_ddate']));
                $this->messages[$i]['to_ddate'] = date($this->dateformat, strtotime($row['to_ddate']));
                $this->messages[$i]['created'] = date($this->dateformat, strtotime($row['created']));
                $i++;
            }
        } else {
            // If not return false
            return false;
        }
    }
    
    // Fetch the username from a userid, I made this function because I don't know how you did build your usersystem, that's why I also didn't use left join... this way you can easily edit it
    function getusername($userid) {
		$sql = pmdb::connect()->select( DB . 'members', 'username', '`user_id` = "' . $userid . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
        // Check if there is someone with this id
        if($sql->num_rows) {
            // if yes get his username
            $row = $sql->fetch_row();
            return $row[0];
        } else {
            // if not, name him Unknown
            return "Unknown";
        }
    }
    
    // Fetch a specific message
    function getmessage($message) {
		$sql = pmdb::connect()->select( DB . 'private_messages', '*', '`id` = "'. $message .'" && (`from` = "' . $this->userid . '" || `to` = "' . $this->userid . '")', null, ' LIMIT 1') or die(pmdb::connect()->is_error());
        if($sql->num_rows) {
            // reset the array
            $this->messages = array();
            // fetch the data
            $row = $sql->fetch_assoc();
            $this->messages[0]['id'] = $row['id'];
            $this->messages[0]['title'] = $row['title'];
            $this->messages[0]['message'] = $row['message'];
            $this->messages[0]['fromid'] = $row['from'];
            $this->messages[0]['toid'] = $row['to'];
            $this->messages[0]['from'] = $this->getusername($row['from']);
            $this->messages[0]['to'] = $this->getusername($row['to']);
            $this->messages[0]['from_viewed'] = $row['from_viewed'];
            $this->messages[0]['to_viewed'] = $row['to_viewed'];
            $this->messages[0]['from_deleted'] = $row['from_deleted'];
            $this->messages[0]['to_deleted'] = $row['to_deleted'];
            $this->messages[0]['from_vdate'] = date($this->dateformat, strtotime($row['from_vdate']));
            $this->messages[0]['to_vdate'] = date($this->dateformat, strtotime($row['to_vdate']));
            $this->messages[0]['from_ddate'] = date($this->dateformat, strtotime($row['from_ddate']));
            $this->messages[0]['to_ddate'] = date($this->dateformat, strtotime($row['to_ddate']));
            $this->messages[0]['created'] = date($this->dateformat, strtotime($row['created']));
        } else {
            return false;
        }
    }
    
    // We need the userid for pms, but we only let users input usernames, so we need to get the userid of the username :)
    function getuserid($username) {
		$sql = pmdb::connect()->select( DB . 'members', 'user_id', '`username` = "' . $username . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
        if($sql->num_rows) {
            $row = $sql->fetch_row();
            return $row[0];
        } else {
            return false;
        }
    }
    
    // Flag a message as viewed
    function viewed($message) {
        $sql = pmdb::connect()->update( DB . 'private_messages', array( '`to_viewed`' => '1', '`to_vdate`' => 'NOW()' ), array('`id`', $message), ' LIMIT 1' ) or die(pmdb::connect()->is_error());
		return ( $sql ) ? true:false;
    }
    
    // Flag a message as deleted
    function deleted($message) {
		$sql = pmdb::connect()->update( DB . 'private_messages', array( '`to_deleted`' => '1', '`to_ddate`' => 'NOW()' ), array('`id`', $message), ' LIMIT 1' ) or die(pmdb::connect()->is_error());
		return ( $sql ) ? true:false;
    }
    
    // Add a new personal message
    function sendmessage($to,$title,$message) {
        $to = $this->getuserid($to);
        $sql = pmdb::connect()->query( "INSERT INTO " . DB . "private_messages SET `to` = '".$to."', `from` = '".$this->userid."', `title` = '".$title."', `message` = '".$message."', `created` = NOW()" ) or die(pmdb::connect()->is_error());
		return ( $sql ) ? true:false;
    }
    
    // Render the text (in here you can easily add bbcode for example)
    function render($message) {
        $message = strip_tags($message, '');
        $message = stripslashes($message); 
        $message = nl2br($message);
        return $message;
    }

}