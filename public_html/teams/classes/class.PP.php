<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress environment setup class.
 *
 * @package ProjectPress
 * @since 3.0.2
 */

class PP {
	
	public function __contruct() {
		if ( !self::$instance )
            self::$instance = new PP();
        return self::$instance;
	}
	
	public static function pm_version() {
		$version =  'Based on <a href="http://projectpress.org/">ProjectPress-' . CURRENT_VERSION . '</a>';
		$version = apply_filter('pm_version',$version);
		return $version;
	}
	
	public static function upgrade($array) {
		$upgrade = explode("\n", file_get_contents('http://projectpress.org/variables.txt'));
		return $upgrade[$array];
	}
	
	public static function notices($num) {
		$msg[1] = _('Your message has been sent and someone will contact you as soon as possible.');
		$msg[2] = _('Sorry, all fields must be filled in before you can submit the form.');
		$msg[3] = _('Sorry, your topic could not be posted. Please try again.');
		$msg[4] = _('You forgot to choose a message to delete.  <a href="' . PM_URI . '/forum/">Click here</a> to go back to the main forum and try again.');
		$msg[5] = _('Sorry, your reply could not be updated.');
		$msg[6] = _('Message successfully sent!');
		$msg[7] = _('Error, couldn\'t send PM. Maybe wrong user.');
		$msg[8] = _('Message successfully deleted!');
		$msg[9] = _('Error, couldn\'t delete PM!');
		$msg[10] = _('Your new member has been added and was sent an email with new account details.');
		$msg[11] = _('Sorry, the new user could not be added. Please try again.');
		$msg[12] = _('Your news posting has been deleted.  <a href="' . PM_URI . '/news/news.php">Click here</a> to go back to your the main news page.');
		$msg[13] = _('You forgot to choose a news posting to delete.  <a href="' . PM_URI . '/news/news.php">Click here</a> to go back to the main news page and try again.');
		$msg[15] = _('The user has been updated successfully.');
		$msg[16] = _('The user was not updated, please try again.');
		$msg[17] = _('News item updated successfully.');
		$msg[18] = _('An error occurred. Please check you post and try again.');
		$msg[19] = _('The file you are trying to upload is not a .zip file. Please try again.');
		$msg[20] = _('Your plugin was uploaded and installed properly.');
		$msg[21] = _('There was a problem uploading your plugin. Please try again or check the plugin package.');
		$msg[22] = _('The re-install was successful.');
		$msg[23] = _('There was an error with the re-install. It is better that you download the full version and overwrite the files on your server manually.');
		$msg[24] = _('Your system was upgraded to the latest version.');
		$msg[25] = _('There was an error upgrading to the latest version. It is better that you download the full version and overwrite the files on your server manually.');
		$msg[26] = _('Sorry, your user type was not added. Please try again.');
		$msg[27] = _('There was an error uploading the file, please try again!');
		$msg[28] = _('File name is not valid or the file was more than ' . $max_file_size . ' bytes. <a href="editprofile.php?p=avatar">Click here</a> to try again.');
		$msg[29] = _('You profile was updated successfully.');
		$msg[30] = _('Your profile could not be updated. Please try again.');
		$msg[31] = _('Your password has been updated.');
		$msg[32] = _('Sorry, your password cannot be blank.');
		$msg[33] = _('The project has been added.');
		$msg[34] = _('Project name, description, type, and email must be filled out before submitting the form.');
		$msg[35] = _('Your project has been updated.');
		$msg[36] = _('Your project was unable to be updated. Please try again.');
		return $msg[$num];
	}
	
}