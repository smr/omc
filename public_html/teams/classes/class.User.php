<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

 /**
 * User Class
 *
 * @package ProjectPress User Class
 * @author Joshua Parker
 * @link http://www.joshparker.us/
 * @since 2.0
 */
class User {
	
	/**
     * User object
     *
     * @access public
     * @var object
     */
    public static $instance;
	
	 /**
     * Creates and references the User object.
     *
     * @access public
     * @return object User object
     */
    public static function instance() {
        if ( !self::$instance )
            self::$instance = new User();
        return self::$instance;
    }
	
	public function get_my_profile_tabs() {
		?>
				<div id="tabs">
					<ul>
						<li <?php if (active_link() == "profile.php?username=".$_GET['username']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $_GET['username'] ); ?>"><span><?php _e( _( 'Wall' ) ); ?></span></a>
						</li>
						<?php if($_GET['username'] == $_SESSION['username']) { ?>
						<li <?php if (active_link() == "index.php?username=".$_GET['username']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/profile/notes/index.php?username=<?php _e( $_GET['username'] ); ?>"><span><?php _e( _( 'Notes' ) ); ?></span></a>
						</li>
						<li <?php if (active_link() == "my_projects.php?username=".$_GET['username']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/profile/my_projects.php?username=<?php _e( $_GET['username'] ); ?>"><span><?php _e( _( 'My Projects' ) ); ?></span></a>
						</li>
						<?php } ?>
					</ul>
				</div>
<?php
	}
	
	/**
     * Retrieve a user's full name.
     *
	 * @access public
     * @param string $username Grab the name of a user.
     * @return object A user's first and last name
     */
	public function get_name($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', '*', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		if($result->num_rows > 0) {
	  		while($r = $result->fetch_object()) {
	  			
				return $r->first_name . ' ' . $r->last_name;
			}
	  
		}
			
	}
	
	/**
     * Retrieve a user's email address.
     *
	 * @access public
     * @param string $username Grab the email of a user.
     * @return object A user's email address
     */
	public function get_email($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', 'email', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		while($r = $result->fetch_object()) {
	  			
				return $r->email;
		}
			
	}
	
	/**
     * Retrieve a user's AOL AIM handle.
     *
	 * @access public
     * @param string $username Grab the aol handle of a user.
     * @return object A user's AOL AIM handle.
     */
	public function get_aim($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', 'aim', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		while($r = $result->fetch_object()) {
	  			
				return $r->aim;	  
	  
	  }
			
	}
	
	/**
     * Retrieve a user's full MSN handle.
     *
	 * @access public
     * @param string $username Grab the msn handle of a user.
     * @return object A user's MSN handle.
     */
	public function get_msn($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', 'msn', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		while($r = $result->fetch_object()) {
	  			
				return $r->msn;	  
	  
	  }
			
	}
	
	/**
     * Retrieve a user's Google Talk.
     *
	 * @access public
     * @param string $username Grab the gtalk handle of a user.
     * @return object A user's gTalk handle.
     */
	public function get_gtalk($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', 'gtalk', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		while($r = $result->fetch_object()) {
	  			
				return $r->gtalk;	  
	  
	  }
			
	}
	
	/**
     * Retrieve a user's iChat handle.
     *
	 * @access public
     * @param string $username Grab the ichat handle of a user.
     * @return object A user's iChat handle.
     */
	public function get_ichat($username) {
	  
		$result = pmdb::connect()->select( DB . 'members', 'ichat', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
	  
	  		while($r = $result->fetch_object()) {
	  			
				return $r->ichat;	  
	  
	  }
			
	}
	
	/**
     * Retrieve a user's DOB.
     *
	 * @access public
     * @param string $username Grab the username of a user.
     * @return object A user's DOB.
     */
	public function get_dob($username) {
		
		$result = pmdb::connect()->select( DB . 'members', 'dob', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
		
			while($r = $result->fetch_object($result)) {
				
				return $r->dob;

			}
	
	}
	
	/**
     * Retrieve a user's Bio.
     *
	 * @access public
     * @param string $username Grab the username of a user.
     * @return object A user's Bio.
     */
	public function get_bio($username) {
		
		$result = pmdb::connect()->select( DB . 'members', 'bio', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
		
		if($result) {
			
			while($r = $result->fetch_object()) {
				
				return $r->bio;

			}
			
		}
	
	}
	
	/**
     * View user's profile if account is active.
     *
	 * @access public
     * @param string $username Grab the username of a user.
     * @return object A user's active profile.
     */
	public function profile_active($username) {
		
		$result = pmdb::connect()->select( DB . 'members', '*', 'username = "' . $username . '" AND active = "1"', null ) or die(pmdb::connect()->is_error());
		
			while($r = $result->fetch_object()) {
				
				return $r->active;

			}

	}
	
	/**
     * Retrieve a Members Directory.
     *
	 * @access public
     * 
     * @return A list of Members in the directory.
     */
	public function get_members_directory() {

		//check if the starting row variable was passed in the URL or not
		if (!isset($_GET['startrow']) or !is_numeric($_GET['startrow'])) {
  		//we give the value of the starting row to 0 because nothing was found in URL
  		$startrow = 0;
		//otherwise we take the value from the URL
		} else {
  		$startrow = (int)$_GET['startrow'];
		}

		$results = pmdb::connect()->select( DB . 'members', '*', 'privacy = "0" AND active = "1"', 'last_name ASC LIMIT "' . $startrow . '",10' ) or die(pmdb::connect()->is_error());

			if($results) {
        	while($r = $results->fetch_object()) {
        		
        		$mem = '<tr><td>' . get_user_avatar($r->username, $r->email, 36);
        		$mem .= '</td> <td><a href="' . PM_URI . '/profile/profile.php?username=' . $r->username . '">' . $r->first_name . ' ' . $r->last_name . '</a></td> <td>' . $r->phone . '</td>';
        		$mem .= '<td>' . clickable_link($r->email) . '</td></tr>';
				
				echo $mem;
			}

			echo '<tr><td colspan="4"><a href="members.php?startrow='.($startrow+10).'">Next</a>';
			
			$prev = $startrow - 10;

			//only print a "Previous" link if a "Next" was clicked
			if ($prev >= 0) {
    		echo '&nbsp;<a href="members.php?startrow='.$prev.'">Previous</a>';
			}

    		echo '</td></tr>';
			
		}

	}

	public function get_my_projects() {
		
		$results = pmdb::connect()->select( DB . 'project_members, ' . DB . 'projects, ' . DB . 'project_types', '*', 'pm_user = "' . $_SESSION['username'] . '" AND pp_id = p_id AND pt_id = ptype_id', null ) or die(pmdb::connect()->is_error());
		
		if($results->num_rows > 0) {
			
			while($r = $results->fetch_object()) {
				
				return "<tbody><tr><td><a href=" . PM_URI . '/projects/project.php?p_id=' . $r->p_id . ">" . $r->project_name . "</a></td> <td>" . $r->project_type . "</td></tr></tbody>";

			}
		} else {
			_e( '<tbody><tr><td><div class="error">Currently, you are not subscribed to any projects.</div></td></tr></tbody>' );
		}
		
	}
	
	public function get_private_message() {
 	
		$gpm = pmdb::connect()->select( DB . 'private_messages', '*', 'receiver = "' . $_SESSION['username'] . '"', 'LIMIT 4' ) or die(pmdb::connect()->is_error());

			while($r = $gpm->fetch_array()) {
		
			echo '<tr><td>' . get_user_avatar($r['sender'],$r['email'],25) .  '&nbsp;&nbsp;<a href="'.PM_URI.'/pms/viewmsg.php?msg_id=' . $r['id'] . '">' . $r['subject'] . '</a> <br /><span>from ' . get_name($r['sender']) . ', sent @ '.date('M j, g:i a', strtotime($r['date'])).'</span></td></tr>';
		}
 	
	}

	public function get_user_info($username,$field) {
	   $results = pmdb::connect()->get_row( "SELECT " . $field . " FROM " . DB . "members WHERE username = '" . $username . "'" ) or die(pmdb::connect()->is_error());
       		return $results->$field;
	}
		
}