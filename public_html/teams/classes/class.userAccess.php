<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

 /**
 * User Access Class
 *
 * @package ProjectPress User Access Class
 * @author Joshua Parker
 * @link http://www.joshparker.us/
 * @since 3.0
 */

class userAccess extends User {
	
    public function __construct() {
        parent::__construct('User');
    }
	
	/**
	 * The is_user_logged_in method selects a user
	 * from the database based on if a hashed token
	 * cookie is present on the user's computer. If so,
	 * then we use the pm_check_password function to check
	 * the plaintext username against the hashed cookie name
	 * and the unique (int) user_id against the hashed cookie id.
	 * If it comes back true, then the user will be logged in
	 * automatically.
	 */
	public function is_user_logged_in() {
		$cookie_name = $_COOKIE['pm_cookname'];
			$cookie_name = str_replace('ppname_', '', $cookie_name);
		$cookie_id = $_COOKIE['pm_cookid'];
			$cookie_id = str_replace('ppid_', '', $cookie_id);
		
		$results = pmdb::connect()->get_row( "SELECT user_id, username, auth_token FROM ". DB ."members WHERE auth_token = '" . $cookie_id . "'" );
		
		if($_SESSION['logged'] != true) {
			if(isset($_COOKIE['pm_cookname']) && isset($_COOKIE['pm_cookid'])) {
				if(!userAccess::pm_authenticate_cookie($results->username, $cookie_name, $results->user_id) && 
					!userAccess::pm_authenticate_cookie($results->user_id, $cookie_id, $results->user_id)) {
					unset($_SESSION['logged']);
	  				unset($_SESSION['username']);
        			unset($_SESSION['userID']);
					pm_redirect(PM_URI . '/pm-login.php');
				} else {
					$_SESSION['logged'] = true;
					$_SESSION['username'] = $results->username;
					$_SESSION['userID'] = $results->user_id;
					pm_redirect(PM_URI . '/index.php');
				}
			}
		}

   		/* Username and user_id have been set and not null */
      	if(isset($_SESSION['username']) && isset($_SESSION['userID']) && $_SESSION['username'] != '') {
      		return $_SESSION['logged'];
			pm_redirect(PM_URI . 'index.php');
	  	} else {
	  		unset($_SESSION['logged']);
	  		unset($_SESSION['username']);
        	unset($_SESSION['userID']);
			pm_redirect(PM_URI . '/pm-login.php');
	  	}
	}
	
	/**
	 * Logs in user and sets the session and cookie.
	 *
	 * @since 2.1
	 * @uses apply_filter() Calls 'login' filter.
	 * @param string $username Username entered by the user
	 * @param string $password Password entered by the user
	 * @param string $remember Remember sets longer cookie session (optional)
	 * @return bool True if $username and $password exist
	 * 
	 */
	public function pm_login($username, $password, $remember = NULL) {
		$user = strtolower(pmdb::connect()->escape($username));
		$pass = pmdb::connect()->escape($password);
		
		/* Use to set cookie session for domain. */
        $cookiedomain = $_SERVER['SERVER_NAME']; 
        $cookiedomain = str_replace('www.', '', $cookiedomain);
		
		$results = pmdb::connect()->get_row( "SELECT user_id, username, password, auth_token FROM ". DB ."members WHERE username = '$user'" );
		
		if(pm_check_password( $pass, $results->password, $results->user_id )) {
			
				do_action( 'pm_login_form_script' );

			if(isset($remember)) {
				/* Insert the auth_token into the database based on user_id. */
				pmdb::connect()->update( DB . 'members', array( 'auth_token' => userAccess::pm_hash_cookie($results->user_id) ), array( 'username', $results->username ) );
				
				/* Select the auth_token after it has been inserted for the user. */
				$sql = pmdb::connect()->select( DB . 'members', 'auth_token', 'username = "' . $results->username . '"', null );
				$result = $sql->fetch_object();
				
				/* Now we can set login our cookies. */
      			setcookie("pm_cookname", 'ppname_' . userAccess::pm_hash_cookie($results->username), time()+COOKIE_EXPIRE, COOKIE_PATH, $cookiedomain);
      			setcookie("pm_cookid", 'ppid_' . $result->auth_token, time()+COOKIE_EXPIRE, COOKIE_PATH, $cookiedomain);
   			}
			
			$_SESSION['logged'] = true; // Sets the logged in session.
			$_SESSION['username'] = $results->username; // Sets the username session.
			$_SESSION['userID'] = $results->user_id; // Sets the user_id session.
			
			pm_redirect(PM_URI . "/index.php");
			
		}
		
	      return apply_filter( 'login', $username, $password, $remember );
	}
	
	/**
	 * Adds error to the css class of the
	 * login fields.
	 */
	public function login_error() {
		if(isset($_POST['login'])) {
			if(pm_check_password($password, $hash) === false) {
				echo ' error';
			}
		}
	}

	public function pm_logout() {
		if(isset($_COOKIE['pm_cookname']) && isset($_COOKIE['pm_cookid'])){
        	setcookie("pm_cookname", "", time()-COOKIE_EXPIRE, COOKIE_PATH);
         	setcookie("pm_cookid", "", 	 time()-COOKIE_EXPIRE, COOKIE_PATH);
			pmdb::connect()->update( DB . 'members', array( 'auth_token' => '' ), array( 'username', is_session_set('username') ) );
      	}
				
		session_unset();
		session_destroy();
		pm_redirect(PM_URI . '/pm-login.php');
	}
	
	public function pm_hash_cookie($cookie) {
		// By default, use the portable hash from phpass
		$ck_hasher = new PasswordHash(8, TRUE);

			return $ck_hasher->HashPassword($cookie);
	}
	 
	public function pm_authenticate_cookie($cookie, $cookiehash, $user_id = '') {

		$ck_hasher = new PasswordHash(8, TRUE);

		$check = $ck_hasher->CheckPassword($cookie, $cookiehash);

			return apply_filter('authenticate_cookie', $check, $cookie, $cookiehash, $user_id);
	}
		
}