<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress access control levels
 *
 * @package ProjectPress
 * @since 2.1
 */

	class ACL
	{
		var $perms = array();		//Array : Stores the permissions for the user
		var $userID = 0;			//Integer : Stores the ID of the current user
		var $userRoles = array();	//Array : Stores the roles of the current user
		
		function __constructor($userID = '')
		{
			if ($userID != '')
			{
				$this->userID = floatval($userID);
			} else {
				$this->userID = floatval(isset($_SESSION['userID']));
			}
			$this->userRoles = $this->getUserRoles('ids');
			$this->buildACL();
		}
		
		function ACL($userID = '')
		{
			$this->__constructor($userID);
			//crutch for PHP4 setups
		}
		
		function buildACL()
		{
			//first, get the rules for the user's role
			if (count($this->userRoles) > 0)
			{
				$this->perms = array_merge($this->perms,$this->getRolePerms($this->userRoles));
			}
			//then, get the individual user permissions
			$this->perms = array_merge($this->perms,$this->getUserPerms($this->userID));
		}
		
		function getPermKeyFromID($permID)
		{
			//$strSQL = pmdb::connect()->query("SELECT permKey FROM " . DB . "permissions WHERE ID = " . floatval($permID) . " LIMIT 1");
			$strSQL = pmdb::connect()->select( DB . 'permissions', 'permkey', 'ID = "' . floatval($permID) . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
			$row = $strSQL->fetch_array();
			return $row[0];
		}
		
		function getPermNameFromID($permID)
		{
			//$strSQL = pmdb::connect()->query("SELECT permName FROM " . DB . "permissions WHERE ID = " . floatval($permID) . " LIMIT 1");
			$strSQL = pmdb::connect()->select( DB . 'permissions', 'permName', 'ID = "' . floatval($permID) . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
			$row = $strSQL->fetch_array();
			return $row[0];
		}
		
		function getRoleNameFromID($roleID)
		{
			//$strSQL = pmdb::connect()->query("SELECT roleName FROM " . DB . "roles WHERE ID = " . floatval($roleID) . " LIMIT 1");
			$strSQL = pmdb::connect()->select( DB . 'roles', 'roleName', 'ID = "' . floatval($roleID) . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
			$row = $strSQL->fetch_array();
			return $row[0];
		}
		
		function getUserRoles()
		{
			//$strSQL = pmdb::connect()->query("SELECT * FROM " . DB . "user_roles WHERE userID = " . floatval($this->userID) . " ORDER BY addDate ASC");
			$strSQL = pmdb::connect()->select( DB . 'user_roles', '*', 'userID = "' . floatval($this->userID) . '"', 'addDate ASC' ) or die(pmdb::connect()->is_error());
			$resp = array();
			while($row = $strSQL->fetch_array())
			{
				$resp[] = $row['roleID'];
			}
			return $resp;
		}
		
		function getAllRoles($format='ids')
		{
			$format = strtolower($format);
			//$strSQL = pmdb::connect()->query("SELECT * FROM " . DB . "roles ORDER BY roleName ASC");
			$strSQL = pmdb::connect()->select( DB . 'roles', '*', null, 'roleName ASC' ) or die(pmdb::connect()->is_error());
			$resp = array();
			while($row = $strSQL->fetch_array())
			{
				if ($format == 'full')
				{
					$resp[] = array("ID" => $row['ID'],"Name" => $row['roleName']);
				} else {
					$resp[] = $row['ID'];
				}
			}
			return $resp;
		}
		
		function getAllPerms($format='ids')
		{
			$format = strtolower($format);
			//$strSQL = pmdb::connect()->query("SELECT * FROM " . DB . "permissions ORDER BY permName ASC");
			$strSQL = pmdb::connect()->select( DB . 'permissions', '*', null, 'permName ASC' ) or die(pmdb::connect()->is_error());
			$resp = array();
			while($row = $strSQL->fetch_assoc())
			{
				if ($format == 'full')
				{
					$resp[$row['permKey']] = array('ID' => $row['ID'], 'Name' => $row['permName'], 'Key' => $row['permKey']);
				} else {
					$resp[] = $row['ID'];
				}
			}
			return $resp;
		}

		function getRolePerms($role)
		{
			if (is_array($role))
			{
				//$roleSQL = pmdb::connect()->query("SELECT * FROM " . DB . "role_perms WHERE roleID IN (" . implode(",",$role) . ") ORDER BY ID ASC");
				$roleSQL = pmdb::connect()->select( DB . 'role_perms', '*', 'roleID IN ("' . implode(",",$role) . '")', 'ID ASC' ) or die(pmdb::connect()->is_error());
			} else {
				//$roleSQL = pmdb::connect()->query("SELECT * FROM " . DB . "role_perms WHERE roleID = " . floatval($role) . " ORDER BY ID ASC");
				$roleSQL = pmdb::connect()->select( DB . 'role_perms', '*', 'roleID = "' . floatval($role) . '"', 'ID ASC' ) or die(pmdb::connect()->is_error());
			}
			$perms = array();
			while($row = $roleSQL->fetch_assoc())
			{
				$pK = strtolower($this->getPermKeyFromID($row['permID']));
				if ($pK == '') { continue; }
				if ($row['value'] === '1') {
					$hP = true;
				} else {
					$hP = false;
				}
				$perms[$pK] = array('perm' => $pK,'inheritted' => true,'value' => $hP,'Name' => $this->getPermNameFromID($row['permID']),'ID' => $row['permID']);
			}
			return $perms;
		}
		
		function getUserPerms($userID)
		{
			//$strSQL = pmdb::connect()->query("SELECT * FROM " . DB . "user_perms WHERE userID = " . floatval($userID) . " ORDER BY addDate ASC");
			$strSQL = pmdb::connect()->select( DB . 'user_perms', '*', 'userID = "' . floatval($userID) . '"', 'addDate ASC' ) or die(pmdb::connect()->is_error());
			$perms = array();
			while($row = $strSQL->fetch_assoc())
			{
				$pK = strtolower($this->getPermKeyFromID($row['permID']));
				if ($pK == '') { continue; }
				if ($row['value'] == '1') {
					$hP = true;
				} else {
					$hP = false;
				}
				$perms[$pK] = array('perm' => $pK,'inheritted' => false,'value' => $hP,'Name' => $this->getPermNameFromID($row['permID']),'ID' => $row['permID']);
			}
			return $perms;
		}
		
		function userHasRole($roleID)
		{
			foreach($this->userRoles as $k => $v)
			{
				if (floatval($v) === floatval($roleID))
				{
					return true;
				}
			}
			return false;
		}
		
		function hasPermission($permKey)
		{
			$permKey = strtolower($permKey);
			if (array_key_exists($permKey,$this->perms))
			{
				if ($this->perms[$permKey]['value'] === '1' || $this->perms[$permKey]['value'] === true)
				{
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		
		function getUser($userID)
		{
			//$strSQL = pmdb::connect()->query("SELECT username FROM " . DB . "members WHERE user_id = " . floatval($userID) . " LIMIT 1");
			$strSQL = pmdb::connect()->select( DB . 'members', 'username', 'user_id = "' . floatval($userID) . '"', null, ' LIMIT 1' ) or die(pmdb::connect()->is_error());
			$row = $strSQL->fetch_array();
			return $row[0];
		}
	}