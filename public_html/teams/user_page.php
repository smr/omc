<?php 
/**
 * ProjectPress plugins
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(__FILE__) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

// Handle plugin user pages
if( isset( $_GET['page'] ) && !empty( $_GET['page'] ) ) {
	plugin_user_page( $_GET['page'] );
}

include(PM_DIR . 'pm-includes/footer.php');