<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress deprecated functions
 *
 * @package ProjectPress
 * @since 3.0
 */

/**
 * This constant is deprecated not that
 * a new automatic upgrade system has
 * been coded and implemented.
 * 
 * Define the update server.
 */
define('UPDATE_SERVER', 'http://dev.projectpress.org');

/**
 * This constant is now deprecated and replaced by the
 * upgrade method PP::upgrade(0);
 * 
 * Where the latest version of ProjectPress is define.
 */
define('LATEST_VERSION', 'http://projectpress.org/latestversion.txt');
 
	/* replaced by the ACL class */
	function userLevel($level, $active, $echo=true) {
	$level = array('1'=>"1",'2'=>"2",'3'=>"3",'4'=>"4",'5'=>"5");

        	$string = '';

        	foreach($level as $k => $v){
            	$s = ($active == $k)? ' selected="selected"' : '';
            	$string .= '<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";   
        	}

        	if($echo)   echo $string;
        	else        return $string;
	}
	
	/* replaced by the User::instance()->get_user_info() method */
	function get_user_info($username,$field) {
		$result = pmdb::connect()->select( DB . 'members', $field, 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
       		while($r = $result->fetch_object()) {
            	$info = $r->$field;
       		return $info;
       	}
	}
	
	/* replaced by the User::instance()->get_email() method */
	function get_email($username) {
		$results = pmdb::connect()->get_row( DB . 'members', 'email', 'username = "' . $username . '"', null ) or die(pmdb::connect()->is_error());
		return $results->email;
	}
	
	/* replaced by the User::instance()->get_name() method */
	function get_name($username) {
		$results = pmdb::connect()->get_row( "SELECT first_name, last_name FROM " . DB . "members WHERE username = '" . $username . "'" ) or die(pmdb::connect()->is_error());
			return $results->first_name . ' ' . $results->last_name;	
	}
	
	/**
	  * Logs in user and sets the session and cookie.
	  *
	  * @since 2.1
	  * @uses apply_filter() Calls 'login' filter.
	  * @param string $username Username entered by the user
	  * @param string $password Password entered by the user
	  * @param string $remember Remember sets cookie session
	  * @return bool True if $username and $password exist
	  * 
	  */
	 function pm_login($username, $password, $remember = NULL) {
		
		$user = strtolower(pmdb::connect()->escape($username));
		$pass = pmdb::connect()->escape($password);
		
		$results = pmdb::connect()->get_row( "SELECT user_id, username, password FROM ". DB ."members WHERE username = '$user'" );
		
		// Use to set cookie session for domain.
		$cookiedomain = $_SERVER['SERVER_NAME']; 
		$cookiedomain = str_replace('www.', '', $cookiedomain);

		if(pm_check_password( $pass, $results->password, $results->user_id )) {
			
				do_action( 'pm_login_form_script' );
				
				$_SESSION['logged'] = 1; // Sets the session.
				$_SESSION['username'] = $results->username; // Sets the username session.
				$_SESSION['userID'] = $results->user_id;
				$_SESSION['remember_me'] = $remember; // Sets a remember me cookie if remember me is checked.

			if(isset($remember)) {
      			setcookie("pm_cookname", $user, time()+60*60*24*365, "/", $cookiedomain);
      			setcookie("pm_cookpass", pm_hash_password($password), time()+60*60*24*365, "/", $cookiedomain);
				pm_redirect(PM_URI . "/index.php");
   			} else {
				setcookie("pm_cookname", $user, time()+3600*24, "/", $cookiedomain);
      			setcookie("pm_cookpass", pm_hash_password($password), time()+3600*24, "/", $cookiedomain);
				pm_redirect(PM_URI . "/index.php");
			}
			
		}
		
	      return apply_filter( 'login', $username, $password, $remember );
	 }

	 /**
	 * This function was being used for the collab feed
	 * and the wall but is now deprecated and replaced
	 * by the method pmdb:connect()->escape().
	 */
	function checkValues($value) {
	
		 	$value = trim($value);
		 
			if (get_magic_quotes_gpc()) {
				$value = stripslashes($value);
			}
		
		 	$value = strtr($value,array_flip(get_html_translation_table(HTML_ENTITIES)));
		
		 	$value = strip_tags($value);
			$value = pmdb::connect()->escape($value);
			$value = htmlspecialchars ($value);
			return $value;
	}
	
	/**
	 * This function is now deprecated and has been replaced
	 * by the upgrade method PP::upgrade(0);
	 * 
	 * Checks the remote server for the current version of ProjectPress.
	 */
	function get_latest_version($file) {
		$ch = curl_init();
  		$timeout = 5;
  		curl_setopt($ch,CURLOPT_URL,$file);
  		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  		$data = curl_exec($ch);
  		curl_close($ch);
  		return $data;
	}