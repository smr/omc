<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress functions for projects
 *
 * @package ProjectPress
 * @since 3.0
 */

	function get_project_tabs() {
		$pmem = pmdb::connect()->select( DB . 'project_members', '*', 'pp_id = "' . $_GET['p_id'] . '" AND pm_user = "' . is_session_set('username') . '"', null ) or die(pmdb::connect()->is_error());
		$plead = pmdb::connect()->select( DB . 'project_leaders', '*', 'p_id = "' . $_GET['p_id'] . '" AND pl_user = "' . is_session_set('username') . '"', null ) or die(pmdb::connect()->is_error());
		if($pmem->num_rows > 0 || $plead->num_rows > 0 || jury_test() == true || admin_test() == true) {
		?>
		
				<div id="tabs">
					<ul>
						<li <?php if (active_link() == "project.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/projects/project.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><span>
                                <?php if(jury_test()==TRUE)
                                        _e( _( 'Jury' ) );
                                      else
                                        _e( _( 'Project' ) ); ?>
                            </span></a>
						</li>
						<li <?php if (active_link() == "discussions.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>
							<?php if (active_link() == "add_ptopic.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/projects/forum/discussions.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><span>
                                <?php if(jury_test()==TRUE)
                                        _e( _( 'Jury forum' ) );
                                      else
                                        _e( _( 'Project forum' ) ); ?>
                            </span></a>
						</li>
						<li <?php if (active_link() == "contact.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/projects/contact.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><span><?php _e( _( 'Email' ) ); ?></span></a>
						</li>
						<li <?php if (active_link() == "filemanager.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/projects/filemanager.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><span><?php _e( _( 'Docs' ) ); ?></span></a>
						</li>
						<li <?php if (active_link() == "project_members.php?p_id=".$_GET['p_id']) _e( "class='active_link'" );?>>
							<a href="<?php _e( PM_URI ); ?>/projects/project_members.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><span>
                                <?php if(jury_test()==TRUE)
                                        _e( _( 'Jury members' ) );
                                      else
                                        _e( _( 'Team members' ) ); ?>
                            </span></a>
						</li>
						<?php do_action( 'project_tab' ); ?>
					</ul>
				</div>
		
	<?php
		}
	}

	function get_project_member_count() {
		$pm = pmdb::connect()->select( DB . 'project_members', 'COUNT(pm_user)', 'pp_id = "' . $_GET['p_id'] . '"', null ) or die(pmdb::connect()->is_error());

		while($rpm = $pm->fetch_array()) {
			if($rpm['COUNT(pm_user)'] > 0) {
				echo "<font color='#f00'>". $rpm['COUNT(pm_user)']."</font>"; 
			} else {
				echo $rpm['COUNT(pm_user)'];
			}
		}
	}
	
	function get_project_meta($id,$field) {
        $result = pmdb::connect()->query("SELECT " . $field . " FROM " . DB . "projects WHERE p_id = '" . $id . "'") or die(pmdb::connect()->is_error());
       		while($r = $result->fetch_object()) {
               $info = $r->$field;
       		return $info;
    	}
	}
	
	function get_project_leader_meta($field) {
		$result = pmdb::connect()->query("SELECT" . $field . " FROM " . DB . "project_leaders WHERE p_id = '" . $_GET['p_id'] . "'") or die(pmdb::connect()->is_error());
			while($r = $result->fetch_object()) {
               $plead = $r->$field;
       		echo $plead;
    	}
	}
	
	function getFileType($extension) {
		$images = array('jpg', 'gif', 'png', 'bmp');
		$docs 	= array('txt', 'rtf', 'doc', 'pdf');
		$apps 	= array('zip', 'rar', 'tar');
	
		if(in_array($extension, $images)) return "Images";
		if(in_array($extension, $docs)) return "Documents";
		if(in_array($extension, $apps)) return "Applications";
		return "";
	}

	function formatBytes($bytes, $precision = 2) { 
    	$units = array('B', 'KB', 'MB', 'GB', 'TB'); 
   
    	$bytes = max($bytes, 0); 
    	$pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    	$pow = min($pow, count($units) - 1); 
   
    	$bytes /= pow(1024, $pow); 
   
    	return round($bytes, $precision) . ' ' . $units[$pow]; 
	}
	
	function is_id_set($id, $getID, $redirect) {
		if(!isset($id) || $id != $getID) {
			 pm_redirect( PM_URI . $redirect );
		}
	}