<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress functions
 *
 * @package ProjectPress
 * @since 2.0
 */

// Define the current version of ProjectPress.
define('CURRENT_VERSION', '3.0.6');

// Record the current microtime
$GLOBALS['REQUEST_MICROTIME'] = microtime( TRUE );

	function showDayWeekDrop($dayweek, $active, $echo=true) {
	$dayweek = array('Sunday'=>"Sunday",'Monday'=>"Monday", 'Tuesday'=>"Tuesday", 'Wednesday'=>"Wednesday", 'Thursday'=>"Thursday", 'Friday'=>"Friday", 'Saturday'=>"Satuday");

        	$string = '';

        	foreach($dayweek as $k => $v){
            	$s = ($active == $k)? ' selected="selected"' : '';
            	$string .= '<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";   
        	}

        	if($echo)   echo $string;
        	else        return $string;
	}

	function showMonthDrop($month, $active, $echo=true) {
	$month = array('01'=>"January",'02'=>"February",'03'=>"March",'04'=>"April",'05'=>"May",'06'=>"June",'07'=>"July",'08'=>"August",'09'=>"September",'10'=>"October",'11'=>"November",'12'=>"December");

        	$string = '';

        	foreach($month as $k => $v){
            	$s = ($active == $k)? ' selected="selected"' : '';
            	$string .= '<option value="'.$k.'"'.$s.'>'.$v.'</option>'."\n";   
        	}

        	if($echo)   echo $string;
        	else        return $string;
	}

	function get_user_avatar($username, $email, $size = 100) {
		if (defined('UPLOAD_AVATAR')) {
			$filename = PM_DIR . 'profile/avatars/' . $username . '.gif';
		if(file_exists($filename)) {
			$mypicture = getimagesize(PM_URI . '/profile/avatars/' . $username . '.gif');
			return '<img src="' . PM_URI . '/profile/avatars/' . $username . '.gif" ' . imgResize($mypicture[1],  $mypicture[1], $size) . ' class="avatar" alt="' . $username . '"/>';
		} else {
			$mypicture = getimagesize("http://www.gravatar.com/avatar.php?gravatar_id=" . md5($email));
			return '<img src="http://www.gravatar.com/avatar.php?gravatar_id=' . md5($email) . '" ' . imgResize($mypicture[1],  $mypicture[1], $size) . ' class="avatar" alt="' . $username . '"/>';
		}
		} else {
			$mypicture = getimagesize("http://www.gravatar.com/avatar.php?gravatar_id=".md5($email));
			return '<img src="http://www.gravatar.com/avatar.php?gravatar_id=' . md5($email) . '" ' . imgResize($mypicture[1],  $mypicture[1], $size) . ' class="avatar" alt="' . $username . '"/>';
		}
		return apply_filter('user_avatar', $username, $email, $size);
	}

	function imgResize($width, $height, $target) {
		//takes the larger size of the width and height and applies the formula. Your function is designed to work with any image in any size.
		if ($width > $height) {
			$percentage = ($target / $width);
		} else {
			$percentage = ($target / $height);
		}

		//gets the new value and applies the percentage, then rounds the value
		$width = round($width * $percentage);
		$height = round($height * $percentage);
		//returns the new sizes in html image tag format...this is so you can plug this function inside an image tag so that it will set the image to the correct size, without putting a whole script into the tag.
		return "width=\"$width\" height=\"$height\"";
	}

	function truncNewsText($string, $limit, $break=".", $pad=" ...") {
		// return with no change if string is shorter than $limit
		if(strlen($string) <= $limit) return $string;
	
		// is $break present between $limit and the end of the string?  
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint) . $pad;
			}
		}
		return $string;
		return apply_filter('NewsText',$string,$limit,$break,$pad='');
	}	

	function get_news() {
		$sql = pmdb::connect()->select( DB . 'news', '*', null, 'dtime DESC LIMIT 5' ) or die(pmdb::connect()->is_error());
	
		if ($sql->num_rows > 0) {
			while($r = $sql->fetch_object()){
			_e( '<li><img src="'.PM_URI.'/images/newspapers.png" alt="'.$r->title.'" /><a href="'.PM_URI.'/news/read_news.php?id='.$r->newsid.'">'.$r->title.'</a> <br /> <span class="news-info">by '.$r->author.' @ '.date('M j, g:i a', strtotime($r->dtime)).'</span></li>' );
	}
		} else {
			_e( '<li>No recent news.</li>' );
		}
	}

	function get_forum_replies() {
		
		$sql = pmdb::connect()->select( DB. 'forum_topics, ' .  DB . 'forum_posts', '*', DB . 'forum_topics.id = ' . DB . 'forum_posts.topic_id', 'p_datetime DESC LIMIT 5' ) or die(pmdb::connect()->is_error());

		if ($sql->num_rows > 0) {
			while($r = $sql->fetch_object()){
				echo '<li><img src="'.PM_URI.'/images/comment.png" alt="'.$r->topic.'" /><a href="'.PM_URI.'/forum/view_topic.php?id='.$r->id.'">'.$r->topic.'</a> <br /> <span class="forum-info">by '.$r->p_user.' @ '.date('M j, g:i a', strtotime($r->p_datetime)).'</span></li>';
			}
		} else {
			echo '<li>No forum replies.</li>';
		}
	}

	function get_sidebar() {
		include(PM_DIR . 'pm-includes/sidebar.php');
	}	

	function userOnline($user) {
		if (isset($_SESSION['username'])) { // If the user is logged in, good, if not, they become an ip address 
			$username = $_SESSION['username']; // Username is $_SESSION['username']; 
		} else { 
			$username = $_SERVER['REMOTE_ADDR']; // Username is IP Address 
		}

			$time = time(); // Current time 
			$previous = "120"; // Time to check in seconds 

			$timeout = $time-$previous; // Timeout=time - 2two minutes 
			$query = "SELECT * FROM " . DB . "online WHERE o_user='$username' AND timeout > '$timeout'"; // Have you been here in the past two minutes? 
			$verify = pmdb::connect()->query($query); #Execute query 

			$row_verify = $verify->fetch_assoc(); // Check if you have been here in two minutes

		if (!isset($row_verify['o_user'])) { // See if you were found 
			$query = "INSERT INTO " . DB . "online (o_user, timeout) VALUES ('$username', '$time')"; // Put you on the online list 
			$insert = pmdb::connect()->query($query);
		}

			$q = pmdb::connect()->query("SELECT * FROM " . DB . "online WHERE o_user = '".$user."' AND timeout > '$timeout' LIMIT 1");
			$result = $q->fetch_array();

			$username = $_GET['username'];
		if($result == $verify || !$result) {
			return '<img src="'.PM_URI.'/images/offline.gif" alt="offline" />';
		} else {
			return "<a href=\"javascript:void(0)\" onclick=\"javascript:chatWith('$user')\"><img src=\"".PM_URI."/images/online.gif\" alt=\"online\" /></a>";
		}
	}

	function unreadInboxCount() {
		$rec = "SELECT * from " . DB . "messages WHERE receiver = '".$_SESSION['username']."'";
		$result = pmdb::connect()->query($rec);
		$mes = $result->fetch_array();

		$sql = "SELECT COUNT(received) FROM " . DB . "messages WHERE receiver = '".$_SESSION['username']."' AND received = '0'";
		$result = pmdb::connect()->query($sql);
			while($row = $result->fetch_array()) {
				if($row['COUNT(received)'] > 0) {
					echo "<font color='#f00'>(". $row['COUNT(received)'].")</font>"; 
				} else {
					echo "(". $row['COUNT(received)'].")";
			}
		}
	}
	
	// Prints out an error message to let the user know that he/she needs to enter a secure password.
	function update_password_notice($username) {
		$results = pmdb::connect()->get_row( "SELECT username, password FROM " . DB . "members WHERE username = '" . $username . "'" ) or die(pmdb::connect()->is_error());
		
		if(empty($results->password)) {
			$pass = '<div class="error"><b>Important Notice:</b> your password is blank. For security reasons, please visit <a href="'. PM_URI . '/profile/changepassword.php">this page</a> to enter a secure password.</div>';
			$pass = apply_filter('password_notice',$pass);
			echo $pass;
		}
	}

	function active_link() {
		return basename($_SERVER["REQUEST_URI"]);
	}

	function ob_postprocess($buffer) {
		// check if the browser accepts gzip encoding. Most do, but just in case
		if(strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') != FALSE) {
			$buffer = gzencode($buffer);
			header('Content-Encoding: gzip');
		}
		return $buffer;
		return apply_filter('postprocess', $buffer);
	}

	function clickable_link($text = '') {
		$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
		$ret = ' ' . $text;
		$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
		
		$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
		$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
		$ret = substr($ret, 1);
		return $ret;
	}

	// An alternative function of using the echo command.
	function _e($translate) {
		echo $translate;
	}
	
	/*function _t( $text, $domain = 'default' ) {
		echo translate( $text, $domain );
	}*/

	function display_errors() {
		return ini_set('display_errors', 1);
		return ini_set('log_errors', 1);
		return ini_set('error_log', PM_DIR . 'pm-content/error_log.txt');
		return error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
	}
	
	function is_valid_username($username) {
		if (preg_match('/^[a-z\d_]{5,20}$/i', $username)) {
			return true;
		} else {
			return false;
		}
	}
	
	function is_valid_email($email){
  		return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
	}
	
	function is_valid_password($x,$y) {
		if(empty($x) || empty($y) ) { return false; }
			if (strlen($x) < 4 || strlen($y) < 4) { return false; }

		if (strcmp($x,$y) != 0) {
 			return false;
		} 
			return true;
	}
	
	function generate_user_password($length = 7) {
  		$password = "";
  		$possible = "0123456789bcdfghjkmnpqrstvwxyz"; //no vowels
  
  			$i = 0; 
    
  				while ($i < $length) { 
    				$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
    				if (!strstr($password, $char)) { 
      					$password .= $char;
      				$i++;
    			}

  		}

  		return $password;
		return apply_filter('user_password',$possible,$length);
	}

	// Read an option from DB. Return value or $default if not found
	function get_pm_option( $option_name, $default = false ) {
	
		// Allow plugins to short-circuit options
		$pre = apply_filter( 'pre_option_'.$option_name, false );
		if ( false !== $pre )
			return $pre;

		if ( !isset( pmdb::connect()->option[$option_name] ) ) {
			$option_name = pmdb::connect()->escape( $option_name );
			$results = pmdb::connect()->get_row( "SELECT `option_value` FROM " . DB . "options WHERE `option_name` = '$option_name'" );
			if ( is_object( $results) ) {
				$value = $results->option_value;
			} else { // option does not exist, so we must cache its non-existence
				$value = $default;
			}
			pmdb::connect()->option[$option_name] = maybe_unserialize( $value );
		}

		return apply_filter( 'get_option_'.$option_name, pmdb::connect()->option[$option_name] );
	}

	// Update (add if doesn't exist) an option to DB
	function update_pm_option( $option_name, $newvalue ) {

		$safe_option_name = pmdb::connect()->escape( $option_name );

		$oldvalue = get_pm_option( $safe_option_name );

		// If the new and old values are the same, no need to update.
		if ( $newvalue === $oldvalue )
			return false;

		if ( false === $oldvalue ) {
			add_pm_option( $option_name, $newvalue );
			return true;
		}

		$_newvalue = pmdb::connect()->escape( maybe_serialize( $newvalue ) );
	
		do_action( 'update_option', $option_name, $oldvalue, $newvalue );

		pmdb::connect()->query( "UPDATE " . DB . "options SET `option_value` = '$_newvalue' WHERE `option_name` = '$option_name'");

		if ( pmdb::connect()->rows_affected == 1 ) {
			pmdb::connect()->option[$option_name] = $newvalue;
			return true;
		}
		return false;
	}

	// Add an option to the DB
	function add_pm_option( $name, $value = '' ) {
		$safe_name = pmdb::connect()->escape( $name );

		// Make sure the option doesn't already exist
		if ( false !== get_pm_option( $safe_name ) )
			return;

		$_value = pmdb::connect()->escape( maybe_serialize( $value ) );

		do_action( 'add_option', $safe_name, $_value );

		pmdb::connect()->query( "INSERT INTO " . DB . "options (`option_name`, `option_value`) VALUES ('$name', '$_value')" );
		pmdb::connect()->option[$name] = $value;
		return;
	}

	// Delete an option from the DB
	function delete_pm_option( $name ) {
		$name = pmdb::connect()->escape( $name );

		// Get the ID, if no ID then return
		$results = pmdb::connect()->get_row( "SELECT option_id FROM " . DB . "options WHERE `option_name` = '$name'" );
		if ( is_null($results) || !$results->option_id )
			return false;
		
		do_action( 'delete_option', $option_name );
		
		pmdb::connect()->query( "DELETE FROM " . DB . "options WHERE `option_name` = '$name'" );
		return true;
	}

	// Serialize data if needed. Stolen from WordPress
	function maybe_serialize( $data ) {
		if ( is_array( $data ) || is_object( $data ) )
			return serialize( $data );

		if ( is_serialized( $data ) )
			return serialize( $data );

		return $data;
	}

	// Check value to find if it was serialized. Stolen from WordPress
	function is_serialized( $data ) {
		// if it isn't a string, it isn't serialized
		if ( !is_string( $data ) )
			return false;
		$data = trim( $data );
		if ( 'N;' == $data )
			return true;
		if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
			return false;
		switch ( $badions[1] ) {
			case 'a' :
			case 'O' :
			case 's' :
				if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
					return true;
				break;
			case 'b' :
			case 'i' :
			case 'd' :
				if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
					return true;
				break;
		}
		return false;
	}

	// Unserialize value only if it was serialized. Stolen from WP
	function maybe_unserialize( $original ) {
		if ( is_serialized( $original ) ) // don't attempt to unserialize data that wasn't serialized going in
			return @unserialize( $original );
		return $original;
	}

	/**
 	* Outputs the html checked attribute.
 	*
 	* Compares the first two arguments and if identical marks as checked
 	*
 	*
 	* @param mixed $checked One of the values to compare
 	* @param mixed $current (true) The other value to compare if not just true
 	* @param bool $echo Whether to echo or just return the string
 	* @return string html attribute or empty string
 	*/
 	function checked( $checked, $current = true, $echo = true ) {
		return checked_selected_helper( $checked, $current, $echo, 'checked' );
	}

	/**
 	* Outputs the html selected attribute.
 	*
 	* Compares the first two arguments and if identical marks as selected
 	*
 	*
 	* @param mixed $selected One of the values to compare
 	* @param mixed $current (true) The other value to compare if not just true
 	* @param bool $echo Whether to echo or just return the string
 	* @return string html attribute or empty string
 	*/
 	function selected( $selected, $current = true, $echo = true ) {
		return checked_selected_helper( $selected, $current, $echo, 'selected' );
	}

	/**
 	* Outputs the html disabled attribute.
 	*
 	* Compares the first two arguments and if identical marks as disabled
 	*
 	*
 	* @param mixed $disabled One of the values to compare
 	* @param mixed $current (true) The other value to compare if not just true
 	* @param bool $echo Whether to echo or just return the string
 	* @return string html attribute or empty string
 	*/
	function disabled( $disabled, $current = true, $echo = true ) {
		return checked_selected_helper( $disabled, $current, $echo, 'disabled' );
	}

	/**
 	* Private helper function for checked, selected, and disabled.
 	*
 	* Compares the first two arguments and if identical marks as $type
 	*
 	* @access private
 	*
 	* @param any $helper One of the values to compare
 	* @param any $current (true) The other value to compare if not just true
 	* @param bool $echo Whether to echo or just return the string
 	* @param string $type The type of checked|selected|disabled we are doing
 	* @return string html attribute or empty string
 	*/
 	function checked_selected_helper( $helper, $current, $echo, $type ) {
		if ( (string) $helper === (string) $current )
			$result = " $type='$type'";
		else
			$result = '';

		if ( $echo )
			echo $result;

		return $result;
	}

	/**
	 * Define a buffer callback to replace the time
	 */
	function load_time( $buffer ) {
    	return str_replace(
        	'{microtime}', // this keyword will be replaced with the load time
        	round( microtime( TRUE ) - $GLOBALS['REQUEST_MICROTIME'], 4 ),
        	$buffer
    	);
	}
	
	/**
	 * Fire up the buffer
	 */
	ob_start( 'load_time' );
	
	function is_admin() {
		global $current_user;
		userAccess::is_user_logged_in();
		if($current_user->hasPermission('access_admin') != true) {
			pm_redirect(get_pm_option('siteurl') . '/index.php');
		} else {
			return true;
		}
	}

	function admin_test() {
		global $current_user;
		userAccess::is_user_logged_in();
		if($current_user->hasPermission('access_admin') != true) {
			return false;
		} else {
			return true;
		}
	}

	function jury_test() {
		global $current_user;
		userAccess::is_user_logged_in();
        $jmem = pmdb::connect()->select( DB . 'project_members', '*', 'pp_id = "0" AND pm_user = "' . is_session_set('username') . '"', null ) or die(pmdb::connect()->is_error());
		if($jmem->num_rows < 1) {
			return false;
		} else {
			return true;
		}
	}

	function header_cache() {
		header('Cache-control: private'); // IE 6 FIX
		// always modified
		header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		// HTTP/1.1
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', false);
		// HTTP/1.0
		header('Pragma: no-cache');	
	}
	
	// Die die die
	function pm_die( $message = '', $title = '', $header_code = 200 ) {
		pm_status_header( $header_code );
		
		echo apply_filter( 'die_title', "<div id=\"page-title\"><h1>$title</h1></div>" );
		echo apply_filter( 'die_message', "<div id=\"middle\"><p>$message</p></div>" );
		do_action( 'pm_die' );
		//die();
	}
	
	// Set HTTP status header
	function pm_status_header( $code = 200 ) {
		if( headers_sent() )
			return;
		
		$protocol = $_SERVER["SERVER_PROTOCOL"];
		if ( 'HTTP/1.1' != $protocol && 'HTTP/1.0' != $protocol )
			$protocol = 'HTTP/1.0';
		
		@header ("$protocol $code $desc"); // This causes problems on IIS and some FastCGI setups
		do_action( 'status_header', $code );
	}
	
	function is_session_set($caller) {
		if(isset($_SESSION[$caller])) {
			return $_SESSION[$caller];
		}
	}
	
	function is_get_set($caller) {
		if(isset($_GET[$caller])) {
			return $_GET[$caller];
		}
	}