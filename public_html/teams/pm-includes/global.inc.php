<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

/**
 * ProjectPress calls global functions, must used plugins, and autoloads classes
 *
 * @package ProjectPress
 * @since 2.0
 */

// Defines Must-Used Plugins directory that will autoload plugins.
define('MU_PLUGINS_DIR', PM_DIR . 'pm-content/mu-plugins/');

	/** __autoload may become deprecated in later versions of php.
	 * so to be on the safe side, this has been replaced
	 * with the get_classes() function.
	 */
	/*function __autoload($class) {
    	require(PM_DIR. 'classes/class.'.$class.'.php');
	}*/
	
	function get_classes($class) {
    	require(PM_DIR. 'classes/class.'.$class.'.php');
	}
	spl_autoload_register('get_classes');
 
require(PM_DIR . 'classes/class.pmdb.php');
require_once(PM_DIR . 'pm-includes/constants.php');
require_once(PM_DIR . 'pm-includes/functions-plugins.php');
require_once(PM_DIR . 'pm-includes/functions-actions.php');
require_once(PM_DIR . 'pm-includes/functions-filters.php');
require_once(PM_DIR . 'pm-includes/functions-projects.php');
require_once(PM_DIR . 'pm-includes/functions-deprecated.php');
include_once(PM_DIR . 'pm-includes/add-hook.php');
foreach (glob(MU_PLUGINS_DIR .'*.php') as $file) { 
    if(file_exists($file))
  		include_once($file);
}