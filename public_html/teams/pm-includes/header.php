<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
ob_start('ob_postprocess');
header_cache();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php _e( get_pm_option('sitetitle') ); ?></title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php if(!get_pm_option('skin')) { ?>
<link href="<?php _e( get_stylesheet_directory_uri() ); ?>/style.css" rel="stylesheet" type="text/css" />
<?php } else { ?>
<link href="<?php _e( get_stylesheet_directory_uri()  . '/skins/' . get_pm_option('skin') . '.css'); ?>" rel="stylesheet" type="text/css" />
<?php } ?>
<link type="text/css" rel="stylesheet" media="all" href="<?php _e( get_stylesheet_directory_uri() ); ?>/chat.css" />
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/user_user_types.js"></script>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/chat.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$(".search").keyup(function() 
{
var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;

if(searchbox=='') {
} else {

$.ajax({
type: "POST",
url: "<?php _e( PM_URI ); ?>/pm-includes/search.php",
data: dataString,
cache: false,
success: function(html)
{

$("#display").html(html).show();
	}

});
}return false;    


});
});

jQuery(function($){
   $("#searchbox").Watermark("Search Users");
   });
</script>
<style type="text/css"> 
 /* backslash hack hides from IE \*/
* html .clearfix {height:100%;}
.clearfix {display:block;}
/*end backslash hack */
</style>
<?php pm_head(); ?>
</head>

<body>
	
<div id="headerWrapper">
	<div id="headerContainer">
		<div id="headerLeft">
			<span id="site-title"><?php _e( get_pm_option('sitetitle') ); ?></span>
		</div> <!--Ends headerLeft-->
		<div id="headerRight">
				<input type="text" class="search" id="searchbox" /><br />
				<div id="display"></div>
		</div> <!--Ends headerRight-->
	</div> <!--Ends headerContainer-->
</div> <!--Ends headerWrapper-->

<div id="mainWrapper" class="clearfix">
	<div id="mainContainer">

		<div id="mainLeftSidebar">
			<div id="avatar">
				<?php _e( get_user_avatar(is_session_set('username'), User::instance()->get_user_info(is_session_set('username'),'email')) ); ?>
			</div> <!--Ends mainLeftSidebar avatar-->
			<div id="profile">
				<ul>
					<li><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( is_session_set('username') ); ?>"><span id="name"><?php _e( User::instance()->get_name(is_session_set('username')) ); ?></span></a></li>
					<li><a href="<?php _e( PM_URI ); ?>/profile/editprofile.php?p=basic"><?php _e( _( 'Edit My Profile' ) ); ?></a></li>
				</ul>
			</div> <!--Ends mainLeftSidebar profile-->
			<div class="leftNavigation">
				<ul>

					<?php if($current_user->hasPermission('access_admin') == true) { ?>
					<li <?php if (active_link() == "admin.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "edit_member.php?username=".$_GET['username']) _e( "class='active_link'" );?>
						<?php if (active_link() == "edit_news.php?id=".$_GET['id']) _e( "class='active_link'" );?>
						<?php if (active_link() == "add_news.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "add_member.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "reports.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "generate.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "info.php") _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="Administration" /><a href="<?php _e( PM_URI ); ?>/pm-admin/admin.php"><?php _e( _( 'Admin' ) ); ?></a>
					</li>
					<li <?php if (active_link() == "plugins.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "plugins.php?page=" . $_GET['page']) _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/cog.png" alt="" /><a href="<?php _e( PM_URI ); ?>/pm-admin/plugins.php"><?php _e( _( 'Plugins' ) ); ?></a>
					</li>
					<?php } ?>

					<li><img src="<?php _e( PM_URI ); ?>/images/user.png" alt="Profile" /><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( is_session_set('username') ); ?>"><?php _e( _( 'Profile' ) ); ?></a></li>
					<li <?php if (active_link() == "private_message.php?p=new") _e( "class='active_link'" );?> 
		 				<?php if (active_link() == "private_message.php?p=send") _e( "class='active_link'" );?> 
		 				<?php if (active_link() == "private_message.php?p=read") _e( "class='active_link'" );?>
		 				<?php if (active_link() == "private_message.php?p=deleted") _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/inbox.png" alt="Inbox" /><a href="<?php _e( PM_URI ); ?>/private_message.php?p=new"><?php _e( _( 'Inbox' ) ); ?></a>
					</li>
					<li <?php if (active_link() == "changepassword.php") _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/key.png" alt="Change Password" /><a href="<?php _e( PM_URI ); ?>/profile/changepassword.php"><?php _e( _( 'Change Password' ) ); ?></a>
					</li>
					<li><img src="<?php _e( PM_URI ); ?>/images/shield.png" alt="Logout" /><a href="<?php _e( PM_URI ); ?>/pm-logout.php"><?php _e( _( 'Logout' ) ); ?></a></li>
				</ul>
			</div> <!--Ends first leftNavigation-->

			<div class="break"></div>

			<div class="leftNavigation">
				<ul>
					<li <?php if (active_link() == "index.php") _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/home.png" alt="Home" /><a href="<?php _e( PM_URI ); ?>/index.php"><?php _e( _( 'Home' ) ); ?></a>
					</li>
					<?php list_plugin_user_pages(); ?>
					<li <?php if (active_link() == "news.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "read_news.php?id=".$_GET['id']) _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/newspapers.png" alt="News" /><a href="<?php _e( PM_URI ); ?>/news/news.php"><?php _e( _( 'News' ) ); ?></a>
					</li>
                    <li <?php if (active_link() == "project.php") _e( "class='active_link'" );?>>
                    <img src="<?php _e( PM_URI ); ?>/images/jury.png" alt="Jury" /><a href="<?php _e( PM_URI ); ?>/projects/project.php?p_id=0"><?php _e( _( 'Jury' ) ); ?></a>
                    </li>
                    <li <?php if (active_link() == "members.php") _e( "class='active_link'" );?>>
                    <img src="<?php _e( PM_URI ); ?>/images/address-book-blue.png" alt="Directory" /><a href="<?php _e( PM_URI ); ?>/members.php"><?php _e( _( 'OMC Members' ) ); ?></a>
                    </li>
                    <li <?php if (active_link() == "list_projects.php") _e( "class='active_link'" );?>
                    <?php if (active_link() == "project.php?id=".$_GET['p_id']) _e( "class='active_link'" );?>>
                    <img src="<?php _e( PM_URI ); ?>/images/projects.png" alt="Projects" /><a href="<?php _e( PM_URI ); ?>/projects/list_projects.php"><?php _e( _( 'Projects' ) ); ?></a>
                    </li>
					<li <?php if (active_link() == "forum.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "create_topic.php") _e( "class='active_link'" );?>
						<?php if (active_link() == "view_topic.php?id=".$_GET['id']) _e( "class='active_link'" );?>
						<?php if (active_link() == "edit_reply.php?a_id=".$_GET['a_id']) _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="Forum" /><a href="<?php _e( PM_URI ); ?>/forum/forum.php"><?php _e( _( 'General forum' ) ); ?></a>
					</li>
					<li <?php if (active_link() == "pm_contact.php") _e( "class='active_link'" );?>>
						<img src="<?php _e( PM_URI ); ?>/images/mail.png" alt="Contact" /><a href="<?php _e( PM_URI ); ?>/pm_contact.php"><?php _e( _( 'Contact' ) ); ?></a>
					</li>
				</ul>
			</div> <!--Ends second leftNavigation-->
		</div> <!--Ends mainLeftSidebar-->
		<?php show_update_message(); ?>
		<?php show_important_notice(); ?>
		<?php update_password_notice(is_session_set('username')); ?>
		<div id="mainContent">
			<div id="mainLeft">