<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

/**
 * ProjectPress action filters
 *
 * @package ProjectPress
 * @since 2.1
 */
	
	/**
	* Redirects to another page.
	*
	* @since 2.1
	* @uses apply_filter() Calls 'pm_redirect' hook on $location and $status.
	* @param string $location The path to redirect to
	* @param int $status Status code to use
	* @return bool False if $location is not set
	*/
	function pm_redirect($location, $status = 302) {
		$location = apply_filter('redirect', $location, $status);
		if ( !$location ) // allows the pm_redirect filter to cancel a redirect
			return false;
		header("Location: $location", true, $status);
	}

	/**
	  * Retrieve javascript directory URI.
	  *
	  * @since 2.1
	  * @uses apply_filter() Calls 'javascript_directory_uri' filter on javascript directory URI path.
	  *
	  * @return string Javascript directory URI.
	  */
	 function get_javascript_directory_uri() {
	 	$directory = 'js';
		$javascript_root_uri = get_site_uri();
		$javascript_dir_uri = "$javascript_root_uri/$directory";
	      return apply_filter( 'javascript_directory_uri', $javascript_dir_uri, $directory, $javascript_root_uri );
	 }
	 
	 /**
	  * Retrieve stylesheet directory URI.
	  *
	  * @since 2.1
	  * @uses apply_filter() Calls 'stylesheet_directory_uri' filter on stylesheet directory URI path.
	  *
	  * @return string Stylesheet directory URI.
	  */
	 function get_stylesheet_directory_uri() {
		$directory = 'css';
		$stylesheet_root_uri = get_site_uri();
		$stylesheet_dir_uri = "$stylesheet_root_uri/$directory";
	      return apply_filter( 'stylesheet_directory_uri', $stylesheet_dir_uri, $directory, $stylesheet_root_uri );
	 }
	 
	 /**
	  * Retrieve the sites root url.
	  *
	  * @since 2.1
	  * @uses apply_filter() Calls 'site_uri' filter.
	  *
	  * @return string Site root url.
	  */
	 function get_site_uri() {
	      return apply_filter( 'site_uri', get_pm_option('siteurl') );
	 }
	 
	 function pm_hash_password($password) {
		// By default, use the portable hash from phpass
		$pm_hasher = new PasswordHash(8, FALSE);

			return $pm_hasher->HashPassword($password);
	}
	 
	 function pm_check_password($password, $hash, $user_id = '') {

		// If the hash is still md5...
		if ( strlen($hash) <= 32 ) {
			$check = ( $hash == md5($password) );
		if ( $check && $user_id ) {
			// Rehash using new hash.
			pm_set_password($password, $user_id);
			$hash = pm_hash_password($password);
		}

		return apply_filter('check_password', $check, $password, $hash, $user_id);
		}

		// If the stored hash is longer than an MD5, presume the
		// new style phpass portable hash.
		$pm_hasher = new PasswordHash(8, FALSE);

		$check = $pm_hasher->CheckPassword($password, $hash);

			return apply_filter('check_password', $check, $password, $hash, $user_id);
	}
	 
	 function pm_set_password( $password, $user_id ) {

		$hash = pm_hash_password($password);
		pmdb::connect()->update( DB . 'members', array( 'password' => $hash ), array( 'user_id', $user_id ));
		
	}

	 /**
	  * Borrowed from WordPress
	  *
	  * Send mail, similar to PHP's mail
	  * A true return value does not automatically mean that the user received the
	  * email successfully. It just only means that the method used was able to
	  * process the request without any errors.
	  */
	 function pm_mail( $to, $subject, $message, $headers = '' ) {
	 	global $pmMailer;
		
		// From email and name
		// If we don't have a name from the input headers
		if ( !isset( $from_name ) )
			$from_name = 'ProjectPress';
		
		if ( !isset( $from_email ) ) {
			// Get the site domain and get rid of www.
			$sitename = strtolower( $_SERVER['SERVER_NAME'] );
			if ( substr( $sitename, 0, 4 ) == 'www.' ) {
				$sitename = substr( $sitename, 4 );
			}

			$from_email = 'projectpress@' . $sitename;
		}
		
		// Plugin authors can override the default mailer
		$pmMailer->From     = apply_filter( 'pm_mail_from'     , $from_email );
		$pmMailer->FromName = apply_filter( 'pm_mail_from_name', $from_name  );
		
		// Set destination addresses
		if ( !is_array( $to ) )
			$to = explode( ',', $to );

		foreach ( (array) $to as $recipient ) {
			try {
				// Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
				$recipient_name = '';
				if( preg_match( '/(.*)<(.+)>/', $recipient, $matches ) ) {
					if ( count( $matches ) == 3 ) {
						$recipient_name = $matches[1];
						$recipient = $matches[2];
					}
				}
				$pmMailer->AddAddress( $recipient, $recipient_name);
			} catch ( phpmailerException $e ) {
				continue;
			}
		}

		// Set mail's subject and body
		$pmMailer->Subject = $subject;
		$pmMailer->Body    = $message;
		
		// Set to use PHP's mail()
		$pmMailer->IsMail();

		// Set Content-Type and charset
		// If we don't have a content-type from the input headers
		if ( !isset( $content_type ) )
			$content_type = 'text/plain';

			$content_type = apply_filter( 'pm_mail_content_type', $content_type );

			$pmMailer->ContentType = $content_type;

		// Set whether it's plaintext, depending on $content_type
		if ( 'text/html' == $content_type )
			$pmMailer->IsHTML( true );

			// Set the content-type and charset
			$pmMailer->CharSet = apply_filter( 'pm_mail_charset', $charset );

		// Set custom headers
		if ( !empty( $headers ) ) {
			foreach( (array) $headers as $name => $content ) {
				$pmMailer->AddCustomHeader( sprintf( '%1$s: %2$s', $name, $content ) );
			}

			if ( false !== stripos( $content_type, 'multipart' ) && ! empty($boundary) )
				$pmMailer->AddCustomHeader( sprintf( "Content-Type: %s;\n\t boundary=\"%s\"", $content_type, $boundary ) );
		}
		
		// Send!
		try {
			$pmMailer->Send();
		} catch ( phpmailerException $e ) {
			return false;
		}

			return true;
	 }

	 function send_wall_comment_email() {
	 	$result = pmdb::connect()->select( DB . 'wall_posts, ' . DB . 'wall_posts_comments', 
	 									  '*',
										  'p_id = post_id AND p_id = "' . $_REQUEST['post_id'] . '"',
										  null
										 );
		
		$r = $result->fetch_object();
		
	 	$subject = 'New Comment on Wall Post';
		$message = 'Original wall post: <em>"' . $r->post . '"</em>';
		$message .= '<br /><br />';
		$message .= 'Comment: ' . pmdb::connect()->escape( $_REQUEST['comment_text'] );
		$message .= '<br /><br />';
		$message .= '<b>Login to your account to reply to this comment: <a href="' . PM_URI . '/login.php">' . PM_URI . '/login.php</a></b>';
		
		$to = $r->email;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; UTF-8' . "\r\n";
		$headers .= 'From: ' . User::instance()->get_name($_SESSION['username']) . ' <' . User::instance()->get_user_info($_SESSION['username'],'email') . '>' . "\r\n";
		$body = User::instance()->get_name($_SESSION['username']) . " posted a new comment to one of your wall posts. See below for details. <br /><br />";
		$body .= "From: " . User::instance()->get_name($_SESSION['username']) . "<br /><br /> E-Mail: " . User::instance()->get_user_info($_SESSION['username'],'email') . "<br /><br />" . $message;
		
		pm_mail($to, $subject, $body, $headers);
		
		return apply_filter('wall_comment_email',$subject,$message,$headers,$body);
	}

	function send_forum_reply_email() {
	 	$result = pmdb::connect()->select( DB . 'forum_topics, ' . DB . 'forum_posts', 
	 									  '*',
										  'id = topic_id',
										  null
										 );
		
		$r = $result->fetch_object();
		
	 	$subject = '[' . get_pm_option('sitetitle') . '] New discussion activity in the Forum';
		$message = 'A new reply has been posted to the topic "' . $r->topic . '"';
		$message .= '<br />';
		$message .= 'Author: ' . User::instance()->get_name( $r->t_user );
		$message .= '<br /><br />';
		$message .= 'To view the entire discussion, visit: ' . PM_URI . '/forum/view_topic.php?id=' . $r->id;
		
		$to = User::instance()->get_email( $r->t_user );
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; UTF-8' . "\r\n";
		$headers .= 'From: ' . User::instance()->get_name($_SESSION['username']) . ' <' . User::instance()->get_user_info($_SESSION['username'],'email') . '>' . "\r\n";
		$body = $message;
		
		pm_mail($to, $subject, $body, $headers);
		
		return apply_filter('forum_reply_email',$subject,$message,$headers,$body);
	 }

	 function pp_logo($size = 100) {
	 	$logo_size = getimagesize(PM_DIR . '/images/pp-logo.png');
		$logo = '<img src="' . PM_URI . '/images/pp-logo.png" ' . imgResize($logo_size[1],  $logo_size[1], $size) . ' alt="ProjectPress Logo" />';
		return apply_filter( 'logo', $logo, $size );
	 }
	 
	 // Prints an update message if the installed version is less than the current version.
	function show_update_message() {
		global $current_user;
		if($current_user->hasPermission('access_admin') == true) {
			if(CURRENT_VERSION < PP::upgrade(0)) {
				$update = '<div id="success">ProjectPress <a href="http://projectpress.org/pp/latest.zip">'.PP::upgrade(0).'</a> is available for download or use the <a href="' . PM_URI . '/pm-admin/upgrade.php">auto updater</a>.</div>';
				$update = apply_filter('update_message',$update);
				echo $update;
			}
		}
	}
	
	// Prints out a notice is something has to be done before upgrading to the newest version of ProjectPress.
	function show_important_notice() {
		global $current_user;
		if($current_user->hasPermission('access_admin') == true) {
			if(@fopen('http://projectpress.org/notice.txt',"r") == true && CURRENT_VERSION < PP::upgrade(0)) {
				$message = '<div id="success">'.file_get_contents('http://projectpress.org/notice.txt').'</div>';
				$message = apply_filter('important_notice',$message);
				echo $message;
			}
		}
	}