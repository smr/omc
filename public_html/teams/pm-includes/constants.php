<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

/**
 * ProjectPress constants
 * 
 * @package ProjectPress
 * @since 3.0.3
 *
 * This file is intended to group all constants to
 * make it easier for the site administrator to tweak
 * the login script.
 */

/**
 * Cookie Constants - these are the parameters
 * to the setcookie function call, change them
 * if necessary to fit your website. If you need
 * help, visit www.php.net for more info.
 * <http://www.php.net/manual/en/function.setcookie.php>
 */
define("COOKIE_EXPIRE", 60*60*24*365);  // 365 days by default
define("COOKIE_PATH", "/");  // Available in whole domain