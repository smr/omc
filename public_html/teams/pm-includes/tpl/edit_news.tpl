<script type="text/javascript">
function Validate_News()
{
    if (document.news_form.title.value == '') 
    {
        alert('Please fill in the news title!');
        return false;
    }
	if (document.news_form.text.value == '') 
    {
        alert('Please fill in the news text.');
        return false;
    }
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/admin.png" alt="" /><h1>Edit News</h1>
</div>

[@message]
				
<div id="middle">
<h2>Edit: [@newstitle]</h2>
<form name="news_form" action="[@pmurl]/pm-admin/edit_news.php?id=[@id]" method="post" onsubmit="return Validate_News();">
	<table cellpadding="0" cellspacing="2" align="center" width="100%">
			<tbody>
				<tr><td>Title:<br /> <input class="forminput" name="title" size="40" value="[@newstitle]" maxlength="255" style="width:500px;" /></td></tr>
				<tr><td>News Text:<br /> <textarea class="forminput" id="eNews" name="text" rows="7" cols="30" style="width:500px;">[@text]</textarea></td></tr>
				

			</tbody>
	</table>
<p><input type="submit" value="Submit" name="news" id="sub_button" />
<index type="hidden" value="[@id]" name="id" /></p>
</form>
</div><!--Ends middle-->