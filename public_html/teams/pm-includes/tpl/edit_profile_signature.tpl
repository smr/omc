<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/user.png" alt="" /><h1>Basic Profile</h1>
</div>

<div id="tabs">
	<ul>
		<li [@basic]>
			<a href="editprofile.php?p=basic"><span>Basic</span></a>
		</li>
		<li [@contact]>
			<a href="editprofile.php?p=contact"><span>Contact</span></a>
		</li>
		<li [@forumsignature]>
			<a href="editprofile.php?p=signature"><span>Forum Signature</span></a>
		</li>
		<li [@avatar]>
			<a href="editprofile.php?p=avatar"><span>Avatar</span></a>
		</li>
	</ul>
</div>

[@message]
				
<div id="middle">
<form action="[@pmurl]/profile/editprofile.php?p=signature" method="post">
	<table cellpadding="10" cellspacing="8" align="center" width="100%">
			<tbody>
				
				<tr>
					<th scope="row">Forum Signature:</th>
					<td><textarea rows="20" type="text" cols="50" id="signature" class="forminput" name="signature" onKeyDown="limitText(this.form.signature,this.form.countdown,500);" onKeyUp="limitText(this.form.signature,this.form.countdown,500);">[@signature]</textarea><br />
					<font size="1">(Maximum characters: 500)<br />
					You have <input readonly type="text" name="countdown" size="3" value="500"> characters left.</font>
					</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td colspan="2"><input name="sig" type="submit" value="Submit" id="sub_button" /></td>
				</tr>

			</tbody>
	</table>
</form>
</div><!--Ends middle-->