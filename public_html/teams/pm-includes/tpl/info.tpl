<div id="page-title">
	<img src="[@pmurl]/images/information.png" alt="" /><h1>Info Page</h1>
</div>

<div id="middle">
<div id="info">
<h3><img src="[@pmurl]/images/home.png" alt="" />Home</h3>
<p>The home page is your first stop when you login. This page shows a
feed of wall posts from everyone in the system.</p>

<h3><img src="[@pmurl]/images/address-book-blue.png" alt="" />Directory</h3>
<p>One of the main parts of SimpleCollab is the directory. This is
where you will find every active member in your organization.</p>

<h3><img src="[@pmurl]/images/newspaper.png" alt="" />News</h3>
<p>The news component is where the admin as well as anyone who has access to 
post updates/announcements to the community.</p>

<h3><img src="[@pmurl]/images/comment.png" alt="" />Forum</h3>
<p>The forum component can be used in a number of ways. It can be used as a
regular discussion board or you it can be used to discuss the weekly activities</p>

<h3><img src="[@pmurl]/images/mail.png" alt="" />Contact</h3>
<p>The contact component is mostly for users to submit bug or
troubleshooting requests to the admin or webmaster. However, this
component can be used for what ever best fits your needs.</p>

<h3><img src="[@pmurl]/images/key.png" alt="" />Change Password</h3>
<p>Users can update their passwords at anytime. Since passwords are hashed
using md5, it is impossible for admins to resend passwords to users
since you cannot decrypt hashed passwords.</p>

<h3><img src="[@pmurl]/images/mail.png" alt="" />Inbox</h3>
<p>The private message system (PMS) is great for private communication
between users in your community. At the moment only one to one messages
can be sent.</p>

<h3><img src="[@pmurl]/images/user.png" alt="" />Profile</h3>
<p>The profile component is where a user's public information will be shown
as well as their own wall posts. Also, users can view each other's
profile unless they have it set to private.</p>

<h3><img src="[@pmurl]/images/admin.png" alt="" />Admin</h3>
<p>The admin component is where the superadmin will go to administer news,
members, and build reports. Understanding levels: level 1 = admin; 
level 2 = members</p>

</div>
</div>