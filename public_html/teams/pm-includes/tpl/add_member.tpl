<script type="text/javascript">
function Validate_Member()
{
    if (document.addmember_form.username.value == '') 
    {
        alert('Please fill in the member\'s username!');
        return false;
    }
	if (document.addmember_form.first_name.value == '') 
    {
        alert('Please fill in the member\'s First Name!');
        return false;
    }
    if (document.addmember_form.last_name.value == '') 
    {
        alert('Please fill in the member\'s Last Name!');
        return false;
    }
    if (document.addmember_form.email.value == '') 
    {
        alert('Please fill in the member\'s Email!');
        return false;
    }
    if (document.addmember_form.password.value == '') 
    {
        alert('Please fill in the member\'s password!');
        return false;
    }
    if (document.addmember_form.role.value == '') 
    {
        alert('Please choose the member\'s role!');
        return false;
    }
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/admin.png" alt="" /><h1>Add A New Member</h1>
</div>

[@message]
		
<div id="middle">
	<table cellspacing="5" cellpadding="0">
			<form name="addmember_form" method="post" action="[@phpself]" onsubmit="return Validate_Member();">
				<tr><td><p style="color:red;">All fields are required.</p></td></tr>
				<tr><td>Username:</td> <td><input class="forminput" name="username" size="40" maxlength="255"></td></tr>
				<tr><td>First Name:</td> <td><input class="forminput" name="first_name" size="40" maxlength="255"></td></tr>
				<tr><td>Last Name:</td> <td><input class="forminput" name="last_name" size="40" maxlength="255"></td></tr>
				<tr><td>Email:</td> <td><input class="forminput" name="email" size="40" maxlength="255"></td></tr>
				<tr><td>Password:</td> <td><input class="forminput" type="password" name="password" size="40" maxlength="255"></td></tr>
				<tr><td>User Role:</td>
					<td>
						<select name="role" class="textBox">
							<option value="">--Level--</option>
							<option value="1">Administrator</option>
							<option value="2">Member</option>
						</select>
					</td>
				</tr>
				<tr><td></td> <td><input type="submit" id="sub_button" name="submit" value="Add Member"></td>
			</form>
      </table>
</div><!--Ends middle-->