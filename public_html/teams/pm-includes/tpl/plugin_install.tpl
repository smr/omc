<div id="page-title">
	<img src="[@pmurl]/images/cog.png" alt="" /><h1>Install Plugins <span id="header-link"><a href="[@pmurl]/pm-admin/plugins.php">Installed Plugins</a></span></h1>
</div>

<div id="middle">
[@message]
<p>If you have a plugin in a .zip format, you may install it by uploading it here.</p>
	<form enctype="multipart/form-data" method="post" action="">
		<label>Choose a zip file to upload: <input type="file" name="zip_file" /></label>
		<input type="submit" name="submit" class="sub_button" value="Upload" />
	</form>
</div>