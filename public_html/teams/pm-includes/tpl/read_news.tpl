<div id="page-title">
	<img src="[@pmurl]/images/newspaper.png" alt="" /><h1>News</h1>
</div>
				
<div id="middle">
	<div class="news">
		<h2>[@newstitle]</h2>
		<span class="date">[@newsdate]</span>
	</div>
	
	<div class="post">
		[@newstext]
	</div>
	
	<p><a href="javascript:history.go(-1)" title="Return to the previous page">&laquo; Go back</a></p>

</div><!--Ends middle-->