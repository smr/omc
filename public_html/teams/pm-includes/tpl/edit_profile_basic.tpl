<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

$(function() {
   $.datepicker.setDefaults({
   showOn: 'both',
   buttonImageOnly: true,
   buttonImage: '[@pmurl]/images/calendar.png',
   buttonText: 'Calendar' });
    $('#date').datepicker({
    	duration: '',
        showTime: false,
        constrainInput: false
     });
});

function Validate()
{
	if (document.edit_profile.first_name.value == '') 
    {
        alert('Please fill in your first name!');
        return false;
    }
    if (document.edit_profile.last_name.value == '') 
    {
        alert('Please fill in your last name!');
        return false;
    }
    if (document.edit_profile.dob.value == '' || document.edit_profile.dob.value == '0000-00-00') 
    {
       alert('Please fill in your date of birth!');
      return false;
    }
    
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/user.png" alt="" /><h1>Basic Profile</h1>
</div>

<div id="tabs">
	<ul>
		<li [@basic]>
			<a href="editprofile.php?p=basic"><span>Basic</span></a>
		</li>
		<li [@contact]>
			<a href="editprofile.php?p=contact"><span>Contact</span></a>
		</li>
		<li [@signature]>
			<a href="editprofile.php?p=signature"><span>Forum Signature</span></a>
		</li>
		<li [@avatar]>
			<a href="editprofile.php?p=avatar"><span>Avatar</span></a>
		</li>
	</ul>
</div>

[@message]
				
<div id="middle">
<form name="edit_profile" action="[@pmurl]/profile/editprofile.php?p=basic" method="post" onsubmit="return Validate();">
	<table cellpadding="10" cellspacing="8" align="center" width="100%">
			<tbody>
				<tr>
					<th scope="row">First Name:</th>
					<td><input name="first_name" type="text" value="[@firstname]" class="forminput"  size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">Last Name:</th>
					<td><input name="last_name" type="text" value="[@lastname]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">Twitter:</th>
					<td><input name="twitter" type="text" value="[@twitter]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">Facebook:</th>
					<td><input name="facebook" type="text" value="[@facebook]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">DOB:</th>
					<td><input name="dob" type="text" value="[@dob]" class="forminput" size="30" />&nbsp;&nbsp;<span>e.g YYYY-MM-DD</span></td>
				</tr>
				
				<tr>
					<th scope="row">Bio:</th>
					<td><textarea rows="20" type="text" id="bio" name="bio" cols="50" class="forminput" onKeyDown="limitText(this.form.bio,this.form.countdown,200);" onKeyUp="limitText(this.form.bio,this.form.countdown,200);">[@bio]</textarea><br />
					<font size="1">(Maximum characters: 200)<br />
					You have <input readonly type="text" name="countdown" size="3" value="200"> characters left.</font>
					</td>
				</tr>
				
				<!-- <tr class="odd">
					<th scope="row">Privacy:</th>
					<td><input type="checkbox" name="privacy" value="1" [@privacy] />&nbsp;&nbsp;Make my profile private (viewable only by me)</td>
				</tr> -->
				
				<tr>
					<td></td>
					<td colspan="2"><input type="submit" value="Submit" name="basic" id="sub_button" /></td>
				</tr>

			</tbody>
	</table>
</form>
</div><!--Ends middle-->