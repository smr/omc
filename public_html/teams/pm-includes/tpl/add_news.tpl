<script type="text/javascript">
function Validate_News() {
    if (document.news_form.title.value == '') {
        alert('Please fill in the news title!');
        return false;
    }
	if (document.news_form.text.value == '') {
        alert('Please fill in the news text.');
        return false;
    }
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/newspaper.png" alt="" /><h1>Add News</h1>
</div>
				
<div id="middle">
	<table cellspacing="10" cellpadding="0">
			<form name="news_form" method="post" action="[@phpself]" onsubmit="return Validate_News();">
				<tr><td><p style="color:red;">All fields are required.</p></td></tr>
				<tr><td>Title:<br /> <input class="forminput" name="title" size="40" maxlength="255" style="width:500px;"></td></tr>
				<tr><td>News Text:<br /> <textarea class="forminput" name="text" id="cnews" rows="7" cols="30" style="width:500px"></textarea></td></tr>
				<tr><td><input type="submit" id="sub_button" name="submit" value="Add News"></td>
			</form>
      </table>
</div><!--Ends middle-->