<script type="text/javascript">
$(function() {
$(".cf-submit").click(function() {
var first_name = $("#first_name").val();
var last_name = $("#last_name").val();
var subject = $("#subject").val();
var email = $("#email").val();
var message = $("#message").val();
var dataString = 'first_name=' + first_name + '&last_name=' + last_name + '&subject=' + subject + '&email=' + email + '&message=' + message;

if(first_name=='' || last_name=='' || subject=='' || email=='' || message=='')
{
$('.success').fadeOut(200).hide();
$('.error').show();
$('.error').fadeOut(5000);
}
else
{
$.ajax({
type: "POST",
url: "contact.php",
data: dataString,
success: function(){
$('.success').show();
$('.success').fadeOut(2000);
$('.error').fadeOut(200).hide();
$('#subject').val('');
$('#email').val('');
$('#message').val('');
}
});
}
return false;
});
});
</script>


<div id="page-title">
	<img src="[@pmurl]/images/mail.png" alt="" /><h1>[@contact]</h1>
</div>

<div class="success" style="display:none">[@success]</div>
<div class="error" style="display:none">[@error]</div>

<div id="middle">
[@form_message]
<form name="form" action="" method="post">
	<table cellpadding="0" cellspacing="2" align="center" width="100%">
		<tbody>
			<tr>
				<th>First Name:</th>
					<td><input readonly class="forminput" id="first_name" name="first_name" value="[@firstname]"</td>
			</tr>
						
			<tr>
				<th>Last Name:</th>
				<td><input readonly class="forminput" id="last_name" name="last_name" value="[@lastname]"</td>
			</tr>
						
			<tr>
				<th>Subject:</th>
				<td><input class="forminput" id="subject" name="subject" type="text" size="30" /></td>
			</tr>
						
			<tr>
				<th>Email:</th>
				<td><input class="forminput" id="email" name="email" type="text" size="30" /></td>
			</tr>
						
			<tr>
				<th>Message:</th>
				<td><textarea class="forminput" id="message" name="message" size="30" /></textarea></td>
			</tr>
						
			<tr>
				<td></td>
				<td colspan="2"><input type="submit" value="Submit" name="submit" class="cf-submit" id="sub_button" /></td>
			</tr>

		</tbody>
	 </table>
</form>
</div>