<script type="text/javascript">
function Validate_Email()
{
    if (document.contact_form.email.value == '') 
    {
        alert('You must have an email address on file. Please enter a valid email.');
        return false;
    }
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/user.png" alt="" /><h1>Contact Info</h1>
</div>

<div id="tabs">
	<ul>
		<li [@basic]>
			<a href="editprofile.php?p=basic"><span>Basic</span></a>
		</li>
		<li [@contact]>
			<a href="editprofile.php?p=contact"><span>Contact</span></a>
		</li>
		<li [@signature]>
			<a href="editprofile.php?p=signature"><span>Forum Signature</span></a>
		</li>
		<li [@avatar]>
			<a href="editprofile.php?p=avatar"><span>Avatar</span></a>
		</li>
	</ul>
</div>

[@message]
				
<div id="middle">
<form name="contact_form" action="[@pmurl]/profile/editprofile.php?p=contact" method="post" onsubmit="return Validate_Email();">
<table cellpadding="10" cellspacing="2" align="center" width="100%">
			<tbody>
				<tr>
					<th scope="row">Email:</th>
					<td><input name="email" type="text" value="[@email]" class="forminput" size="30" /></td>
		
				
				<tr class="odd">
					<th scope="row">AIM:</th>
					<td><input name="aim" type="text" value="[@aim]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">MSN:</th>
					<td><input name="msn" type="text" value="[@msn]" class="forminput" size="30" /></td>
				</tr>
				
				<tr class="odd">
					<th scope="row">Gtalk:</th>
					<td><input name="gtalk" type="text" value="[@gtalk]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">iChat:</th>
					<td><input name="ichat" type="text" value="[@ichat]" class="forminput" size="30" /></td>
				</tr>
				
				<tr class="odd">
					<td></td>
					<td><input type="submit" value="Submit" name="contact" id="sub_button" /></td>
				</tr>
				
			</tbody>
</table>
</form>
</div><!--Ends middle-->