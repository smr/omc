<div id="page-title">
	<img src="[@pmurl]/images/admin.png" alt="" /><h1>Administration Homepage</h1>
</div>

<div id="middle">
	<p>To seek out help/support for plugin development or best practices, check out the <a href="http://projectpress.org/forums">support forum</a>. However, if you find a bug, 
	please use the <a href="http://trac.projectpress.org/">Trac/Ticket</a> system.</p>

	<div id="mod-nav">
		<ul>
			<li><a href="[@pmurl]/pm-admin/info.php"><img src="[@pmurl]/images/information.png" alt="" />Info Page</a></li>
			<li><a href="[@pmurl]/pm-admin/manage_news.php"><img src="[@pmurl]/images/newspaper.png" alt="" />Manage News</a></li>
			<li><a href="[@pmurl]/pm-admin/settings.php"><img src="[@pmurl]/images/notebook-plus.png" alt="" />Site Settings</a></li>
			<li><a href="[@pmurl]/pm-admin/plugins.php"><img src="[@pmurl]/images/cog.png" alt="" />Plugins</a></li>
		</ul>

		<h3>User Management</h3>
		<ul>
			<li><a href="[@pmurl]/pm-admin/user_types.php"><img src="[@pmurl]/images/user-types.png" alt="" />User Types</a></li>
			<li><a href="[@pmurl]/pm-admin/add_member.php"><img src="[@pmurl]/images/user-plus.png" alt="" />Add User</a></li>
			<li><a href="[@pmurl]/pm-admin/user_user_type.php"><img src="[@pmurl]/images/add-user-user-type.png" alt="" />Add User's User Type</a></li>
		</ul>
		<ul>
			<li><a href="[@pmurl]/pm-admin/acl/"><img src="[@pmurl]/images/key.png" alt="" />Access Control Levels</a></li>
			<li><a href="[@pmurl]/pm-admin/manage_members.php"><img src="[@pmurl]/images/group_gear.png" alt="" />Manage Users</a></li>
		</ul>

		<h3>Project Management</h3>
		<ul>
			<li><a href="[@pmurl]/projects/project_types.php"><img src="[@pmurl]/images/user-silhouette.png" alt="" />Project Types</a></li>
			<li><a href="[@pmurl]/pm-admin/leaders.php"><img src="[@pmurl]/images/leaders.png" alt="" />Project Leaders</a></li>
		</ul>
	</div>

</div>