<script type="text/javascript">
function Validate()
{
	if (document.edit_member.first_name.value == '') 
    {
        alert('Please fill in the user\'s first name!');
        return false;
    }
    if (document.edit_member.last_name.value == '') 
    {
        alert('Please fill in the user\'s last name!');
        return false;
    }
    
    return true;
}
</script>

<div id="page-title">
	<img src="[@pmurl]/images/admin.png" alt="" /><h1>Edit [@username]'s Profile</h1>
</div>

[@message]
				
<div id="middle">
<div id="edit-member">
<form name="edit_member" action="[@pmurl]/pm-admin/edit_member.php?username=[@username]" method="post" onsubmit="return Validate();">
	<table cellpadding="0" cellspacing="2" align="center" width="100%">
			<tbody>
				<tr class="odd">
					<th scope="row">First Name:</th>
					<td><input name="first_name" type="text" value="[@firstname]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">Last Name:</th>
					<td><input name="last_name" type="text" value="[@lastname]" class="forminput" size="30" /></td>
				</tr>
				
				<tr class="odd">
					<th scope="row">Twitter URL:</th>
					<td><input name="twitter" type="text" value="[@twitter]" class="forminput" size="30" /></td>
				</tr>
				
				<tr>
					<th scope="row">Facebook URL:</th>
					<td><input name="facebook" type="text" value="[@facebook]" class="forminput" size="30" /></td>
				</tr>
				
				<tr class="odd">
					<th scope="row">Active:</th>
					<td><input type="checkbox" name="active" value="1" [@active] />&nbsp;&nbsp;Account Active?</td>
				</tr>
		
				<tr>
					<th scope="row">Privacy:</th>
					<td><input type="checkbox" name="privacy" value="1" [@privacy] />&nbsp;&nbsp;Profile private?</td>
				</tr>
				
				<tr class="odd">
					<td></td>
					<td colspan="2"><input type="submit" value="Submit" name="manage" id="sub_button" /></td>
				</tr>

			</tbody>
	</table>
</form>
</div><!--Ends edit-member-->
</div><!--Ends edit middle-->