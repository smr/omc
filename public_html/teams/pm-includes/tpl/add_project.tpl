<script type="text/javascript" >
$(function() {
$(".ap-submit").click(function() {
var project_name = $("#project_name").val();
var ptype_id = $("#ptype_id").val();
var project_description = $("#project_description").val();
var contact_email = $("#contact_email").val();
var dataString = 'project_name='+ project_name + '&project_description='+ project_description + '&ptype_id='+ ptype_id + 
'&contact_email='+ contact_email;

if(project_name=='' || project_description=='' || ptype_id=='' || contact_email=='')
{
$('.success').fadeOut(200).hide();
$('.error').show();
$('.error').fadeOut(3000);
}
else
{
$.ajax({
type: "POST",
url: "add_project.php",
data: dataString,
success: function(){
$('.success').show();
$('.success').fadeOut(3000);
$('.error').fadeOut(200).hide();
$("#project_name").val('');
$("#project_description").val('');
$("#ptype_id").val('');
$("#contact_email").val('');
}
});
}
return false;
});
});
</script>

<div id="page-title">
	<img src="[@pmurl]/images/projects.png" alt="" /><h1>Add Project</h1>
</div>
				
<div class="success" style="display:none">[@success]</div>
<div class="error" style="display:none">[@error]</div>

				<div id="middle">
					<form name="form" action="" method="post">
					<table cellpadding="0" cellspacing="5" align="center" width="100%">
					<tbody>
				
						<tr>
							<th>Project Name:</th>
							<td><input class="forminput" id="project_name" name="project_name" type="text" size="30" /></td>
						</tr>
						
						<tr>
							<th>Project Type:</th>
							<td><select id="ptype_id" class="forminput">
								<option value=""></option>
							    [@content]
						</tr>
						
						<tr>
							<th>Project Description:</th>
							<td><textarea class="forminput" id="project_description" name="project_description" cols="50" rows="10"></textarea></td>
						</tr>
						
						<tr>
							<th>Contact Email:</th>
							<td><input class="forminput" id="contact_email" name="contact_email" type="text" size="30" /></td>
						</tr>
						
						<!-- <tr>
							<th>Private:</th>
							<td><input class="forminput" id="privacy" name="privacy" type="checkbox" value="1" />Can anyone join?</td>
						</tr> -->
						
						<tr>
							<td></td>
							<td colspan="2"><input type="submit" value="Submit" name="submit" class="ap-submit" id="sub_button" /></td>
						</tr>

					</tbody>
	 				</table>
</form>
</div><!--Ends middle-->