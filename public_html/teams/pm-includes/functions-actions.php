<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');
/**
 * ProjectPress defines actions hooks to use
 *
 * @package ProjectPress
 * @since 2.1
 */
	
	/**
	* Fire the pm_head action
	*
	* @since 2.1
	* @uses do_action() Calls 'pm_head' hook.
	*/
	function pm_head() {
		do_action('pm_head');
	}

	/**
	* Fire the pm_footer action
	*
	* @since 2.1
	* @uses do_action() Calls 'pm_footer' hook.
	*/
	function pm_footer() {
		do_action('pm_footer');
	}
	
	/**
	* Is triggered when the admin pages are accessed.
	*
	* @since 2.1
	* @uses do_action() Calls 'admin_init hook.
	*/
	do_action('admin_init');