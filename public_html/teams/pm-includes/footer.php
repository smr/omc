<?php if(!defined('access')) die ('You are not allowed to execute this file directly.'); ?>

			</div><!--Ends mainLeft-->

			<?php if(basename($_SERVER["REQUEST_URI"]) != 'plugins.php') { _e(get_sidebar()); } ?>

		</div> <!--Ends mainContent-->
	</div> <!--Ends mainContainer-->
</div> <!--Ends mainWrapper-->

<div id="footerWrapper">
	<div id="footerContainer">
		<div id="footer">
			<span><?php _e( PP::pm_version() ); ?> &copy; <?php _e( date('Y') ); ?> <a href="http://ceata.org" target="_blank">Ceata</a></span> </br />
			<span>This page was rendered in {microtime} seconds.</span>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
        mode : "exact",
		theme : "simple",
		elements: "cnews,fCreate,eNews,bio,signature,p_reply,contact_form,pft_detail,pfp_post"
});
</script>

<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/jquery.livequery.js"></script>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/jquery.elastic.js"></script>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/jquery.watermarkinput.js"></script>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/user_types.js"></script>
<?php if(basename($_SERVER["REQUEST_URI"]) == 'profile.php?username='.$_GET['username']) { ?>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/post-wall.js"></script>
<?php } elseif(basename($_SERVER["REQUEST_URI"]) == 'index.php') { ?>
<script type="text/javascript" src="<?php _e( get_javascript_directory_uri() ); ?>/wall-feed.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php _e( PM_URI ); ?>/projects/project_lead.js"></script>

<?php userOnline(is_session_set('username')); ?>

<?php pm_footer(); ?>
</body>
</html>
<?php ob_end_flush(); ?>