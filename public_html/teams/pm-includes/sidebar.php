<?php if(!defined('access')) die ('You are not allowed to execute this file directly.'); ?>

			<div id="mainRightSidebar">
			<?php if($_GET['username']) { ?>

				<div id="avatar">
					<?php _e( get_user_avatar($_GET['username'], User::instance()->get_email($_GET['username']), 100) ); ?>
				</div> <!--Ends mainLeftSidebar avatar-->

				<div id="status">
					<?php _e( userOnline($_GET['username']) ); ?>
				</div> <!--Ends mainLeftSidebar status-->
			<?php } ?>
			
<?php if($_GET['p_id']) { ?>
				<div id="projLeader">
				<div class="bar">
				<div class="barTitle"><h4 class="uiHeaderTitle"><?php _e( _( 'Project Leader(s)' ) ); ?></h4></div>
				<div class="barLink"><a rel="leadbox" class="pl_cancel" href="javascript:void(0)"><?php _e( _( 'Cancel' ) ); ?></a></div>
				</div>
				<div class="break"></div>
				<div align="left">
				<?php if(is_session_set('username') == get_project_meta($_GET['p_id'],'creator')) { ?>
				<form action="" method="post" name="LeadForm" id="LeadForm">
					<input id="pl_user" name="pl_user" maxlength="40" value="" />
					<input type="hidden" id="project_id" name="project_id" value="<?php _e( $_GET['p_id'] ); ?>" />
					<div id="LeadBox">
						<a id="AddLead" class="small button comment"> Add Leader</a>
					</div>
				</form>
				<?php } ?>
				<br clear="all" />
				<div id="ShowLead" align="center">
					<?php include( PM_DIR . 'projects/p_lead.php'); ?>
				</div>
				</div>
				</div><!--Ends projLeader-->
<?php } ?>
				
				<div id="news">
				<div class="bar">
				<div class="barTitle"><?php _e( _( 'News' ) ); ?></div>
				<div class="barLink"><a href="<?php _e( PM_URI ); ?>/news/news.php"><?php _e( _( 'See All' ) ); ?></a></div>
				</div>
				<div class="break"></div>
					<ul>
						<?php _e( get_news() ); ?>
					</ul>
				</div><!--Ends news-->
				
				<div id="forum">
				<div class="bar">
				<div class="barTitle"><?php _e( _( 'Forum' ) ); ?></div>
				<div class="barLink"><a href="<?php _e( PM_URI ); ?>/forum/forum.php"><?php _e( _( 'See All' ) ); ?></a></div>
				</div>
				<div class="break"></div>
					<ul>
						<?php _e( get_forum_replies() ); ?>
					</ul>
				</div><!--Ends forum-->
				
				<?php do_action( 'sidebar_widget' ); ?>

			</div> <!--Ends mainRightSidebar-->