<?php
if(!defined('access')) die ('You are not allowed to execute this file directly.');

	/**
	* Initialize all plugins
	*
	* @since 2.1
	* @uses do_action() Calls 'plugins_loaded' hook.
	*/
	load_activated_plugins();
	do_action('plugins_loaded');