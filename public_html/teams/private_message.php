<?php
 /**
 * ProjectPress private messaging system
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(__FILE__) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

    // initiate a new pm class
    $pm = new pms(is_session_set('userID'));
    

// In this switch we check what page has to be loaded, this way we just load the messages we want using numbers from 0 to 3 (0 is standart, so we don't need to type this)
if(isset($_GET['p'])) {
    switch($_GET['p']) {
        // get all new / unread messages
        case 'new': $pm->getmessages(); break;
        // get all send messages
        case 'send': $pm->getmessages(2); break;
        // get all read messages
        case 'read': $pm->getmessages(1); break;
        // get all deleted messages
        case 'deleted': $pm->getmessages(3); break;
        // get a specific message
        case 'view': $pm->getmessage($_GET['mid']); break;
        // get all new / unread messages
        default: $pm->getmessages(); break;
    }
} else {
    // get all new / unread messages
    $pm->getmessages();
}
// Standard links
?>

		<div id="page-title">
			<img src="<?php _e( PM_URI ); ?>/images/inbox.png" alt="" /><h1><?php _e( _( 'Inbox' ) ); ?></h1>
		</div>


		<div id="tabs">
  			<ul>
    			<li <?php if (active_link() == "private_message.php?p=new") _e("class='active_link'"); ?>><a href='<?php _e($_SERVER['PHP_SELF']); ?>?p=new'><span><?php _e( _( 'New' ) ); ?></span></a></li>
    			<li <?php if (active_link() == "private_message.php?p=send") _e("class='active_link'"); ?>><a href='<?php _e($_SERVER['PHP_SELF']); ?>?p=send'><span><?php _e( _( 'Send' ) ); ?></span></a></li>
    			<li <?php if (active_link() == "private_message.php?p=read") _e("class='active_link'"); ?>><a href='<?php _e($_SERVER['PHP_SELF']); ?>?p=read'><span><?php _e( _( 'Read' ) ); ?></span></a></li>
    			<li <?php if (active_link() == "private_message.php?p=deleted") _e("class='active_link'"); ?>><a href='<?php _e($_SERVER['PHP_SELF']); ?>?p=deleted'><span><?php _e( _( 'Deleted' ) ); ?></span></a></li>
  			</ul>
		</div>
		
<div id="middle">
	
<?php

 // check if a new message had been send
    if(isset($_POST['newmessage'])) {
        // check if there is an error while sending the message (beware, the input hasn't been checked, you should never trust users input!)
        if($pm->sendmessage($_POST['to'],$_POST['subject'],$_POST['message'])) {
            // Tell the user it was successful
            _e( '<div class="success">' . PP::notices(6) . '</div>' );
        } else {
            // Tell user something went wrong it the return was false
            _e( '<div class="error">' . PP::notices(7) . '</div>' );
        }
    }
    
    // check if a message had been deleted
    if(isset($_POST['delete'])) {
        // check if there is an error during deletion of the message
        if($pm->deleted($_POST['did'])) {
            _e( '<div class="success">' . PP::notices(8) . '</div>' );
        } else {
            _e( '<div class="error">' . PP::notices(9) . '</div>' );
        }
    }
?>
		
<br  /><br  />
<?php
// if it's the standart startpage or the page new, then show all new messages
if(!isset($_GET['p']) || $_GET['p'] == 'new') {
?>
<div id="pms">
<table border="0" cellspacing="1" cellpadding="1">
    <tr>
        <th><?php _e( _( 'From' ) ); ?></th>
        <th><?php _e( _( 'Title' ) ); ?></th>
        <th><?php _e( _( 'Date' ) ); ?></th>
    </tr>
    <?php
        // If there are messages, show them
        if(count($pm->messages)) {
            // message loop
            for($i=0;$i<count($pm->messages);$i++) {
                ?>
                <tr>
                    <td><?php _e( User::instance()->get_name($pm->messages[$i]['from']) ); ?></td>
                    <td><a href='<?php _e( $_SERVER['PHP_SELF'] ); ?>?p=view&mid=<?php _e( $pm->messages[$i]['id'] ); ?>'><?php _e( $pm->messages[$i]['title'] ); ?></a></td>
                    <td><?php _e( $pm->messages[$i]['created'] ); ?></td>
                </tr>
                <?php
            }
        } else {
            // else... tell the user that there are no new messages
            _e( "<tr><td colspan='3'><strong>No new messages found</strong></td></tr>" );
        }
    ?>
</table>
</div>
<?php
// check if the user wants send messages
} elseif($_GET['p'] == 'send') {
?>
<div id="pms">
<table border="0" cellspacing="1" cellpadding="1">
    <tr>
        <th><?php _e( _( 'To' ) ); ?></th>
        <th><?php _e( _( 'Title' ) ); ?></th>
        <th><?php _e( _( 'Status' ) ); ?></th>
        <th><?php _e( _( 'Date' ) ); ?></th>
    </tr>
    <?php
        // if there are messages, show them
        if(count($pm->messages)) {
            // message loop
            for($i=0;$i<count($pm->messages);$i++) {
                ?>
                <tr>
                    <td><?php _e( User::instance()->get_name($pm->messages[$i]['to']) ); ?></td>
                    <td><a href='<?php _e( $_SERVER['PHP_SELF'] ); ?>?p=view&mid=<?php _e( $pm->messages[$i]['id'] ); ?>'><?php _e( $pm->messages[$i]['title'] ); ?></a></td>
                    <td>
                    <?php  
                        // If a message is deleted and not viewed
                        if($pm->messages[$i]['to_deleted'] && !$pm->messages[$i]['to_viewed']) {
                            _e( "Deleted without reading" );
                        // if a message got deleted AND viewed
                        } elseif($pm->messages[$i]['to_deleted'] && $pm->messages[$i]['to_viewed']) {
                            _e( "Deleted after reading" );
                        // if a message got not deleted but viewed
                        } elseif(!$pm->messages[$i]['to_deleted'] && $pm->messages[$i]['to_viewed']) {
                            _e( "Read" );
                        } else {
                        // not viewed and not deleted
                            _e( "Not read yet" );
                        }
                    ?>
                    </td>
                    <td><?php _e( $pm->messages[$i]['created'] ); ?></td>
                </tr>
                <?php
            }
        } else {
            // else... tell the user that there are no new messages
            _e( "<tr><td colspan='4'><strong>No send messages found</strong></td></tr>" );
        }
    ?>
</table>
</div>
<?php
// check if the user wants the read messages
} elseif($_GET['p'] == 'read') {
?>
<div id="pms">
    <table border="0" cellspacing="1" cellpadding="1">
    <tr>
        <th><?php _e( _( 'From' ) ); ?></th>
        <th><?php _e( _( 'Title' ) ); ?></th>
        <th><?php _e( _( 'Date' ) ); ?></th>
    </tr>
    <?php
        // if there are messages, show them
        if(count($pm->messages)) {
            // message loop
            for($i=0;$i<count($pm->messages);$i++) {
                ?>
                <tr>
                    <td><?php _e( User::instance()->get_name($pm->messages[$i]['from']) ); ?></td>
                    <td><a href='<?php _e( $_SERVER['PHP_SELF'] ); ?>?p=view&mid=<?php _e( $pm->messages[$i]['id'] ); ?>'><?php _e( $pm->messages[$i]['title'] ); ?></a></td>
                    <td><?php _e( $pm->messages[$i]['to_vdate'] ); ?></td>
                </tr>
                <?php
            }
        } else {
            // else... tell the user that there are no new messages
            _e( "<tr><td colspan='4'><strong>No read messages found</strong></td></tr>" );
        }
    ?>
    </table>
</div>
<?php
// check if the user wants the deleted messages
} elseif($_GET['p'] == 'deleted') {
?>
<div id="pms">
    <table border="0" cellspacing="1" cellpadding="1">
    <tr>
        <th><?php _e( _( 'From' ) ); ?></th>
        <th><?php _e( _( 'Title' ) ); ?></th>
        <th><?php _e( _( 'Date' ) ); ?></th>
    </tr>
    <?php
        // if there are messages, show them
        if(count($pm->messages)) {
            // message loop
            for($i=0;$i<count($pm->messages);$i++) {
                ?>
                <tr>
                    <td><?php _e( User::instance()->get_name($pm->messages[$i]['from']) ); ?></td>
                    <td><a href='<?php _e( $_SERVER['PHP_SELF'] ); ?>?p=view&mid=<?php _e( $pm->messages[$i]['id'] ); ?>'><?php _e( $pm->messages[$i]['title'] ); ?></a></td>
                    <td><?php _e( $pm->messages[$i]['to_ddate'] ); ?></td>
                </tr>
                <?php
            }
        } else {
            // else... tell the user that there are no new messages
            _e( "<tr><td colspan='4'><strong>No deleted messages found</strong></td></tr>" );
        }
    ?>
</table>
</div>
<?php
// if the user wants a detail view and the message id is set...
} elseif($_GET['p'] == 'view' && isset($_GET['mid'])) {
    // if the users id is the recipients id and the message hadn't been viewed yet
    if($_SESSION['userID'] == $pm->messages[0]['toid'] && !$pm->messages[0]['to_viewed']) {
        // set the messages flag to viewed
        $pm->viewed($pm->messages[0]['id']);
    }
?>
    <table border="0" cellspacing="1" cellpadding="1">
        <tr>
            <td><?php _e( _( 'From:' ) ); ?></td>
            <td><?php _e( $pm->messages[0]['from'] ); ?></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><?php _e( _( 'Date:' ) ); ?></td>
            <td><?php _e( $pm->messages[0]['created'] ); ?></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td><?php _e( _( 'Subject:' ) ); ?></td>
            <td colspan="3"><?php _e( $pm->messages[0]['title'] ); ?></td>
        </tr>
        <tr>
            <td colspan="4"><?php _e( $pm->render($pm->messages[0]['message']) ); ?></td>
        </tr>
    </table>
    <form name='reply' method='post' action='<?php _e( $_SERVER['PHP_SELF'] ); ?>'>
        <input type='hidden' name='rfrom' value='<?php _e( $pm->messages[0]['from'] ); ?>' />
        <input type='hidden' name='rsubject' value='Re: <?php _e( $pm->messages[0]['title'] ); ?>' />
        <input type='hidden' name='rmessage' value='[quote]<?php _e( $pm->messages[0]['message'] ); ?>[/quote]' />
        <input type='submit' name='reply' class="sub_button" value='Reply' />
    </form>
    <br />
    <form name='delete' method='post' action='<?php _e( $_SERVER['PHP_SELF'] ); ?>'>
        <input type='hidden' name='did' value='<?php _e( $pm->messages[0]['id'] ); ?>' />
        <input type='submit' name='delete' class="sub_button" value='Delete' />
    </form>
<?php
}
?>

<div class="space"><!--Spacer--></div>

<table>
<form name="new" method="post" action="<?php _e( $_SERVER['PHP_SELF'] ); ?>">
<tr>
<td><label><strong><?php _e( _( 'To:' ) ); ?></strong></label></td>
<td><input class="forminput" type='text' name='to' value='<?php if(isset($_POST['reply'])) { _e( $_POST['rfrom'] ); } ?>' /></td>
</tr>

<tr>
<td><label><strong><?php _e( _( 'Subject:' ) ); ?></strong></label></td>
<td><input class="forminput" type='text' name='subject' value='<?php if(isset($_POST['reply'])) { _e( $_POST['rsubject'] ); } ?>' /></td>

<tr>
<td><label><strong><?php _e( _( 'Message:' ) ); ?></strong></label></td>
<td><textarea class="forminput" name='message'><?php if(isset($_POST['reply'])) { _e( $_POST['rmessage'] ); } ?></textarea></td>
</tr>
</table>
<input type='submit' name='newmessage' class="sub_button" value='Send' />
</form>
</div>
<?php include(PM_DIR . 'pm-includes/footer.php'); ?>