<?php 
session_start();
if(isset($_SESSION['logged'])) { header('Location: index.php'); }
define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

if(isset($_POST['login'])) {
	// calls the user login method/filter
	userAccess::pm_login($_POST['username'],$_POST['password'],$_POST['remember_me']);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title><?php _e( _( 'Login Form :: ' ) . get_pm_option( 'sitetitle' ) ); ?></title>
<link href="<?php _e( PM_URI ) ?>/css/form.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
<?php do_action( 'pm_login_head' ); ?>
</head>
<body>

<!-- Box Start-->
<div id="box_bg">
	<div id="pp-logo"><a href="<?php _e( PM_URI ); ?>"><?php _e( pp_logo( 80 ) ); ?></a></div>
<div id="content">
	<h1><?php _e( _( 'Sign In' ) ); ?></h1>
	
	<?php do_action( 'pm_login_form_top' ); ?>
	
	<!-- Login Fields -->
	<form name="login_form" action="pm-login.php" method="post">
	<div id="login"><?php _e( _( 'Sign in using your registered account:' ) ); ?><br/>
	<input type="text" name='username' onblur="if(this.value=='')this.value='Username';" onfocus="if(this.value=='Username')this.value='';" value="Username" class="login user<?php userAccess::login_error(); ?>"/>
	<input type='text' name='password' value='Password'  onfocus="if(this.value=='' || this.value == 'Password') {this.value='';this.type='password'}"  onblur="if(this.value == '') {this.type='text';this.value=this.defaultValue}" class="login password<?php userAccess::login_error(); ?>"/>
	</div>
	
	<br />
	
	<?php do_action( 'pm_login_form_bottom' ); ?>
	
	<!-- Green Button -->
	<div class="button input"><input class="button green" type="submit" value="Sign In" name="login" /></div>

	<!-- Checkbox -->
	<div class="checkbox">
	<li>
	<fieldset>
	<![if !IE | (gte IE 8)]><legend id="title2" class="desc"></legend><![endif]>
	<!--[if lt IE 8]><label id="title2" class="desc"></label><![endif]-->
	<div>
		<span>
		<input id="Field" name="remember_me" type="checkbox" class="field checkbox" value="First Choice" tabindex="4" onchange="handleInput(this);" />
		<label class="choice" for="Field"><?php _e( _( 'Keep me signed in' ) ); ?></label>
		</span>
	</div>
	</fieldset>
	</li>
	</div>
	</form>

</div>
</div>

<!-- Text Under Box -->
<div id="bottom_text">
	<?php if(get_pm_option('enable_registration') == 'yes') { ?>
	<?php _e( _( 'Don\'t have an account?' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-register.php"><?php _e( _( 'Sign Up' ) ); ?></a><br/>
	<?php } ?>
	<?php _e( _( 'Remind' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-forgot.php"><?php _e( _( 'Password' ) ); ?></a>
</div>

</body>
</html>