<?php 
/**
 * ProjectPress list of members subscribed to a project
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

// Checks if user is logged in; if not redirect to login page.
if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_POST['pmuser'])) {
	pmdb::connect()->delete( DB . "project_members", "pp_id = " . $_GET['p_id'] . " AND pm_user = '" . $_POST['pm_user'] . "'");
}

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/address-book-blue.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> Members</h1>
</div>
				
	<?php _e( get_project_tabs() ); ?>

				<div id="middle">
				<div id="group-directory">
						<table cellpadding="0" cellspacing="0" border="0" align="center">
							<th>Avatar</th>
							<th>Name</th>
							<th>Email</th>
							<th <?php if(is_session_set('username') != get_project_meta($_GET['p_id'], 'creator')) { _e( 'style="display:none;"' ); } ?>>Delete</th>
							
				<form method="post" action="<?php _e( PM_URI ); ?>/projects/project_members.php?p_id=<?php _e( $_GET['p_id'] ); ?>">
				<?php
				$query = "SELECT * FROM ".DB."project_members, ".DB."members WHERE pm_user = username AND pp_id = '".$_GET['p_id']."' ORDER BY last_name ASC";
				$result = pmdb::connect()->query($query);

					if($result->num_rows > 0) {
       					 while($row = $result->fetch_object()) { ?>
        				<tr>
        					<td><?php _e( get_user_avatar($row->username,$row->email) ); ?></td>
        					<td><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row->username ); ?>"><?php _e( User::instance()->get_name( $row->username ) ); ?></a></td>
        					<td><?php _e( clickable_link($row->email) ); ?></td>
        					<td <?php if(is_session_set('username') != get_project_meta($_GET['p_id'], 'creator')) { _e( 'style="display:none;' ); } ?>><input name="pm_user" type="checkbox" value="<?php _e( $row->pm_user ); ?>" /></td>
        				</tr>
				<?php
       				 }
						} else {
						_e( "<tr><td>There are no members in this group.</td></tr>" );
					}
				?>
					<?php if(is_session_set('username') == get_project_meta($_GET['p_id'], 'creator')) { ?>
						<tr>
        					<td colspan="4"><input style="float:right !important;" type="submit" value="Delete Member(s)" name="pmuser" id="sub_button" /></td>
        				</tr>
        			<?php } ?>
				</form>
						</table>
				</div>
				</div>


<?php include(PM_DIR . 'pm-includes/footer.php');