<?php
/**
 * ProjectPress add project type
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access', true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// Checks if user is logged in; if not redirect to login page.
is_admin();

// Enable for error checking and troubleshooting.
# display_errors();

if($_POST) {
	pmdb::connect()->insert( DB . "project_types", array('pt_id', pmdb::connect()->escape( $_POST['project_type']), date('Y-m-d H:i:s')) );
} else { }

?>

<li class="project_type"><?php _e($_POST['project_type']);?>&nbsp;&nbsp;&nbsp;&nbsp;<a style="float:right;" href="#" id="PTID-<?php _e($row['pt_id']);?>" class="pt_delete"><img src="<?php _e(PM_URI); ?>/images/delete.png" alt="Delete" title="Delete" /></a></li>