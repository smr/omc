<?php 
/**
 * ProjectPress add a project
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$sql = pmdb::connect()->select( DB . 'project_types', '*', null, null );

if($_POST) {
	
	$project = array('LAST_INSERT_ID()',
					pmdb::connect()->escape($_POST['ptype_id']),
					pmdb::connect()->escape($_POST['project_name']),
					pmdb::connect()->escape($_POST['project_description']),
					pmdb::connect()->escape($_SESSION['username']),
					pmdb::connect()->escape($_POST['contact_email'])
					);

	pmdb::connect()->insert( DB . "projects", $project );

	//$pID = pmdb::connect()->query("SELECT p_id FROM " . DB . "projects WHERE project_name = '" . pmdb::connect()->escape($_POST['project_name']) . "'");
	$pID = pmdb::connect()->select( DB . 'projects', 'p_id', 'project_name = "' . pmdb::connect()->escape($_POST['project_name']) . '"', null );
	$r = $pID->fetch_object();

	if($r) {
		pmdb::connect()->insert( DB . 'project_leaders', array($r->p_id, $_SESSION['username']), 'p_id,pl_user' );
		pmdb::connect()->insert( DB . 'project_members', array($r->p_id, $_SESSION['username']), 'pp_id,pm_user' );
	}
}
	/**
	 * Creates a new template for the add member page.
	 */
	while($row = $sql->fetch_array()) {
		$addproject = new Template(PM_DIR . "pm-includes/tpl/list_add_project_row.tpl");
		$addprojectTemplates[] = $addproject;
		$addproject->set("typeid", $row['pt_id']);
		$addproject->set("type", $row['project_type']);
		$addproject->set("success", PP::notices(33));
		$addproject->set("error", PP::notices(34));
	}
	
	/**
	 * Merges all our members' templates into a single variable.
	 * This will allow us to use it in the main template.
	 */
	$addprojectContents = Template::merge($addprojectTemplates);
	
	/**
	 * Defines the main template and sets the members' content.
	 */
	$addprojectList  = new Template(PM_DIR . "pm-includes/tpl/list_add_project.tpl");
	$addprojectList->set("projecttype", $addprojectContents);
	
	/**
	 * Loads our layout template, setting its site url, site title
	 * and content.
	 */
	$layout = new Template(PM_DIR . "pm-includes/tpl/add_project.tpl");
	$layout->set("pmurl", get_pm_option('siteurl'));
	$layout->set("content", $addprojectList->output());
	
	/**
	 * Outputs the page with the members list.
	 */
	echo $layout->output();

include(PM_DIR . 'pm-includes/footer.php');