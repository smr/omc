<?php 
/**
 * ProjectPress project reply forum topic
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
if(!isset($_GET['p_id']) || $_GET['p_id'] != get_project_meta($_GET['p_id'],'p_id')) { pm_redirect( PM_URI . '/projects/list_projects.php' ); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if($_POST) {

	$sql = pmdb::connect()->insert( DB . 'project_forum_topics', array( 'LAST_INSERT_ID()',
																		$_GET['p_id'],
																 		pmdb::connect()->escape($_POST['pft_topic']),
																 		pmdb::connect()->escape($_POST['pft_detail']),
																 		$_SESSION['username'],
																 		pmdb::connect()->escape(date("d/m/y h:i:s"))
															    	   ),
															    		'pft_id,
															    		pt_id,
															    		pft_topic,
															    		pft_detail,
															    		pft_user,
															    		pft_datetime'
						    );
	$results = pmdb::connect()->get_row( "SELECT * FROM " . DB . "project_forum_topics WHERE pft_topic = '" . $_POST['pft_topic'] . "'" );
	
	if( $sql ) {
		pm_redirect( 'view_ptopic.php?p_id=' . $results->pt_id . '&id=' . $results->pft_id );
	} else {
		$message = '<div class="error">' . PP::notices(3) . '</div>'; exit;
	}

}

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum' ) ); ?></h1>
</div>
				
	<?php _e( get_project_tabs() ); ?>
	<?php _e( $message ); ?>


<div id="middle">
<a href="<?php _e( PM_URI ); ?>/projects/forum/discussions.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum Homepage' ) ); ?></a><br /><br />
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
		<tr>
		<form id="form1" name="form1" method="post" action="">
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td bgcolor="#E6E6E6"><strong><?php _e( _( 'Create New Topic' ) ); ?></strong></td>
					</tr>
					<tr>
						<td vAlign="top"><strong><?php _e( _( 'Topic:' ) ); ?></strong><br />
						<input class="forminput" name="pft_topic" type="text" id="pft_topic" size="50" style="width:500px;" /></td>
					</tr>
					<tr>
						<td vAlign="top"><strong><?php _e( _( 'Detail:' ) ); ?></strong><br />
						<textarea class="forminput" name="pft_detail" cols="52" rows="15" id="pft_detail" style="width:500px;"></textarea></td>
					</tr>
					<tr>
						<td><br />
						<input id="sub_button" type="submit" name="newpTopic" value="Submit" /> <input id="sub_button" type="reset" name="Submit2" value="Reset" /></td>
					</tr>
				</table>
			</td>
		</form>
		</tr>
	</table>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');