<?php
/**
 * ProjectPress project forum reply
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

$sql = pmdb::connect()->select( DB . 'project_forum_posts', 'MAX(p_id) AS Maxp_id', 'topic_id = "' . $_POST['id'] . '" AND pp_id = "' . $_GET['p_id'] . '"', null );
$rows = $sql->fetch_object();

// add + 1 to highest answer number and keep it in variable name "$Max_id". if there no post yet set it = 1 
if($rows) {
$Max_id = $rows->Maxp_id+1;
} else {
$Max_id = 1;
}

// Insert post
if ($_POST['pfp_post'] == "") { // Checks for blanks.
		exit("There was a field missing, please correct the form.");
	} else { 
$sql2 =pmdb::connect()->insert( DB . 'project_forum_posts', array( pmdb::connect()->escape($_POST['id']),
																   $_GET['p_id'],
																   $_SESSION['username'],
																   pmdb::connect()->escape($_POST['pfp_post']),
																   pmdb::connect()->escape(date("y-m-d H:i:s"))
																  ),
																  'topic_id,
																  pp_id,
																  pfp_user,
																  pfp_post,
																  pfp_datetime'
							   );
}

if($sql2){
	pm_redirect('view_ptopic.php?p_id='.$_GET['p_id'].'&id='.$_GET['id']);

// If added new post, add value +1 in reply column 
if ($_POST['pfp_post'] == "") { // Checks for blanks.
		exit("There was a field missing, please correct the form.");
	} else {
$sql3 = pmdb::connect()->update( DB . 'project_forum_topics', array( 'pft_reply' => $Max_id ), array( 'pt_id',$_GET['p_id'] ) );
	}
} else {
echo "ERROR";
}