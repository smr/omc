<?php
/**
 * ProjectPress project delete form reply
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum' ) ); ?></h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<div id="middle">				
<?php

if($_GET['pft_id']) {
    
	$delete_topic = pmdb::connect()->delete( DB . 'project_forum_topics', 'pft_id = "' . $_GET['pft_id'] . '" AND pt_id = "' . $_GET['p_id'] . '"' );
	$delete_posts = pmdb::connect()->delete( DB . 'project_forum_posts', 'topic_id = "' . $_GET['pft_id'] . '" AND pp_id = "' . $_GET['p_id'] . '"' );

	if($delete_topic && $delete_posts) {
		pm_redirect( 'discussions.php?p_id=' . $_GET['p_id'] );
	} else {
		_e( '<div class="error">Something went very wrong.  <a href="' . PM_URI . '/projects/forum/discussions.php?p_id='.$_GET['p_id'].'">Click here</a> to go back to the project forum and try again.</div>' );
	}
}
?>

</div>

<?php include(PM_DIR . 'pm-includes/footer.php');