<?php
/**
 * ProjectPress view project forum topic
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$sql = pmdb::connect()->select( DB . 'project_forum_topics, ' . DB . 'members', '*', 'pt_id = "' . $_GET['p_id'] . '" AND pft_id = "' . $_GET['id'] . '" AND pft_user = username', null );
	$row_a = $sql->fetch_object();
?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum' ) ); ?></h1>
</div>
				
	<?php _e( get_project_tabs() ); ?>
				
<div id="middle">
<h2><?php _e( _( 'Topic:' ) ); ?> <a href="<?php _e( PM_URI ); ?>/projects/forum/view_ptopic.php?p_id=<?php _e( $_GET['p_id'] ); ?>&id=<?php _e( $_GET['id'] ); ?>"><?php _e( $row_a->pft_topic ); ?></a></h2>
<a href="<?php _e( PM_URI ); ?>/projects/forum/discussions.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><?php _e( _( 'Go Back Forum Homepage' ) ); ?></a><br /><br />
<table width="504" align="center" border="0" bgcolor="#DDD">
	<tr>
		<td align="left"><strong>Title:</strong> <?php _e( $row_a->pft_topic ); ?> </td> <td align="right"><strong>Date: </strong><?php _e( date('M d, Y h:i A',strtotime($row_a->pft_datetime)) ); ?></td>
	</tr>
</table>

<table width="504" border="0" align="center" cellpadding="0" cellspacing="0">

<tr>
<td vAlign="top" class="forum">
	<table width="484" border="0">
		<tr>
			<td width="100%" class="forum-topic"><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row_a->pft_user ); ?>"><strong><?php _e( $row_a->first_name ); ?> <?php _e( $row_a->last_name ); ?></strong></a> <?php _e( $row_a->pft_detail ); ?></td>
		</tr>
		<?php if($row_a->signature != '') { ?>
		<tr>
			<td class="forum-signature">
				<div class="avatar">
				<?php _e( get_user_avatar($row_a->username,$row_a->email) ); ?>
				</div>
				
				<div class="signature">
				<?php _e( nl2br($row_a->signature) ); ?>
				</div>
			</td>
		</tr>
		<?php } ?>
	</table>
</td>
</tr>
</table>

<br />
<?php 
	$sql2 = pmdb::connect()->select( DB . 'project_forum_posts, ' . DB . 'members', '*', 'topic_id = "' . $_GET['id'] . '" AND pp_id = "' . $_GET['p_id'] . '" AND pfp_user = username', 'p_id ASC' );
	while($row_b = $sql2->fetch_object()){
?>
<table width="504" align="center" border="0" bgcolor="#C6E2FF">
	<tr>
		<td align="left"><strong><?php _e( _( 'Re:' ) ); ?></strong> <?php _e( $row_a->pft_topic ); ?> </td> <td align="right"><strong><?php _e( _( 'Reply Date:' ) ); ?> </strong><?php _e( date('M d, Y h:i A',strtotime($row_b->pfp_datetime)) ); ?></td>
	</tr>
</table>

<table width="504" border="0" align="center" cellpadding="0" cellspacing="0" class="forum-1">

<tr>
<td vAlign="top" class="forum">
	<table width="484" border="0">
		<tr>
			<td width="100%"><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row_b->pfp_user ); ?>"><strong><?php _e( $row_b->first_name ); ?> <?php _e( $row_b->last_name ); ?></strong></a> <?php _e( nl2br($row_b->pfp_post) ); ?></td>
		</tr>
		<tr>
			<td class="forum-topic"><?php if($row_b->pfp_post && $row_b->pfp_user == $_SESSION['username']) _e( '<a href="edit_preply.php?p_id='.$_GET['p_id'].'&id='.$row_b->p_id.'">Edit</a> | ' ); ?> <?php if($row_b->pfp_post && $row_b->pfp_user == $_SESSION['username']) _e( '<a href="delete_pfp.php?p_id='.$_GET['p_id'].'&id='.$row_b->p_id.'">Delete</a>' ); ?></td>
		</tr>
		<?php if($row_b->signature != '') { ?>
		<tr>
			<td class="forum-signature">
				<div class="avatar">
				<?php _e( get_user_avatar($row_b->username,$row_b->email) ); ?>
				</div>
				
				<div class="signature">
				<?php _e( nl2br($row_b->signature) ); ?>
				</div>
			</td>
		</tr>
		<?php } ?>
	</table>
</td>
</tr>
</table>

<?
}

$sql3 = pmdb::connect()->select( DB . 'project_forum_topics', 'pft_view', 'pft_id = "' . $_GET['id'] . '" AND pt_id = "' . $_GET['p_id'] . '"', null );

$rows = $sql3->fetch_object();
$pft_view = $rows->pft_view;

$addview = $pft_view+1;
$sql4 = pmdb::connect()->update( DB . 'project_forum_topics', array( 'pft_view' => $addview ), array( 'pt_id',$_GET['p_id'] ) );
?>
<br />
	<table width="495" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
		<tr>
			<form name="form1" method="post" action="<?php _e( PM_URI ); ?>/projects/forum/preply.php?p_id=<?php _e( $_GET['p_id'] ); ?>&id=<?php _e( $_GET['id'] ); ?>">
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td valign="top"><strong><?php _e( _( 'Quick Reply' ) ); ?></strong><br />
						<textarea class="forminput" name="pfp_post" cols="45" rows="3" id="pfp_post" style="width:500px;"></textarea></td>
					</tr>
					<tr>
						<td><br />
							<input name="id" type="hidden" value="<?php _e( $_GET['id'] ); ?>">
							<input type="submit" name="Submit" value="Submit" id="sub_button"> <input type="reset" name="Submit2" value="Reset" id="sub_button"></td>
					</tr>
				</table>
			</td>
			</form>
		</tr>
	</table>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');