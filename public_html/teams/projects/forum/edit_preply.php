<?php
/**
 * ProjectPress project forum reply edit
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();
?>


<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum' ) ); ?></h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<?php
if (isset($_POST['edit']) && $_POST['edit'] == 'Submit') {
	
	$query = pmdb::connect()->update( DB . 'project_forum_posts', array( 'pfp_post' => pmdb::connect()->escape($_POST['pfp_post']) ), array ( 'p_id', $_GET['id'], 'pp_id', $_GET['p_id'], 'pfp_user', $_SESSION['username'] ) );
    
	if($query) {
		pm_redirect( 'view_ptopic.php?p_id=' . $_GET['p_id'] . '&id=' . $_GET['id'] );
	} else {		
	    _e( '<div class="error">' . PP::notices(5) . '</div>' );				
	}
}

$results = pmdb::connect()->get_row( "SELECT pfp_post FROM ". DB ."project_forum_posts WHERE pp_id = '" . $_GET['id'] . "' AND p_id = '" . $_GET['id'] . "' AND pfp_user = '" . $_SESSION['username'] . "'" );

?>

<div id="middle">
<a href="<?php echo PM_URI ?>/projects/forum/discussions.php?p_id=<?php _e( $_GET['p_id'] ); ?>">Go Back Forum Homepage</a><br /><br />
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
		<tr>
		<form id="form2" name="form2" method="post" action="<?php _e( PM_URI ); ?>/projects/forum/edit_preply.php?p_id=<?php _e( $_GET['p_id'] ); ?>&id=<?php _e( $_GET['id'] ); ?>">
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td valign="top"><strong><?php _e( _( 'Edit Reply' ) ); ?></strong><br />
						<textarea class="forminput" name="pfp_post" cols="60" rows="20" id="pfp_post"><?php _e( $results->pfp_post ); ?></textarea></td>
					</tr>
					<tr>
						<td><br />
						<input id="sub_button" type="submit" name="edit" value="Submit" /> <input id="sub_button" type="reset" name="Submit2" value="Reset" /></td>
					</tr>
				</table>
			</td>
		</form>
		</tr>
	</table>
</div>

	
<?php include(PM_DIR . 'pm-includes/footer.php');