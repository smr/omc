<?php
/**
 * ProjectPress project delete form reply
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Forum' ) ); ?></h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<div id="middle">				
<?php

if($_GET['p_id']) {
    
	$delete = pmdb::connect()->delete( DB . 'project_forum_posts', 'p_id = "' . $_GET['p_id'] . '" AND pp_id = "' . $_GET['id'] . '" AND pfp_user = "' . $_SESSION['username'] . '"' );
	
	if($delete) {
		pm_redirect( 'view_ptopic.php?p_id=' . $_GET['p_id'] . '&id=' . $_GET['id'] );
	} else {
		_e( '<div class="error">You forgot to choose a message to delete.  <a href="' . PM_URI . '/projects/forum/">Click here</a> to go back to the main project forum and try again.</div>' );
	}
}
?>

</div>

<?php include(PM_DIR . 'pm-includes/footer.php');