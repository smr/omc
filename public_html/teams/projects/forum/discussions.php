<?php 
/**
 * ProjectPress project forum topics
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$sql = pmdb::connect()->select( DB . 'project_forum_topics', '*', 'pt_id = "' . $_GET['p_id'] . '"', null );

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> Forum</h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<div id="middle">
	<div id="groups-page">
		<h2><?php _e( _( 'Forum Topics' ) ); ?></h2>
		<table width="504" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
			<tr>
				<td width="63%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Topic' ) ); ?></strong></td>
				<td width="13%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Replies' ) ); ?></strong></td>
				<td width="19%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Date/Time' ) ); ?></strong></td>
				<td width="5%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( ' ' ) ); ?></strong></td>
			</tr>

			<?php
				while($rows = $sql->fetch_object()) {
				$query = pmdb::connect()->select( DB . 'project_forum_posts', 'COUNT(p_id)', 'topic_id = "' . $rows->pt_id . '"', null );
				$r = $query->fetch_array();
			?>
			<tr>
				<td bgcolor="#FFFFFF"><a style="margin-left:5px;" href="view_ptopic.php?p_id=<?php _e( $_GET['p_id'] ); ?>&id=<?php _e( $rows->pft_id ); ?>"><?php _e( $rows->pft_topic ); ?></a><br /></td>
				<td align="center" bgcolor="#FFFFFF"><?php _e( $r['COUNT(p_id)'] ); ?></td>
				<td align="center" bgcolor="#FFFFFF"><?php _e( date('M d, y h:i A',strtotime($rows->pft_datetime)) ); ?></td>
				<td align="center" bgcolor="#FFFFFF"><a onclick="if(confirm('Really delete the entire topic?')) return true; else return false;" href="<?php _e( PM_URI ); ?>/projects/forum/delete_pft.php?pft_id=<?php _e( $rows->pft_id ); ?>&amp;p_id=<?php _e( $_GET['p_id'] ); ?>">Delete</a></td>
			</tr>
			<?php } ?>

			<tr>
				<td colspan="5" align="right" bgcolor="#E6E6E6"><a href="<?php _e( PM_URI ); ?>/projects/forum/add_ptopic.php?p_id=<?php _e( $_GET['p_id'] ); ?>"><strong><?php _e( _( 'Create New Topic' ) ); ?></strong> </a></td>
			</tr>
		</table>
	</div>
</div><!--middle-->

<?php include(PM_DIR . 'pm-includes/footer.php');