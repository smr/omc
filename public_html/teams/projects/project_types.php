<?php 
/**
 * ProjectPress add project types
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<script type="text/javascript">
$(function() {
$(".pt-submit").click(function() {
var project_type = $("#project_type").val();
var dataString = 'project_type=' + project_type;

if(project_type=='')
{
alert('You need to enter a project type.');
}
else
{
$("#flash").show();
$("#flash").fadeIn(400).html('<img src="<?php echo PM_URI ?>/images/ajax-loader.gif" />Loading Project Type...');
$.ajax({
type: "POST",
url: "insert_pt.php",
data: dataString,
cache: false,
success: function(html) {
$('#project_type').val('');
$("ul#update").append(html);
$("ul#update li:last").fadeIn("slow");
$("#flash").hide();
}
});
}
return false;
});

		//deleteUserType
		$('a.pt_delete').livequery("click", function(e){
			
			if(confirm('Are you sure you want to delete this project type?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var pt_id =  $(this).attr('id').replace('PTID-','');	
			
			$.ajax({

				type: 'get',

				url: 'dpt.php?pt_id='+ pt_id,

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						parent.remove();

					});

				}

			});
		});
});
</script>
<style type="text/css">	
	#flash
	{
	margin-left:100px;
	
	}
	ul#update {
	margin-top:25px;
	}
	.project_type
	{
	height:20px;
	border-bottom:#DEDEDE dashed 1px;
	margin-bottom:10px;
	}
</style>

				<div id="page-title">
					<img src="<?php echo PM_URI ?>/images/admin.png" alt="" /><h1>Project Types</h1>
				</div>

			<div id="middle">
			<form name="form" action="" method="post">
			<input type="text" class="forminput" id="project_type" name="project_type" />
			<input type="submit" class="pt-submit" id="sub_button" value="Add Project Type">
			</form>
			
			<ul id="update" class="project_type">
			<?php
			$query = pmdb::connect()->query("SELECT * FROM ".DB."project_types ORDER BY project_type ASC");
			while($row = $query->fetch_object())
			{
			?>
			<!--Displaying existing user types.-->
			<li class="project_type"><?php echo $row->project_type; ?>&nbsp;&nbsp;&nbsp;&nbsp;<a style="float: right;" href="#" id="PTID-<?php echo $row->pt_id;?>" class="pt_delete"><img src="<?php echo PM_URI ?>/images/delete.png" alt="Delete" title="Delete" /></a></li>
			<?php
			}
			?>
			</ul>
			<div id="flash"></div>

			</div>


<?php include(PM_DIR . 'pm-includes/footer.php');