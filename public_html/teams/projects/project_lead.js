$(document).ready(function() {

		// click to add project leader
		$('#AddLead').click(function(){

			var a = $("#pl_user").val();
			if(a != "Add project leader's username.")
			{
				$.post("p_lead.php?val=1&"+$("#LeadForm").serialize(), {
	
				}, function(response){
					$('#show_lead').prepend($(response).fadeIn('slow'));
					clearPLForm();
				});
			}
			else
			{
				alert("Can't be blank.");
				$("#pl_user").focus();
			}
		});
		
		// delete project leader
		$('a.pl_remove').livequery("click", function(e){
			if(confirm('Are you sure you want to delete?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var p_id =  $(this).attr('id').replace('lead-','');
			var pl_user =  $(this).attr('ce').replace('pl_user-','');
			
			$.ajax({
				type: 'get',
				url: 'dpl.php?p_id='+ p_id + '&pl_user=' + pl_user,
				data: '',
				beforeSend: function(){
				},
				success: function(){
					parent.fadeOut(200,function(){
						parent.remove();
					});
				}
			});
		});	
		
		// show form when input get focus
		$('#pl_user').focus(function(){
			$('#LeadBox').fadeIn();
			$('a.pl_cancel').fadeIn();
		});	
		
		// hide for when click on cancel
		$('a.pl_cancel').click(function(){
			$('#LeadBox').fadeOut();
			$('a.pl_cancel').hide();
		});	
		
		// watermark input fields
		jQuery(function($){
		   
		   $("#pl_user").Watermark("Add project leader's username.");
		   
		});
		jQuery(function($){

		    $("#pl_user").Watermark("watermark","#369");
			
		});	
		function UseData(){
		   $.Watermark.HideAll();
		   $.Watermark.ShowAll();
		}
	
	// end function	
	});
	
		function clearPLForm()
	{
		$('#pl_user').val("Add project leader's username.");
		
		$('#LeadBox').hide();
		$('a.pl_cancel').hide();
	}