<?php 
/**
 * ProjectPress document manager
 *
 * @package ProjectPress
 * @since 3.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

// Checks if user is logged in; if not redirect to login page.
if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$message = "";

	if(!file_exists('uploads/' . $_GET['p_id'])) {
		mkdir('uploads/' . $_GET['p_id'], 0755);
	}

	$pfolder = 'uploads/' . $_GET['p_id'];

	$uploadFolder = $pfolder . '/';

	//Has the user tried to delete something?
    if(isset($_GET['rm'])) {
        //Is he the project owner?
        if(is_session_set('username') == get_project_meta($_GET['p_id'],'creator')) {
            if(unlink($uploadFolder . $_GET['rm'])) {
                $message = 'The file was deleted!';
            } else {
                $message = 'There was an error deleting the file!';
            }
        } else {
            $message = 'Only project owner is allowed to delete uploaded files!';
        }
        //Clear the array
		unset($_get['rm']);

		//pm_redirect( 'filemanager.php?p_id=' . $_GET['p_id'] );
    }

	//Has the user uploaded something?
	if(isset($_FILES['file'])) {
		$target_path = $uploadFolder;
		$target_path = $target_path . time() . '_' . basename( $_FILES['file']['name']);
	
		//Try to move the uploaded file into the designated folder
		if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
			$message = "The file ".  basename( $_FILES['file']['name']). 
			" has been uploaded";
		} else{
		    $message = "There was an error uploading the file, please try again!";
		}	
		//Clear the array
		unset($_FILES['file']);
	
		pm_redirect( 'filemanager.php?p_id=' . $_GET['p_id'] );
	}

	if(strlen($message) > 0) {
		$message = '<p class="error">' . $message . '</p>';
	}

	/** LIST UPLOADED FILES **/
	$uploaded_files = "";

	//Open directory for reading
	$dh = opendir($uploadFolder);

	//LOOP through the files
	while (($file = readdir($dh)) !== false)  {
		if($file != '.' && $file != '..') {
			$filename = $uploadFolder . $file;
			$parts = explode("_", $file);
			$size = formatBytes(filesize($filename));
			$added = date("m/d/Y", $parts[0]);
			$origName = $parts[1];
			$filetype = getFileType(substr($file, strlen($file) - 3));
        	$uploaded_files .= '<li class="' . $filetype . '"><a href="' . $filename . '">' . $origName . '</a> ' . $size . '-' . $added
                            . '<span style="display: inline-block; width: 20%; float:right; text-align: center;">
                                    <a href="filemanager.php?p_id=' . $_GET['p_id'].'&amp;rm='.$file.'">
                                        Delete
                                    </a>
                               </span>'
                            . '</li>' . "\n";
		}
	}
	closedir($dh);

	if(strlen($uploaded_files) == 0) {
		$uploaded_files = "<li><em>No files found</em></li>";
	}

?>		

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/folder.png" alt="" /><h1><?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> <?php _e( _( 'Document Manager' ) ); ?></h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<div id="container">
<div id="middle">

<?php if(is_session_set('username')) {// == get_project_meta($_GET['p_id'],'creator')) { ?>
<form method="post" action="filemanager.php?p_id=<?php _e( $_GET['p_id'] ); ?>" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
	<fieldset>
		<legend><?php _e( _( 'Add a new file to the storage' ) ); ?></legend>
			<?php _e( $message ); ?>
			<p><label for="name"><?php _e( _( 'Select file' ) ); ?></label><br />
			<input type="file" name="file" /></p>
			<p><input type="submit" name="submit" id="sub_button" value="Start upload" /></p>
	</fieldset>
</form>
<?php } ?>

	<fieldset>
		<legend><?php _e( _( 'Previousely uploaded files' ) ); ?></legend>
			<ul id="menu">
				<li><a href=""><?php _e( _( 'All files' ) ); ?></a></li>
				<li><a href=""><?php _e( _( 'Documents' ) ); ?></a></li>
				<li><a href=""><?php _e( _( 'Images' ) ); ?></a></li>
				<li><a href=""><?php _e( _( 'Applications' ) ); ?></a></li>
			</ul>
			
			<ul id="files">
				<?php _e( $uploaded_files ); ?>
			</ul>
	</fieldset>

<!--<script src="http://code.jquery.com/jquery-latest.js"></script>-->
<script src="<?php _e( get_javascript_directory_uri() ); ?>/filestorage.js" ></script>

</div>
</div>
<?php include(PM_DIR . 'pm-includes/footer.php');