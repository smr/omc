<?php 
/**
 * ProjectPress list of members
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

// Checks if user is logged in; if not redirect to login page.
if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );
if(is_session_set('username') != get_project_meta($_GET['p_id'],'creator')) { pm_redirect( PM_URI . '/projects/list_projects.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

$proj = pmdb::connect()->query("SELECT * FROM ".DB."projects, ".DB."project_types WHERE ".DB."projects.p_id = '".$_GET['p_id']."' AND ".DB."projects.ptype_id = ".DB."project_types.pt_id");
$r = $proj->fetch_array();

if($_POST) {

	$project = array( 'project_name' => pmdb::connect()->escape($_POST['project_name']),
					  'ptype_id' => pmdb::connect()->escape($_POST['ptype_id']),
					  'project_description' => pmdb::connect()->escape($_POST['project_description']),
					  'contact_email' => pmdb::connect()->escape($_POST['contact_email'])
					);
	
	$sql = pmdb::connect()->update( DB . "projects", $project, array('p_id',$_GET['p_id']) );

}
?>

<script type="text/javascript" >
$(function() {
$(".ep-submit").click(function() {
var project_name = $("#project_name").val();
var ptype_id = $("#ptype_id").val();
var project_description = $("#project_description").val();
var contact_email = $("#contact_email").val();
var dataString = 'project_name='+ project_name + '&project_description='+ project_description + '&ptype_id='+ ptype_id + 
'&contact_email='+ contact_email;

if(project_name=='' || project_description=='' || ptype_id=='' || contact_email=='')
{
$('.success').fadeOut(200).hide();
$('.error').show();
$('.error').fadeOut(3000);
}
else
{
$.ajax({
type: "POST",
url: "edit_project.php?p_id=<?php _e($r['p_id']); ?>",
data: dataString,
success: function(){
$('.success').show();
$('.success').fadeOut(1500);
$('.error').fadeOut(200).hide();
}
});
}
return false;
});
});
</script>

				<div id="page-title">
					<img src="<?php _e(PM_URI); ?>/images/projects.png" alt="" /><h1>Edit Project</h1>
				</div>
				
				<div class="success" style="display:none"><?php _e( PP::notices(35) ); ?></div>
				<div class="error" style="display:none"><?php _e( PP::notices(36) ); ?></div>

				<div id="middle">
					<form name="form" action="" method="post">
					<table cellpadding="0" cellspacing="5" align="center" width="100%">
					<tbody>
				
						<tr>
							<th>Project Name:</th>
							<td><input class="forminput" id="project_name" name="project_name" type="text" size="30" value="<?php _e($r['project_name']); ?>" /></td>
						</tr>
						
						<tr>
							<th>Group Type:</th>
							<td><select id="ptype_id" class="forminput">
								<?php
								$result = pmdb::connect()->query("SELECT * FROM ".DB."project_types");
								while($row = $result->fetch_array()) {
								if($r['pt_id'] == $row['pt_id']) { ?>
							    <option value="<?php _e($row['pt_id']);?>" selected><?php _e($row['project_type']); ?></option>
							    <?php } else { ?>
							    <option value="<?php _e($row['pt_id']);?>"><?php _e($row['project_type']); ?></option>
							    <?php } } ?>
						</tr>
						
						<tr>
							<th>Project Description:</th>
							<td><textarea class="forminput" id="project_description" name="project_description" cols="50" rows="10"><?php _e($r['project_description']); ?></textarea></td>
						</tr>
						
						<tr>
							<th>Contact Email:</th>
							<td><input class="forminput" id="contact_email" name="contact_email" type="text" size="30" value="<?php _e($r['contact_email']); ?>" /></td>
						</tr>
						
						<tr>
							<td></td>
							<td colspan="2"><input type="submit" value="Submit" name="submit" class="ep-submit" id="sub_button" /></td>
						</tr>

					</tbody>
	 				</table>
</form>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');