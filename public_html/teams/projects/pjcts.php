<?php 
/**
 * ProjectPress manage projects
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<script type="text/javascript">
$(function() {

		//delete group
		$('a.p_delete').livequery("click", function(e){
			
			if(confirm('Are you sure you want to delete this project?')==false)

			return false;
	
			e.preventDefault();
			var parent  = $(this).parent();
			var p_id =  $(this).attr('id').replace('PID-','');	
			
			$.ajax({

				type: 'get',

				url: 'dp.php?p_id='+ p_id,

				data: '',

				beforeSend: function(){

				},

				success: function(){

					parent.fadeOut(200,function(){

						parent.remove();

					});

				}

			});
		});
});

</script>

				<div id="page-title">
					<img src="<?php echo PM_URI; ?>/images/groups.png" alt="" /><h1>Manage Projects</h1>
				</div>

				<div id="middle">
				<div id="groups">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="504px">

<?php
$query = 'SELECT * FROM '.DB.'projects, '.DB.'project_types WHERE '.DB.'projects.pt_id = '.DB.'project_type.pt_id ORDER BY p_id ASC';
$result = pmdb::connect()->query($query);

//Create a PS_Pagination object
$pager = new PS_Pagination(pmdb::connect(),$query,10,10);

//The paginate() function returns a mysql result set
$query = $pager->paginate();

if($result->num_rows > 0) {
while($row = $query->fetch_object()) {
        
?>
        		
        <tr class="e-group">
        	<td>
        		<a href="<?php echo PM_URI ?>/projects/project.php?p_id=<?php echo $row->p_id; ?>"><?php echo $row->project_name; ?></a>
        		<a style="float:right;" href="<?php echo PM_URI ?>/projects/edit_project.php?p_id=<?php echo $row->p_id; ?>"><img src="<?php echo PM_URI ?>/images/project_edit.png" alt="Edit Project" title="Edit Project" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
        		<a style="float:right;" href="#" id="PID-<?php echo $row->p_id;?>" class="p_delete"><img src="<?php echo PM_URI ?>/images/group_delete.png" alt="Delete Project" title="Delete Project" /></a>
        	</td>
        </tr>

<?php
}
} else {
  echo "<p>There are no projects because no one has created one yet.</p>";
 }
?>
						</table>
						<!--Display the full navigation in one go-->
						<p>&nbsp;</p>

						<p align="center"><?php echo $pager->renderFullNav(); ?></p>

				</div>
				</div>


<?php

include(PM_DIR . 'pm-includes/footer.php');