<?php
/**
 * ProjectPress email project members
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

// Send Newsletter
if(!empty($_POST['sendMessage'])){
	$title = $_POST['title'];
	$message = $_POST['message'];
	if(!empty($title) && !empty($message)){
		$sql = "SELECT email FROM " . DB . "members, " . DB . "project_members WHERE pm_user = username and pp_id = '" . $_GET['p_id'] . "'";
		$counter = 1;
		if($result = pmdb::connect()->query($sql)){
			while($row = $result->fetch_object()){
				if(ceil($counter/$limit) == ($counter/$limit)) sleep($timeLimit);
				$to = $row->email;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; UTF-8' . "\r\n";
				$headers .= 'From: ' . User::instance()->get_name($_SESSION['username']) . ' <' . User::instance()->get_user_info($_SESSION['username'],'email') . '>' . "\r\n";
				$email = nl2br($message);
				$email .= "<br /><br /><em>You are receiving this email because you are a member of the " .get_project_meta($_GET['p_id'],'project_name')." project.</em>";
				pm_mail($to, $title, $email, $headers);
				$counter++;
			}
			pm_redirect('contact.php?p_id='.$_GET['p_id'].'&sent="'.$result->num_rows .'"');
		}
	} else {
		pm_redirect('contact.php?p_id='.$_GET['p_id'].'&message=2');
	}
}

?>
<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/mail-send.png" alt="" /><h1>Email <?php _e(get_project_meta($_GET['p_id'],'project_name')); ?> Members</h1>
</div>

	<?php _e( get_project_tabs() ); ?>

<div id="middle">
	<div id="groups-page">


<?php if(isset($_GET['sent'])): _e( "<p>Message sent successfully!<br />Sent to {$_GET['sent']} members</p>" ); else: ?>
	<p><?php _e( _( 'Use this form to send an email to everyone who is subscribed to this project.' ) ); ?></p>
	<form method="post" action="">
	<table cellpadding="0" cellspacing="5" align="center" width="100%">
		<tbody>
			<tr>
				<th><?php _e( _( 'Subject:' ) ); ?></th>
					<td><input class="forminput" id="title" name="title" type="text" size="30" /></td>
			</tr>
			<tr>
				<th><?php _e( _( 'Message:' ) ); ?></th>
					<td><textarea name="message" class="forminput" rows="10" cols="34"></textarea></td>
			</tr>
			<tr>
				<td><div align='left' style="margin-top:10px;"><input type="submit" name="sendMessage" class="sub_button" value="Send Message" /></div></td>
			</tr>
		</tbody>
	</table>
	</form>

	<?php endif; ?>


	</div>
</div>


<?php include(PM_DIR . 'pm-includes/footer.php');