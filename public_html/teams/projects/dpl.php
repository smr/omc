<?php
/**
 * ProjectPress delete project leader
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('edit_projects') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

pmdb::connect()->query("delete from ".DB."project_leaders where p_id = '".pmdb::connect()->escape($_REQUEST['p_id'])."' AND pl_user = '".pmdb::connect()->escape($_REQUEST['pl_user'])."' LIMIT 1");