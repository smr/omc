<?php 
/**
 * ProjectPress prints a list of projects
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<script type="text/javascript" src="<?php _e(get_javascript_directory_uri()); ?>/projects.js"></script>

<script type="text/javascript">
$(function() {
		//delete project
		$('a.p_delete').livequery("click", function(e){
			if(confirm('Are you sure you want to delete this project?')==false)
			return false;
			e.preventDefault();
			var parent  = $(this).parent();
			var p_id =  $(this).attr('id').replace('PID-','');	
			$.ajax({
				type: 'get',
				url: 'dp.php?p_id='+ p_id,
				data: '',
				beforeSend: function(){
				},

				success: function(){
					parent.fadeOut(200,function(){
						parent.remove();
					});
				}
			});
		});
});
</script>
    <?php
        if($current_user->hasPermission('access_admin') == true)
            $add_project = '<span id="header-link"><a href="'. PM_URI . '/projects/add_project.php">Add a Project</a></span>';
    ?>
				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/projects.png" alt="" /><h1>Projects <?php echo $add_project; ?></h1>
				</div>

				<div id="middle">
				<div id="groups">
						<table cellpadding="0" cellspacing="0" border="0" align="center">
							<th>Project</th>
							<th>Type</th>
							<th>Join/Leave</th>
							<th>&nbsp;</th>

<?php

$username = $_SESSION['username'];

$sql_p = "SELECT * FROM " . DB . "members, " . DB . "project_members WHERE username = pm_user AND username='".$username."'";
$result_p = pmdb::connect()->get_row($sql_p);
$is_assigned = ( $result_p->pp_id > 0 ? $result_p->pp_id : 0);
//echo 'Assigned: '.$is_assigned;

if($is_assigned == 0 || $current_user->hasPermission('access_admin') == true || $result_p->pp_id == 0)
    $sql = "SELECT * FROM " . DB . "projects, " . DB . "project_types WHERE ptype_id = pt_id AND p_id > 0 ORDER BY p_id ASC";
else
    $sql = "SELECT * FROM " . DB . "projects WHERE p_id = '".$is_assigned."' AND p_id > 0 ORDER BY p_id ASC";

unset($is_assigned);
$result = pmdb::connect()->query($sql);

//Create a PS_Pagination object
$pager = new PS_Pagination(pmdb::connect(),$sql,10,10);

//The paginate() function returns a mysql result set
$sql = $pager->paginate();

if($result->num_rows > 0) {
while($row = $result->fetch_object()) {


$username = $_SESSION['username'];

$user = pmdb::connect()->query("SELECT pm_user FROM " . DB . "project_members WHERE pp_id = '".$row->p_id."' AND pm_user = '".$username."'");
$user_joins = $user->num_rows;

?>
        		
        <tr>
        	<td><a href="<?php _e( PM_URI ); ?>/projects/project.php?p_id=<?php _e( $row->p_id ); ?>"><?php _e( $row->project_name ); ?></a></td> <td><?php _e( $row->project_type ); ?></td>
        
        	<td>
        	<span id="join-project-<?php _e( $row->p_id ); ?>">
			<?php
			if($user_joins){?>
				<!--<a href="javascript: void(0)" id="project_id<?php //_e( $row->p_id ); ?>" class="leave">Leave Project</a>-->
				<a href="javascript: void(0)">Cannot leave project</a>
			<?php }elseif(jury_test() == TRUE){?>
				<a href="javascript: void(0)" id="project_id<?php _e( $row->p_id ); ?>">Cannot join project</a>
			<?php }else{?>
				<a href="javascript: void(0)" id="project_id<?php _e( $row->p_id ); ?>" class="join">Join Project</a>
			<?php }?>

			</span>
        	</td>
        	
        	<?php if(is_session_set('username') == get_project_meta($row->p_id,'creator')) {
        		_e('<td><a style="float:right;" href="#" id="PID-' .$row->p_id . '" class="p_delete"><img src="' . PM_URI . '/images/project_delete.png" alt="Delete Project" title="Delete Project" /></a></td>');
        	}
			?>
        
        </tr>

<?php 
	}
} else {
  echo '<p>There are no projects yet.</p>';
}
?>
						</table>
						<!--Display the full navigation in one go-->
						<p>&nbsp;</p>

						<p align="center"><?php _e($pager->renderFullNav()); ?></p>

				</div>
				</div>


<?php

include(PM_DIR . 'pm-includes/footer.php');