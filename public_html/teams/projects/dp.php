<?php
/**
 * ProjectPress delete project
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

pmdb::connect()->delete( DB . 'projects', 'p_id = "' . $_REQUEST['p_id'] . '" AND creator = "' . $_SESSION['username'] . '"' );
pmdb::connect()->delete( DB . 'project_members', 'pp_id = "' . $_REQUEST['p_id'] . '"' );
pmdb::connect()->delete( DB . 'project_leaders', 'p_id = "' . $_REQUEST['p_id'] . '"' );
pmdb::connect()->delete( DB . 'project_forum_posts', 'pp_id = "' . $_REQUEST['p_id'] . '"' );
pmdb::connect()->delete( DB . 'project_forum_topics', 'pt_id = "' . $_REQUEST['p_id'] . '"' );
if( file_exists( PM_DIR . 'projects/uploads/' . $_REQUEST['p_id'] ) ) {
	$files = glob( PM_DIR . 'projects/uploads/' . $_REQUEST['p_id'] . '/*' );
		foreach( $files as $file ) unlink($file);
	rmdir( PM_DIR . 'projects/uploads/' . $_REQUEST['p_id'] );
}