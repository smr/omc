<?php
/**
 * ProjectPress delete project type
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access', true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// Checks if user is logged in; if not redirect to login page.
is_admin();

// Enable for error checking and troubleshooting.
# display_errors();

pmdb::connect()->delete( DB."project_types", "pt_id ='".$_REQUEST['pt_id']."'" );