<?php
/**
 * ProjectPress delete project leader
 *
 * @package ProjectPress
 * @since 2.0
 */
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

$limit = "";

	if(@$_REQUEST['val']) {
	$pl_user = pmdb::connect()->escape(strtolower($_REQUEST['pl_user']));
	$project_id = $_REQUEST['project_id'];

	//pmdb::connect()->query("INSERT INTO ".DB."project_leaders (p_id,pl_user) VALUES('".$project_id."','".$pl_user."')");
	pmdb::connect()->insert( DB . 'project_leaders', array( $project_id, $pl_user ), 'p_id,pl_user' );
}

//$result = pmdb::connect()->query("SELECT * FROM ".DB."project_leaders, ".DB."members WHERE pl_user = username AND p_id = '".$_GET['p_id']."' order by pl_user LIMIT 1");
$result = pmdb::connect()->select( DB . 'project_leaders, ' . DB . 'members', '*', 'pl_user = username AND p_id = "' . $_GET['p_id'] . '"', 'pl_user' );

while ($row = $result->fetch_array())
{?>

   <div class="show_lead">
   	<?php _e( get_user_avatar($row['pl_user'], User::instance()->get_user_info($row['pl_user'],'email')) ); ?>
	   <label style="float:left;" class="text">
	   <b><?php _e( '<a href="'.PM_URI.'/profile/profile.php?username='.$row['pl_user'].'">' . User::instance()->get_name($row['pl_user']) . '</a>' ); ?></b>
	   <br />
	   <?php _e( clickable_link($row['email']) ); ?>
	   </label>
	   <?php if($_SESSION['username'] == get_project_meta($_GET['p_id'], 'creator')) { ?>
	   <a href="#" class="pl_remove" ce="pl_user-<?php _e( $row['pl_user'] ); ?>" id="lead-<?php _e( $row['p_id'] ); ?>">x</a>
	   <?php } ?>
		<br clear="all" />
   </div>
<?php
}