<?php
/**
 * ProjectPress leave a project
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(dirname(__FILE__)) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();
	
	if($_REQUEST['projectId']) {
		pmdb::connect()->delete(DB . "project_members", "pm_user = '".$_SESSION['username']."' AND pp_id = '".$_REQUEST['projectId']."'");
	}