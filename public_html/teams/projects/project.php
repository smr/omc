<?php 
/**
 * ProjectPress a project's page
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

// Checks if user is logged in; if not redirect to login page.
if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }
	is_id_set( $_GET['p_id'], get_project_meta($_GET['p_id'],'p_id'), '/projects/list_projects.php' );

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$sql = pmdb::connect()->select( DB . 'projects', '*', 'p_id = "' . $_GET['p_id'] . '"', null ) or die(pmdb::connect()->is_error());
 
	while($row = $sql->fetch_array()) {
?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/projects.png" alt="" />
					<h1>
						<?php _e(get_project_meta($_GET['p_id'],'project_name')); ?>
						<?php
                            if(is_session_set('username') == get_project_meta($_GET['p_id'],'creator')) {
                                _e( '<span id="header-link"><a href="edit_project.php?p_id=' . $_GET['p_id'] . '">Edit Project</a></span>' );
                            }
                            elseif (admin_test() == TRUE) {
                                _e( '<span id="header-link"><a href="edit_project.php?p_id=' . $_GET['p_id'] . '">Edit Project</a></span>' );
                            }
                        ?>
					</h1>
				</div>
				
				<?php _e( get_project_tabs() ); ?>

				<div id="middle">
				<div id="groups-page">
					
					<?php do_action( 'project_info_top' ); ?>
					
					<table cellspacing="0" cellpadding="0">
						<tr class="list">
							<th scope="row"><span class="list-name"><?php _e( _( 'Description' ) ); ?></span></th>
							<td><?php _e( nl2br($row['project_description']) ); ?></td>
						</tr>

						<tr class="list">
							<th scope="row"><span class="list-name"><?php _e( _( 'Contact Email' ) ); ?></span></th>
							<td><a href="mailto:<?php _e( $row['contact_email'] ); ?>"><?php _e( $row['contact_email'] ); ?></a></td>
						</tr>
						
						<tr class="list">
							<th scope="row"><span class="list-name"><?php _e( _( 'Members' ) ); ?></span></th>
							<td>This project has <?php get_project_member_count(); ?> members</td>
						</tr>
					</table>
					
					<?php do_action( 'project_info_bottom' ); ?>

				</div>
				</div>
<?php } ?>


<?php include(PM_DIR . 'pm-includes/footer.php');