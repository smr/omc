<?php
/**
 * ProjectPress edit forum reply
 *
 * @package ProjectPress
 * @since 2.0
 */


// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e( _( 'Forum' ) ); ?></h1>
</div>

<?php
if (isset($_POST['edit']) && $_POST['edit'] == 'Submit') {
	
	$query = pmdb::connect()->update( DB . 'forum_posts', array( 'p_reply' => pmdb::connect()->escape($_POST['p_reply']) ), array('p_id', $_GET['p_id'], 'p_user', $_SESSION['username']), ' LIMIT 1' );
        
	if($query) {
		pm_redirect( 'view_topic.php?id=' . $_POST['tid'] );
	} else {		
	    _e( '<div class="error">' . PP::notices(5) . '</div>' );				
	}
}

$results = pmdb::connect()->get_row( "SELECT p_reply FROM ". DB ."forum_posts WHERE p_id='" . $_GET['p_id'] . "' AND p_user = '" . $_SESSION['username'] . "'" );

?>

<div id="middle">
<a href="<?php _e( PM_URI ); ?>/forum/forum.php"><?php _e( _( 'Go Back Forum Homepage' ) ); ?></a><br /><br />
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
		<tr>
		<form id="form2" name="form2" method="post" action="<?php _e( PM_URI ); ?>/forum/edit_reply.php?p_id=<?php _e( $_GET['p_id'] ); ?>">
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td valign="top"><strong><?php _e( _( 'Edit Reply' ) ); ?></strong><br />
						<textarea class="forminput" name="p_reply" cols="60" rows="20" id="p_reply" style="width:500px;"><?php _e( $results->p_reply ); ?></textarea></td>
					</tr>
					<tr>
						<td><br />
						<input type="hidden" name="tid" value="<?php _e( $_GET['tid'] ); ?>" />
						<input id="sub_button" type="submit" name="edit" value="Submit" /> <input id="sub_button" type="reset" name="Submit2" value="Reset" /></td>
					</tr>
				</table>
			</td>
		</form>
		</tr>
	</table>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');