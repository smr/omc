<?php
/**
 * ProjectPress delete forum reply
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e( _( 'Forum' ) ); ?></h1>
</div>

<div id="middle">			
<?php

if($_GET['p_id']) {

	$delete = pmdb::connect()->delete( DB . 'forum_posts', 'p_id = "' . $_GET['p_id'] . '" AND p_user = "' . $_SESSION['username'] . '"' );
		
	if($delete) {
		pm_redirect( 'view_topic.php?id=' . $_GET['tid'] );
	} else {
		_e( '<div class="error">' . PP::notices(4) . '</div>' );
	}
}
?>

</div>

<?php include(PM_DIR . 'pm-includes/footer.php');