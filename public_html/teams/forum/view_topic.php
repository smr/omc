<?php
/**
 * ProjectPress view forum topic
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

$sql = pmdb::connect()->select( DB . 'forum_topics, ' . DB . 'members', '*', 'id = "' . $_GET['id'] . '" AND t_user = username', null );

$row_t = $sql->fetch_object();
?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e( _( 'Forum' ) ); ?></h1>
				</div>
				
<div id="middle">
<h2><?php _e( _( 'Topic:' ) ); ?> <a href="<?php _e( PM_URI ); ?>/forum/view_topic.php?id=<?php _e( $_GET['id'] ); ?>"><?php _e( $row_t->topic ); ?></a></h2>
<a href="<?php _e( PM_URI ); ?>/forum/forum.php"><?php _e( _( 'Go Back Forum Homepage' ) ); ?></a><br /><br />
<table width="504" align="center" border="0" bgcolor="#DDD">
	<tr>
		<td align="left"><strong>Title:</strong> <?php _e( $row_t->topic ); ?> </td> <td align="right"><strong>Date: </strong><?php _e( $row_t->datetime ); ?></td>
	</tr>
</table>

<table width="504" border="0" align="center" cellpadding="0" cellspacing="0">

<tr>
<td vAlign="top" class="forum">
	<table width="484" border="0">
		<tr>
			<td width="100%" class="forum-topic"><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row_t->t_user ); ?>"><strong><? _e( $row_t->first_name ); ?> <?php _e( $row_t->last_name ); ?></strong></a> <?php _e( BBCode::parse($row_t->detail) ); ?></td>
		</tr>
		<?php if($row_t->signature != '') { ?>
		<tr>
			<td class="forum-signature">
				<div class="avatar">
				<?php _e( get_user_avatar($row_t->t_user,$row_t->email) ); ?>
				</div>
				
				<div class="signature">
				<?php _e( clickable_link(nl2br($row_t->signature)) ); ?>
				</div>
			</td>
		</tr>
		<?php } ?>
	</table>
</td>
</tr>
</table>

<br />
<?php 

$sql2 = pmdb::connect()->select( DB . 'forum_posts, ' . DB . 'members', '*', 'topic_id = "' . $_GET['id'] . '" AND p_user = username', 'p_id ASC' );

while($row_p = $sql2->fetch_object()){
?>
<table width="504" align="center" border="0" bgcolor="#C6E2FF">
	<tr>
		<td align="left"><strong><?php _e( _( 'Re:' ) ); ?></strong> <?php _e( $row_t->topic ); ?> </td> <td align="right"><strong><?php _e( _( 'Reply Date:' ) ); ?> </strong><?php _e( $row_p->p_datetime ); ?></td>
	</tr>
</table>

<table width="504" border="0" align="center" cellpadding="0" cellspacing="0" class="forum-1">

<tr>
<td vAlign="top" class="forum">
	<table width="484" border="0">
		<tr>
			<td width="100%"><a href="<?php _e( PM_URI ); ?>/profile/profile.php?username=<?php _e( $row_p->p_user ); ?>"><strong><?php _e( $row_p->first_name ); ?> <?php _e( $row_p->last_name ); ?></strong></a> <?php _e( BBCode::parse($row_p->p_reply) ); ?></td>
		</tr>
		<tr>
			<td class="forum-topic"><?php if($row_p->p_reply && $row_p->p_user == $_SESSION['username']) _e( '<a href="edit_reply.php?tid='.$_GET['id'].'&p_id='.$row_p->p_id.'">Edit</a> | ' ); ?> <?php if($row_p->p_reply && $row_p->p_user == $_SESSION['username']) _e( '<a href="delete.php?tid=' . $_GET['id'] . '&p_id='.$row_p->p_id.'">Delete</a>' ); ?></td>
		</tr>
		<?php if($row_p->signature != '') { ?>
		<tr>
			<td class="forum-signature">
				<div class="avatar">
				<?php _e( get_user_avatar($row_p->username,$row_p->email) ); ?>
				</div>
				
				<div class="signature">
				<?php _e( clickable_link(nl2br($row_p->signature)) ); ?>
				</div>
			</td>
		</tr>
		<?php } ?>
	</table>
</td>
</tr>
</table>

<?
}

$sql3 = pmdb::connect()->select( DB .'forum_topics', 'views', 'id = "' . $_GET['id'] . '"', null );

$rows = $sql3->fetch_object();
$views = $rows->views;

// count more value
$addview = $views+1;
$sql5 = pmdb::connect()->update( DB . 'forum_topics', array('views' => $addview), array('id', $_GET['id']) );
?>
<br />
<table width="484" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
<tr>
<form name="form1" method="post" action="<?php _e( PM_URI ); ?>/forum/reply.php?id=<?php _e( $_GET['id'] ); ?>">
<td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
<tr>
<td valign="top"><strong><?php _e( _( 'Quick Reply' ) ); ?></strong><br />
<textarea class="forminput" name="p_reply" cols="45" rows="3" id="p_reply" style="width:500px"></textarea></td>
</tr>
<tr>
<td><br />
<input name="id" type="hidden" value="<?php _e( $_GET['id'] ); ?>">
<input type="submit" name="Submit" value="Submit" id="sub_button"> <input type="reset" name="Submit2" value="Reset" id="sub_button"></td>
</tr>
</table>
</td>
</form>
</tr>
</table>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');