<?php 
/**
 * ProjectPress create forum topic
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_POST['newtopic'])) {
	$sql = pmdb::connect()->insert( DB . 'forum_topics', array( 'LAST_INSERT_ID()',
																pmdb::connect()->escape($_POST['topic']),
																pmdb::connect()->escape($_POST['detail']),
																$_SESSION['username'],
																pmdb::connect()->escape(date("d/m/y h:i:s"))
																),
																'id, 
																topic, 
																detail, 
																t_user, 
																datetime'
								  );
	$results = pmdb::connect()->get_row( "SELECT * FROM " . DB . "forum_topics WHERE topic = '" . $_POST['topic'] . "'" );
	
	if( $sql ) {
		pm_redirect( 'view_topic.php?id=' . $results->id );
	} else {
		$message = '<div class="error">' . PP::notices(3) . '</div>';
	}
}

	/**
	 * Creates a new template for the create a topic page.
	 */
	$newtopic = new Template(PM_DIR . "pm-includes/tpl/create_topic.tpl");
	$newtopic->set("pmurl", get_pm_option('siteurl'));
	$newtopic->set("message", $message);
	
	/**
	 * Outputs the page for create a topic.
	 */
	echo $newtopic->output();

include(PM_DIR . 'pm-includes/footer.php');