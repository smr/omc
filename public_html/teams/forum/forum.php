<?php 
/**
 * ProjectPress main forum
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

$sql = pmdb::connect()->select( DB . 'forum_topics', '*', null, 'id DESC' );

?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/comment.png" alt="" /><h1><?php _e( _( 'Forum' ) ); ?></h1>
				</div>

				<div id="middle">
<h2><?php _e( _( 'Forum Topics' ) ); ?></h2>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
<tr>
<td width="6%" align="center" bgcolor="#E6E6E6"><strong>#</strong></td>
<td width="53%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Topic' ) ); ?></strong></td>
<td width="15%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Views' ) ); ?></strong></td>
<td width="13%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Replies' ) ); ?></strong></td>
<td width="13%" align="center" bgcolor="#E6E6E6"><strong><?php _e( _( 'Date/Time' ) ); ?></strong></td>
</tr>

<?php
while($rows = $sql->fetch_object()) { // Start looping table row
$query = pmdb::connect()->select( DB . 'forum_posts', 'COUNT(p_id)', 'topic_id = "' . $rows->id . '"', null );
$r = $query->fetch_array();
?>
<tr>
<td bgcolor="#FFFFFF"><?php _e( $rows->id ); ?></td>
<td bgcolor="#FFFFFF"><a href="view_topic.php?id=<?php _e( $rows->id ); ?>"><?php _e( $rows->topic ); ?></a><br /></td>
<td align="center" bgcolor="#FFFFFF"><?php _e( $rows->views ); ?></td>
<td align="center" bgcolor="#FFFFFF"><?php _e( $r['COUNT(p_id)'] ); ?></td>
<td align="center" bgcolor="#FFFFFF"><?php _e( $rows->datetime ); ?></td>
</tr>

<?php
// Exit looping and close connection 
}
?>

<tr>
<td colspan="5" align="right" bgcolor="#E6E6E6"><a href="<?php _e( PM_URI ); ?>/forum/create_topic.php"><strong><?php _e( _( 'Create New Topic' ) ); ?></strong> </a></td>
</tr>
</table>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');