<?php
/**
 * ProjectPress forum reply
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();

// Get value of id that sent from hidden field 
$id = pmdb::connect()->escape($_POST['id']);

// Find highest answer number. 
$sql = "SELECT MAX(p_id) AS Maxp_id FROM ". DB ."forum_posts WHERE topic_id='$id'";
$result = pmdb::connect()->query($sql);
$rows = $result->fetch_object();

// add + 1 to highest answer number and keep it in variable name "$Max_id". if there no answer yet set it = 1 
if ($rows) {
$Max_id = $rows->Maxp_id+1;
} else {
$Max_id = 1;
} 

// Insert answer
if ($_POST['p_reply'] == "") { // Checks for blanks.
		exit("There was a field missing, please correct the form.");
	} else { 
	$sql2 = pmdb::connect()->insert( DB . 'forum_posts', array( 'p_id', $id, pmdb::connect()->escape($_SESSION['username']), pmdb::connect()->escape($_POST['p_reply']), date("m/d/y H:i:s") ) );
	
	send_forum_reply_email();
}

if($sql2){
	pm_redirect( 'view_topic.php?id=' . $id );

// If added new answer, add value +1 in reply column 
if ($_POST['p_reply'] == "") { // Checks for blanks.
		exit("There was a field missing, please correct the form.");
	} else {
//$sql3 = "UPDATE ". DB ."forum_topics SET replies = '$Max_id' WHERE id = '$id'";
$sql3 = pmdb::connect()->update( DB . 'forum_topics', array('replies' => $Max_id), array('id', $id) );
//$result3 = pmdb::connect()->query($sql3);
	}
} else {
echo "ERROR";
}