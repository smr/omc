<?php
/**
 * ProjectPress like a wall feed post
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
require_once(dirname(__FILE__) . '/config.inc.php');
require_once(PM_DIR . 'pm-includes/global.inc.php');
require_once(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/index.php'); }

// Enable for error checking and troubleshooting.
# display_errors();
	
	if($_REQUEST['postId']) {
		
		pmdb::connect()->query( "UPDATE ". DB ."wall_posts SET likes=likes+1 WHERE p_id= '".$_REQUEST['postId']."'" );
		pmdb::connect()->insert( DB . 'wall_likes_username', array($_SESSION['username'], $_REQUEST['postId']) );
		
		$total_likes = pmdb::connect()->select( DB . 'wall_posts', '*', 'p_id = "' . $_REQUEST['postId'] . '"', null );
		$likes = $total_likes->fetch_array();
		$likes = $likes['likes'];
	}
	echo $likes;