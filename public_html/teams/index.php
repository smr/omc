<?php 
/**
 * ProjectPress main page
 *
 * @package ProjectPress
 * @since 2.0
 */

 
define('INST_RUNSCRIPT', pathinfo(__FILE__, PATHINFO_BASENAME));
define('INST_BASEDIR',	 str_replace(INST_RUNSCRIPT, '', __FILE__));
define('INST_RUNFOLDER', 'installer/');
define('INST_RUNINSTALL', 'installer.php');
if (is_dir(INST_BASEDIR.INST_RUNFOLDER) && 
is_readable(INST_BASEDIR.INST_RUNFOLDER.INST_RUNINSTALL))
require(INST_BASEDIR.INST_RUNFOLDER.INST_RUNINSTALL);

/********************************************************************/

session_start();
define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('access_site') != true) { pm_redirect(PM_URI . '/pm-login.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/newspaper.png" alt="" /><h1><?php _e( _( 'Collab Feed' ) ); ?></h1>
				</div>

				<div id="wall-input" class="UIComposer_Box">
					<form action="" method="post">
					<fieldset>
					<textarea class="input" rows="2" cols="15" id="post" name="post"></textarea>
					<a id="shareButton" style="float:right" class="small button Detail"> <?php _e( _( 'Share' ) ); ?></a>
					</fieldset>
					</form>
				</div>
				
				<div class="wall">
				<div id="posting" align="center">
					<?php include(PM_DIR . 'posts.php'); ?>
				</div>
				</div> <!--Ends wall-->


<?php include(PM_DIR . 'pm-includes/footer.php');