<?php 
/**
 * ProjectPress plugin installer
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_FILES["zip_file"]["name"])) {
	$filename = $_FILES["zip_file"]["name"];
	$source = $_FILES["zip_file"]["tmp_name"];
	$type = $_FILES["zip_file"]["type"];
 
	$name = explode(".", $filename);
	$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
	foreach($accepted_types as $mime_type) {
		if($mime_type == $type) {
			$okay = true;
			break;
		} 
	}
 
	$continue = strtolower($name[1]) == 'zip' ? true : false;
	if(!$continue) {
		$message = '<div class="error">' . PP::notices(19) . '</div>';
	}
 
	$target_path = PM_DIR . "pm-content/plugins/".$filename;  // change this to the correct site path
	if(move_uploaded_file($source, $target_path)) {
		$zip = new ZipArchive();
		$x = $zip->open($target_path);
		if ($x === true) {
			$zip->extractTo(PM_DIR . "pm-content/plugins/"); // change this to the correct site path
			$zip->close();
 
			unlink($target_path);
		}
		$message = '<div class="success">' . PP::notices(20) . '</div>';
	} else {	
		$message = '<div class="error">' . PP::notices(21) . '</div>';
	}
}

	/**
	 * Creates a new template for the add member page.
	 */
	$plugininstall = new Template(PM_DIR . "pm-includes/tpl/plugin_install.tpl");
	$plugininstall->set("pmurl", get_pm_option('siteurl'));
	$plugininstall->set("message", $message);
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $plugininstall->output();

include(PM_DIR . 'pm-includes/footer.php');