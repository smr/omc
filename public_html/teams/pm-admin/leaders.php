<?php 
/**
 * ProjectPress list of project leaders
 *
 * @package ProjectPress
 * @since 3.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	// Checks if user is logged in; if not redirect to login page.
	if($current_user->hasPermission('edit_projects') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$project = pmdb::connect()->select( DB . 'projects', '*', null, 'p_id ASC' );
	$pl = pmdb::connect()->select( DB . 'members', '*', null, 'last_name ASC' );

	if(isset($_POST['dpl'])) {
		pmdb::connect()->delete( DB . "project_leaders", "pl_id = '" . pmdb::connect()->escape( $_POST['plID'] ) . "'");
	
	} else {

	if(isset($_POST['add_pl']))	{
		pmdb::connect()->insert( DB . "project_leaders", array( 'pl_id', pmdb::connect()->escape($_POST['p_id']), pmdb::connect()->escape($_POST['pl_user'])) );
	}

}

?>

		<div id="page-title">
			<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="" /><h1><?php _e( _( 'Project Leaders' ) ); ?></h1>
		</div>
		
		<div id="middle">
			<div id="groups-page">
			<p><?php _e( _( 'Use the form below to add project leaders to your available projects.' ) ); ?></p>
					<table> 
						<tr>
							<td>
								<form name="form" action="leaders.php" method="post">
								<select id="pl" name="pl_user">
								<option value=""><?php _e( _( '--- Member Name ---' ) ); ?></option>
								<?php while($r = $pl->fetch_object()) { ?>
								<option value="<?php _e( $r->username ); ?>"><?php _e( get_name($r->username) ); ?></option>
								<?php } ?>
								</select>

								<select id="project" name="p_id">
								<option value=""><?php _e( _( '--- Projects ---' ) ); ?></option>
								<?php while($lg = $project->fetch_object()) { ?>
								<option value="<?php _e( $lg->p_id ); ?>"><?php _e( $lg->project_name ); ?></option>
								<?php } ?>
								</select>
								<input type="submit" value="Add Project Leader" name="add_pl" id="sub_button" />
								</form>
							</td>
						</tr>
					</table>
					
					<table>
						<thead>
							<th><?php _e( _( 'Avatar' ) ); ?></th>
							<th><?php _e( _( 'Project Leader' ) ); ?></th>
							<th><?php _e( _( 'Project Name' ) ); ?></th>
							<th><?php _e( _( 'Delete' ) ); ?></th>
						</thead>
						<?php $query = pmdb::connect()->query("SELECT * FROM " . DB . "projects, " . DB . "project_leaders, " . DB . "members WHERE username = pl_user AND " . DB . "projects.p_id = " . DB . "project_leaders.p_id ORDER by pl_user ASC");
						while($row = $query->fetch_object()) { ?>
						<tbody>
						<tr>
							<td><?php _e( get_user_avatar($row->pl_user,$row->email,36) ); ?></td>
							<td><?php _e( get_name($row->pl_user) ); ?></td>
							<td><?php _e( $row->project_name ); ?></td>
							<td><form action="leaders.php" method="post">
									<input type="hidden" name="plID" value="<?php _e( $row->pl_id ); ?>" />
									<input style="float:right !important;" type="submit" value="Delete Project Leader" name="dpl" id="sub_button" />
								</form>
							</td>
						</tr>
						</tbody>
						<?php } ?>	
					</table>
				</div>
			</div>
	
<?php include(PM_DIR . 'pm-includes/footer.php');