<?php
/**
 * ProjectPress upgrade/update core script
 *
 * @package ProjectPress
 * @since 3.0.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(CURRENT_VERSION >= PP::upgrade(0)) {
	$update_message = '<p><b>You are using the latest version of ProjectPress.</b></p> <p>You are using the latest version, but you can do a re-install if necessary.</p>';
} elseif(CURRENT_VERSION < PP::upgrade(0)) {
	$update_message = '<p><b>A new version is available for upgrade.</b></p> <p>You may choose upgrade or re-install to upgrade your current system.</p>';
	$post_upgrade = '<input name="upgrade" type="submit" id="sub_button" value="Upgrade to ' . PP::upgrade(0) . '" />';
}

$file = PM_DIR . 'latest.zip';

if(isset($_POST['reinstall'])) {
	$fp = fopen($file, 'w');
    $client = curl_init( PP::upgrade(1) );
    curl_setopt($client, CURLOPT_FILE, $fp);

    curl_exec($client);
	
	curl_close($client);
    fclose($fp);

    $zip = new ZipArchive();  
    $x = $zip->open($file);  
    if($x === true) {  
        $zip->extractTo(PM_DIR);  
        $zip->close();

        unlink($file);  
		$message = '<div class="success">' . PP::notices(22) . '</div>';
    } else {
		$message = '<div class="error">' . PP::notices(23) . '</div>';
	}
} elseif(isset($_POST['upgrade'])) {
	$fp = fopen($file, 'w');
    $client = curl_init( PP::upgrade(2) );
    curl_setopt($client, CURLOPT_FILE, $fp);

    curl_exec($client);
	
	curl_close($client);
    fclose($fp);

    $zip = new ZipArchive();  
    $x = $zip->open($file);  
    if($x === true) {  
        $zip->extractTo(PM_DIR);  
        $zip->close();

        unlink($file);  
		$message = '<div class="success">' . PP::notices(24) . '</div>';
    } else {
		$message = '<div class="error">' . PP::notices(25) . '</div>';
	}
}
?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="" /><h1><?php _e( _( 'Upgrade ProjectPress' ) ); ?></h1>
</div>

<div id="middle">
	<?php _e( $message ); ?>
	<?php _e( $update_message ); ?>
<form enctype="multipart/form-data" method="post" action="">
	<input name="reinstall" type="submit" id="sub_button" value="Re-install" />
	<?php _e( $post_upgrade ); ?>
</form>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');