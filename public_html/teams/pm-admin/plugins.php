<?php 
/**
 * ProjectPress plugins
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

// Handle plugin administration pages
if( isset( $_GET['page'] ) && !empty( $_GET['page'] ) ) {
	plugin_admin_page( $_GET['page'] );
}

?>
				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/cog.png" alt="" /><h1><?php _e( _( 'Plugins' ) ); ?> <span id="header-link"><a href="<?php _e( PM_URI ); ?>/pm-admin/plugin_install.php"><?php _e( _( 'Add New' ) ); ?></a></span></h1>
				</div>

<div id="middle">
	<div id="plugins">
		<?php list_plugin_admin_pages(); ?>
	<table cellspacing="5" cellpadding="0"> 
		<thead> 
			<tr> 
				<th class="first" style="width:15%; padding-left:20px;"><?php _e( _( 'Plugin' ) ); ?></th>
				<th style="width:60%; padding-left:20px;"><?php _e( _( 'Description' ) ); ?></th>
				<th class="last" style="width:15%; padding-left:20px;"><?php _e( _( 'Action' ) ); ?></th> 
			</tr> 
		</thead> 
		<tbody>
							<?php // Set our plugins dir
							set_plugins_dir(PM_DIR . 'pm-content/plugins');
		
							// Now it's time to get all the plugins in the plugins dir,
							// getting, too, if are activated or not
							$plugins_list = get_plugins_specific();
		
							// Let's read the content of the array
							foreach($plugins_list as $plugin) {
							if($plugin['activated'] == true)
								echo '<tr class="activated">';
							else
								echo '<tr class="separated">';
				
							foreach($plugin as $index => $value) {
							// If the actual index specifies 'activated'
							if($index == 'file'){
							// Otherwise, get the plugin information
							$plugin_info = get_plugin_info($value);
					
					
					// Display the plugin information
					echo '<td>'.$plugin_info['Name'].'</td>';
					echo '<td>'.$plugin_info['Description'];
					echo '<br /><br />';
					echo 'Version '.$plugin_info['Version'];
					echo ' By <a href="'.$plugin_info['Author URI'].'">'.$plugin_info['Author'].'</a> ';
					echo ' | <a href="' .$plugin_info['Plugin URI'].'">Visit plugin site</a></td>';
					
					if($plugin_info['Activated'] == true) {
						echo '<td><a href="deact.php?id='.urlencode($value).'">Deactivate</a></td>';
					}else{
						echo '<td><a href="act.php?id='.urlencode($value).'">Activate</a></td>';
					}
				}
				
				echo '</tr>';
			}
		}?>
						</tbody>
						</table>
</div>
</div>
		
<?php include(PM_DIR . 'pm-includes/footer.php');