<?php 
/**
 * ProjectPress add new member form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

$hasher = new PasswordHash(8, FALSE);
$hash = $hasher->HashPassword(pmdb::connect()->escape($_POST['password']));

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_POST['submit'])) {

	$result = pmdb::connect()->insert( DB . 'members', array( 'LAST_INSERT_ID()',
															   pmdb::connect()->escape(md5($_POST['username'])),
															   strtolower(pmdb::connect()->escape($_POST['username'])),
															   pmdb::connect()->escape($_POST['first_name']),
															   pmdb::connect()->escape($_POST['last_name']),
															   pmdb::connect()->escape($_POST['email']),
															   $hash,
															   '1',
															   date("Y-m-d H:i:s")
															 ),
																'user_id,
																md5_id,
																username,
																first_name,
																last_name,
																email,
																password,
																active,
																date'
									 );
	
	pmdb::connect()->query("INSERT INTO " . DB . "user_roles (userID,roleID,addDate) VALUES (LAST_INSERT_ID(),'" . pmdb::connect()->escape($_POST['role']) . "',NOW())");

	if($result) {
		$amessage = '<div class="success">' . PP::notices(10) . '</div>';
	
	$sitetitle = get_pm_option('sitetitle');
	$siteurl = get_pm_option('siteurl');
	$email = $_POST['email'];
	$subject = "New OMC Hackathon Account";
	$headers  = "From: \"Member Account\" <auto-reply@$host>\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$message = 
	"Dear OMC Partipant, \n\n
	An account the 'Teams Platform' [i.e. http://thesponge.eu/teams] was created on your behalf.
	Below are your login details. We strongly recommend you to change it as soon as possible from your account details.

	User ID: $_POST[username]
	Email: $_POST[email] \n 
	Password: $_POST[password] \n

	$siteurl/pm-login.php

    The 'Teams Platform' is aiming to provide a common space for all project participants, where you can share,
    discuss and elaborate any kind of details relevant for your project. This is where Team Leaders can provide
    related project instructions and monitor progress.

    Additionally, this will be the place where you can find regular updates from the event organizers and further
    details, before they even reach other forums.

    A more detailed 'Teams Platform' description will follow after you will gain access to it, and you can also send
    your feedback and questions via the integrated contact form. More technical data will also be made public
    for those interested.

    Cheers,

    The OMC Team

	$host_upper
	______________________________________________________
	THIS IS AN AUTOMATED RESPONSE. 
	***DO NOT RESPOND TO THIS EMAIL****
";

	pm_mail($email,$subject,$message,$headers);
	
	} else {
		$amessage = '<div class="error">' . PP::notices(11) . '</div>';
	}
}

	/**
	 * Creates a new template for the add member page.
	 */
	$addmember = new Template(PM_DIR . "pm-includes/tpl/add_member.tpl");
	$addmember->set("pmurl", get_pm_option('siteurl'));
	$addmember->set("phpself", $PHP_SELF);
	$addmember->set("message", $amessage);
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $addmember->output();

include(PM_DIR . 'pm-includes/footer.php');