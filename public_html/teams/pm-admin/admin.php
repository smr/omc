<?php 
/**
 * ProjectPress admin panel
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	/**
	 * Creates a new template for the admin panel.
	 */
	$admin = new Template(PM_DIR . "pm-includes/tpl/admin.tpl");
	$admin->set("pmurl", get_pm_option('siteurl'));
	
	/**
	 * Outputs the admin panel page.
	 */
	echo $admin->output();

include(PM_DIR . 'pm-includes/footer.php');