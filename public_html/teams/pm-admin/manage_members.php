<?php 
/**
 * ProjectPress manage news
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="" /><h1><?php _e( _( 'Manage Members' ) ); ?> 
						<?php if($current_user->hasPermission('access_admin') == true) {
							_e( '<span id="header-link"><a href="' . PM_URI . '/pm-admin/add_member.php">Add Member</a></span></h1>' ); } ?>
				</div>

				<div id="middle">
				<div id="groups">
						<table cellpadding="0" cellspacing="0" border="0" align="center">
							<th><?php _e( ( 'Avatar' ) ); ?></th>
							<th><?php _e( ( 'Name' ) ); ?></th>
							<th><?php _e( ( 'Edit' ) ); ?></th>
							<th><?php _e( ( 'Active' ) ); ?></th>

<?php
$sql = "SELECT * FROM " . DB . "members ORDER BY user_id ASC";
$result = pmdb::connect()->query($sql);

//Create a PS_Pagination object
$pager = new PS_Pagination(pmdb::connect(),$sql,10,10);

//The paginate() function returns a mysql result set
$sql = $pager->paginate();

if($result->num_rows > 0) {
while($row = $result->fetch_object()) {
        
?>
        		
        <tr>
        	<td><?php _e( get_user_avatar($row->username,$row->email,25) ); ?></td> 
        	<td><?php _e( User::instance()->get_name($row->username) ); ?></td> 
        	<td><a href="<?php _e( PM_URI ); ?>/pm-admin/edit_member.php?username=<?php _e( $row->username ); ?>"><?php _e( _( 'Edit Member' ) ); ?></a></td>
        	<td><?php if($row->active == 1) _e( _( 'Yes' ) ); else _e( _( 'No' ) ); ?></td>
        </tr>

<?php 
	}
} else {
  _e( _( '<p>You haven\'t posted any new items.</p>' ) );
}
?>
						</table>
						<!--Display the full navigation in one go-->
						<p>&nbsp;</p>

						<p align="center"><?php _e( $pager->renderFullNav()) ; ?></p>

				</div>
				</div>


<?php include(PM_DIR . 'pm-includes/footer.php');