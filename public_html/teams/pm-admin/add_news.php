<?php 
/**
 * ProjectPress add news form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if(isset($_POST['submit'])) {

	$result = pmdb::connect()->insert( DB . "news", array( 'newsid',
														   date('Y-m-d H:i:s'),
														   pmdb::connect()->escape($_SESSION['username']),
														   pmdb::connect()->escape( $_POST['title'] ), 
														   pmdb::connect()->escape( $_POST['text'] ))
														  );
}

	/**
	 * Creates a new template for the add member page.
	 */
	$addnews = new Template(PM_DIR . "pm-includes/tpl/add_news.tpl");
	$addnews->set("pmurl", get_pm_option('siteurl'));
	$addnews->set("phpself", $PHP_SELF);
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $addnews->output();

include(PM_DIR . 'pm-includes/footer.php');