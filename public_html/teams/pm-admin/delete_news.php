<?php
/**
 * ProjectPress delete news
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

<div id="middle">
				<?php
				
				pmdb::connect()->delete( DB . "news", "newsid = '" .$_GET['id'] . "' AND author = '" . $_SESSION['username'] . "'" );

				if($_GET) {
				_e( '<div class="success">' . PP::notices(12) . '</div>' );
				} else {
				_e( '<div class="error">' . PP::notices(13) . '</div>' );
				}
				?>
</div>

<?php include(PM_DIR . 'pm-includes/footer.php');