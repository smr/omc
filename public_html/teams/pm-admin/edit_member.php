<?php
/**
 * ProjectPress edit member form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if (isset($_POST['manage']) && $_POST['manage'] == 'Submit') {
	
	$sql = pmdb::connect()->update( DB . 'members', array( 'first_name' => pmdb::connect()->escape($_POST['first_name']),
														   'last_name' => pmdb::connect()->escape($_POST['last_name']),
														   'twitter' => pmdb::connect()->escape($_POST['twitter']),
														   'facebook' => pmdb::connect()->escape($_POST['facebook']),
														   'privacy' => pmdb::connect()->escape($_POST['privacy']),
														   'active' => pmdb::connect()->escape($_POST['active'])
														 ),
													array( 'username', $_GET['username'] ), ' LIMIT 1'
								  );

	if( $sql ) {
		$message = '<div class="success">' . PP::notices(15) . '</div>';
	} else {
		$message = '<div class="error">' . PP::notices(16) . '</div>';
	}
}

	//$results = pmdb::connect()->get_row("SELECT * FROM ". DB ."user_roles WHERE userID = '".$_GET['username']."'");
	$results = pmdb::connect()->get_row("SELECT * FROM ". DB ."members WHERE username = '".$_GET['username']."'");

	/**
	 * Creates a new template for the add member page.
	 */
	$editmember = new Template(PM_DIR . "pm-includes/tpl/edit_member.tpl");
	$editmember->set("pmurl", get_pm_option('siteurl'));
	$editmember->set("message", $message);
	$editmember->set("username", $_GET['username']);
	$editmember->set("firstname", $results->first_name);
	$editmember->set("lastname", $results->last_name);
	$editmember->set("twitter", $results->twitter);
	$editmember->set("facebook", $results->facebook);
	
	if (defined('PRIVACY')) {
		if($results->privacy == 1) {
	$editmember->set("privacy", 'checked=checked');
		}
	}
	
	if($results->active == 1) {
	$editmember->set("active", 'checked=checked');
	}

	/**
	 * Outputs the page with add member form.
	 */
	echo $editmember->output();

include(PM_DIR . 'pm-includes/footer.php');