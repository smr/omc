<?php
/**
 * ProjectPress add user user type/tag
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

// Enable for error checking and troubleshooting.
# display_errors();

if($_POST) {
	pmdb::connect()->insert( DB . "user_user_types", array('uut_id', pmdb::connect()->escape($_POST['ut_id']), pmdb::connect()->escape($_POST['uut_user'])) );
} else { }