<?php 
/**
 * ProjectPress list user's user type/tag
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$uut = pmdb::connect()->select( DB . 'user_types', '*', null, 'usrt_id ASC' );
	$user = pmdb::connect()->select( DB . 'members', '*', null, 'last_name ASC' );

?>

<style type="text/css">	
#flash {margin-left:100px;}
ul#update {margin-top:25px;}
.user_ut {height:20px;border-bottom:#DEDEDE dashed 1px;margin-bottom:10px;}
</style>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="" /><h1><?php _e( _( 'User\'s User Type' ) ); ?></h1>
				</div>

				<div class="error" style="display:none"><?php _e( PP::notices(26) ); ?></div>

			<div id="middle">
			
			<p><?php _e( _( 'Choose a member/visitor and then choose a user type to add to the member/visitor. The user will appear 
			once you reload the page. User types are not used for role permissions but instead are used as identifiers 
			and for reporting purposes.' ) ); ?></p>
			<p>&nbsp;</p>
			
			<form name="form" action="" method="post">
			<select id="uut_user" name="uut_user">
			<option value=""></option>
			<?php while($r = $user->fetch_object()) { ?>
			<option value="<?php _e( $r->username ); ?>"><?php _e( $r->last_name . ', ' . $r->first_name ); ?></option>
			<?php } ?>
			</select>
			
			<select id="ut_id" name="ut_id">
			<option value=""></option>
			<?php while($ruut = $uut->fetch_object()) { ?>
			<option value="<?php _e( $ruut->usrt_id ); ?>"><?php _e( $ruut->user_type ); ?></option>
			<?php } ?>
			</select>
			<input type="submit" class="uut-submit" id="sub_button" value="Add">
			</form>
			
			<p>&nbsp;</p>
			
			<ul id="uuType" class="usertype">
			<?php
			$query = pmdb::connect()->select( DB . 'user_types, ' . DB . 'user_user_types, ' . DB . 'members', '*', 'username = uut_user AND usrt_id = ut_id', 'uut_user ASC' );
			while($row = $query->fetch_object())
			{
			?>
			<!--Displaying existing user types.-->
			<li class="user_ut"><?php _e( $row->first_name ); ?> <?php _e( $row->last_name ); ?> (<?php _e( $row->user_type ); ?>)&nbsp;&nbsp;&nbsp;&nbsp;<a style="float:right;" href="#" ut="uut_user-<?php _e( $row->uut_user ); ?>" id="UUTID-<?php _e( $row->ut_id );?>" class="uut_delete"><img src="<?php _e( PM_URI ); ?>/images/delete.png" alt="Delete" title="Delete" /></a></li>
			<?php
			}
			?>
			</ul>
			<div id="flash"></div>
			

			</div>


<?php include(PM_DIR . 'pm-includes/footer.php');