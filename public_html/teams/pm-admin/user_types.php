<?php 
/**
 * ProjectPress user types/tags
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

	$query = pmdb::connect()->select( DB . 'user_types', '*', null, 'user_type ASC' );

	/**
	 * Loop through the user types and create a template for each one.
	 * Because each user type is an array with key/value pairs defined, 
	 * we made our template so each key matches a tag in the template,
	 * allowing us to directly replace the values in the array.
	 * We save each template in the $userTypesTemplates array.
	 */
	while($row = $query->fetch_object()) {
	$userTypes = new Template(PM_DIR . "pm-includes/tpl/list_user_types_row.tpl");
	$userTypesTemplates[] = $userTypes;
	$userTypes->set("pmurl", get_pm_option('siteurl'));
    $userTypes->set("usertype", $row->user_type);
	$userTypes->set("utID", $row->usrt_id);
	}
	
	/**
	 * Merges all our user types templates into a single variable.
	 * This will allow us to use it in the main template.
	 */
	$userTypesContents = Template::merge($userTypesTemplates);
	
	/**
	 * Defines the main template and sets the user types content.
	 */
	$userTypesList  = new Template(PM_DIR . "pm-includes/tpl/list_user_types.tpl");
	$userTypesList->set("usertypes", $userTypesContents);
	
	/**
	 * Loads our layout template, setting its site url, site title
	 * and content.
	 */
	$layout = new Template(PM_DIR . "pm-includes/tpl/user_types.tpl");
	$layout->set("pmurl", get_pm_option('siteurl'));
	$layout->set("content", $userTypesList->output());
	
	/**
	 * Outputs the page with the list of user types.
	 */
	echo $layout->output();

include(PM_DIR . 'pm-includes/footer.php');