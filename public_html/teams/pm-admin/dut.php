<?php
/**
 * ProjectPress delete user type/tag
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

// Enable for error checking and troubleshooting.
# display_errors();

pmdb::connect()->delete( DB . "user_types", "usrt_id = '" . $_REQUEST['usrt_id'] . "'" );
pmdb::connect()->delete( DB . "user_user_types", "ut_id = '" . $_REQUEST['usrt_id'] . "'" );