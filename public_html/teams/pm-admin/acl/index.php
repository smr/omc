<?php 
/**
 * ProjectPress access control levels
 *
 * @package ProjectPress
 * @since 2.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

$pmACL = new ACL();
?>

		<div id="page-title">
			<h1 valign="middle"><?php _e( _e( 'Members' ) ); ?> :: <a href="./"><?php _e( _( 'User/Permissions' ) ); ?></a> | <a href="acl.php"><?php _e( _( 'Settings' ) ); ?></a></h1>
		</div>
					
	<div id="middle">
		<div id="directory">				
	<h2 class="box_head grad_colour"><?php _e( _( 'Permissions for user' ) ); ?> <?php _e( User::instance()->get_name($pmACL->getUser($_GET['userID'])) ); ?></h2>
	<? 
		$userACL = new ACL($_GET['userID']);
		$aPerms = $userACL->getAllPerms('full');
		foreach ($aPerms as $k => $v)
		{
			echo "<strong>" . $v['Name'] . ": </strong>";
			echo "<img src=\"../../images/";
			if ($userACL->hasPermission($v['Key']) === true)
			{
				echo "allow.png";
				$pVal = "Allow";
			} else {
				echo "deny.png";
				$pVal = "Deny";
			}
			echo "\" width=\"16\" height=\"16\" alt=\"$pVal\" /><br />";
		}
	?>	
					<table class="static">
						<thead> 
							<tr>
								<th><?php _e( _( 'Avatar' ) ); ?></th>
								<th><?php _e( _( 'Username' ) ); ?></th>
								<th><?php _e( _( 'Name' ) ); ?></th>
							</tr>
						</thead>
						
						<tbody>
						
    <h2 class="box_head grad_colour"><?php _e( _( 'Change User:' ) ); ?></h2>
    <? 
		$strSQL = pmdb::connect()->query("SELECT * FROM " . DB . "members ORDER BY last_name ASC") or die(pmdb::connect()->is_error());
		while ($row = $strSQL->fetch_assoc()) {
			_e( "<tr><td>" . get_user_avatar($row['username'],$row['email'],25) . "<td><a href=\"?userID=" . $row['user_id'] . "\">" . $row['username'] . "</a></td> <td>" . User::instance()->get_name($row['username']) . "</td></tr>" );
		}
    ?>		
    			</tbody></table>
    		</div>
	</div>
	
<?php include(PM_DIR . 'pm-includes/footer.php');