<?php 
/**
 * ProjectPress access control levels permissions
 *
 * @package ProjectPress
 * @since 2.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

$pmACL = new ACL();
if (isset($_POST['action']))
{
	switch($_POST['action'])
	{
		case 'savePerm':
			$strSQL = sprintf("REPLACE INTO `" . DB . "permissions` SET `ID` = %u, `permName` = '%s', `permKey` = '%s'",$_POST['permID'],$_POST['permName'],$_POST['permKey']);
			pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
		break;
		case 'delPerm':
			$strSQL = sprintf("DELETE FROM `" . DB . "permissions` WHERE `ID` = %u LIMIT 1",$_POST['permID']);
			pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
		break;
	}
	header("location: perms.php");
}

?>

				<div id="page-title">
					<h1 valign="middle"><?php _e('Access Control List System'); ?> :: <a href="./"><?php _e( _( 'User/Permissions' ) ); ?></a> | <a href="acl.php"><?php _e( _( 'Settings' ) ); ?></a></h1>
				</div>

				<div id="middle">	
					<table class="static"> 
						<tr><td>
							<?php if ($_GET['action'] == '') { ?>
    						<h2 class="box_head grad_colour"><?php _e( _( 'Select a Permission to Manage:' ) ); ?></h2>
        					<?php 
							$roles = $pmACL->getAllPerms('full');
							foreach ($roles as $k => $v)
							{
							echo "<a href=\"?action=perm&permID=" . $v['ID'] . "\">" . $v['Name'] . "</a><br />";
							}
							if (count($roles) < 1)
							{
							echo "No permissions yet.<br />";
							} ?>
        					<input type="submit" name="New" id="sub_button" value="New Permission" onclick="window.location='?action=perm'"></td>
    						</tr></table><?php } 
    						if ($_GET['action'] == 'perm') { 
							if ($_GET['permID'] == '') { 
							?> <h2 class="box_head grad_colour"><?php _e( _( 'New Permission:' ) ); ?></h2>
							 <?php } else { ?>
							<h2 class="box_head grad_colour"><?php _e( _( 'Manage Permission:' ) ); ?> (<?php _e( $pmACL->getPermNameFromID($_GET['permID']) ); ?>)</h2><? } ?>
							<form action="perms.php" method="post">
        					<tr><th><label for="permName"><?php _e( _( 'Name:' ) ); ?></label></th> <td><input type="text" name="permName" class="forminput" id="permName" value="<?php _e( $pmACL->getPermNameFromID($_GET['permID']) ); ?>" maxlength="30" /></td></tr>
            				<tr><th><label for="permKey"><?php _e( _( 'Key:' ) ); ?></label></th> <td><input type="text" name="permKey" class="forminput" id="permKey" value="<?php _e( $pmACL->getPermKeyFromID($_GET['permID']) ); ?>" maxlength="30" /></td></tr>
    						<tr><td>
    						<input type="hidden" name="action" value="savePerm" />
        					<input type="hidden" name="permID" value="<?php _e( $_GET['permID'] ); ?>" />
    						<input type="submit" id="sub_button" name="Submit" value="Submit"/>
    						</td></tr>
    						</form>

    						<form action="perms.php" method="post">
    						<tr><td>
         					<input type="hidden" name="action" value="delPerm" />
         					<input type="hidden" name="permID" value="<?php _e( $_GET['permID'] ); ?>" />
    						<input type="submit" name="Delete" id="sub_button" value="Delete" onclick="window.location='?action=perm'" />
    						</td></tr>
    						</form>

    						<form action="perms.php" method="post">
    						<tr><td>
    						<input type="submit" name="Cancel" id="sub_button" value="Cancel" onclick="window.location='?action=perm'">
    						</td></tr>
    						</form>
    					</table>
    						<?php } ?>
				</div><!--Ends middle-->
	
<?php include(PM_DIR . 'pm-includes/footer.php');