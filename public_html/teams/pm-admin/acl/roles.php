<?php 
/**
 * ProjectPress access control levels roles
 *
 * @package ProjectPress
 * @since 2.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

if(CURRENT_VERSION >= '3.0') {
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
} else {
	$link = mysqli_connect($myConfig['db_hostname'], $myConfig['db_username'], $myConfig['db_password'], $myConfig['db_database']);
}

$pmACL = new ACL();
if (isset($_POST['action']))
{
	switch($_POST['action'])
	{
		case 'saveRole':
			$strSQL = sprintf("REPLACE INTO `" . DB . "roles` SET `ID` = %u, `roleName` = '%s'",$_POST['roleID'],$_POST['roleName']);
			mysqli_query($link,$strSQL);
			if (mysqli_affected_rows($link) > 1)
			{
				$roleID = $_POST['roleID'];
			} else {
				$roleID = mysqli_insert_id();
			}
			foreach ($_POST as $k => $v)
			{
				if (substr($k,0,5) == "perm_")
				{
					$permID = str_replace("perm_","",$k);
					if ($v == 'X')
					{
						$strSQL = sprintf("DELETE FROM `" . DB . "role_perms` WHERE `roleID` = %u AND `permID` = %u",$roleID,$permID);
						pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
						continue;
					}
					$strSQL = sprintf("REPLACE INTO `" . DB . "role_perms` SET `roleID` = %u, `permID` = %u, `value` = %u, `addDate` = '%s'",$roleID,$permID,$v,date ("Y-m-d H:i:s"));
					pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
				}
			}
			header("location: roles.php");
		break;
		case 'delRole':
			$strSQL = sprintf("DELETE FROM `" . DB . "roles` WHERE `ID` = '%u' LIMIT 1",$_POST['roleID']);
			pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
			$strSQL = sprintf("DELETE FROM `" . DB . "user_roles` WHERE `roleID` = '%u'",$_POST['roleID']);
			pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
			$strSQL = sprintf("DELETE FROM `" . DB . "role_perms` WHERE `roleID` = '%u'",$_POST['roleID']);
			pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
			header("location: roles.php");
		break;
	}
}

?>

		<div id="page-title">
			<h1 valign="middle"><?php _e( _e( 'Access Control List System' ) ); ?> :: <a href="./"><?php _e( _( 'User/Permissions' ) ); ?></a> | <a href="acl.php"><?php _e( _( 'Settings' ) ); ?></a></h1>
		</div>

			<div id="middle">	
					<table class="static"> 
						<tr>
							<td><?php if ($_GET['action'] == '') { ?>
    	<h2 class="box_head grad_colour">Select a Role to Manage:</h2>
        <?php 
		$roles = $pmACL->getAllRoles('full');
		foreach ($roles as $k => $v) {
			_e( "<a href=\"?action=role&roleID=" . $v['ID'] . "\">" . $v['Name'] . "</a><br />" );
		}
		if (count($roles) < 1) {
			_e( "No roles yet.<br />" );
		} ?>
        <input type="submit" name="Delete" id="sub_button" value="New Roles" onclick="window.location='?action=role'" />
        </td>
						</tr>
				</table>
				 <table class="static">

								<?php } 
    if ($_GET['action'] == 'role') { 
		if ($_GET['roleID'] == '') { 
		?>
		<thead>
    	<h2><?php _e( _( 'New Role:' ) ); ?></h2>
        <?php } else { ?>
       
    	<h2 class="box_head grad_colour"><?php _e( _( 'Manage Role:' ) ); ?> (<?php _e( $pmACL->getRoleNameFromID($_GET['roleID']) ); ?>)</h2><? } ?>
        <form action="roles.php" method="post">
        	<label for="roleName"><?php _e( _( 'Name:' ) ); ?></label> <input type="text" name="roleName" class="forminput" id="roleName" value="<?php _e( $pmACL->getRoleNameFromID($_GET['roleID']) ); ?>" />
            <tr><th></th><th><?php _e( _( 'Allow' ) ); ?></th><th><?php _e( _( 'Deny' ) ); ?></th><th><?php _e( _( 'Ignore' ) ); ?></th></tr></thead>
            <?php 
            $rPerms = $pmACL->getRolePerms($_GET['roleID']);
            $aPerms = $pmACL->getAllPerms('full');
            foreach ($aPerms as $k => $v)
            {
                echo "<tbody><tr><td><label>" . $v['Name'] . "</label></td>";
                echo "<td><input type=\"radio\" name=\"perm_" . $v['ID'] . "\" id=\"perm_" . $v['ID'] . "_1\" value=\"1\"";
                if ($rPerms[$v['Key']]['value'] === true && $_GET['roleID'] != '') { echo " checked=\"checked\""; }
                echo " /></td>";
                echo "<td><input type=\"radio\" name=\"perm_" . $v['ID'] . "\" id=\"perm_" . $v['ID'] . "_0\" value=\"0\"";
                if ($rPerms[$v['Key']]['value'] != true && $_GET['roleID'] != '') { echo " checked=\"checked\""; }
                echo " /></td>";
				echo "<td><input type=\"radio\" name=\"perm_" . $v['ID'] . "\" id=\"perm_" . $v['ID'] . "_X\" value=\"X\"";
                if ($_GET['roleID'] == '' || !array_key_exists($v['Key'],$rPerms)) { echo " checked=\"checked\""; }
                echo " /></td>";
                echo "</tr></tbody>";
            }
        ?>
    	
    	<tr><td><input type="hidden" name="action" value="saveRole" />
        <input type="hidden" name="roleID" value="<?php _e( $_GET['roleID'] ); ?>" />
    	<input type="submit" name="Submit" id="sub_button" value="Submit" /></td>
    </form>
    <form action="roles.php" method="post">
         <td><input type="hidden" name="action" value="delRole" />
         <input type="hidden" name="roleID" value="<?php _e( $_GET['roleID'] ); ?>" />
    	<input type="submit" name="Delete" id="sub_button" value="Delete" /></td>
    </form>
    <form action="roles.php" method="post">
    	<td><input type="submit" name="Cancel" id="sub_button" value="Cancel" /></td></tr>
    </form>
    <?php } ?>
    </table>
	</div><!--Ends middle-->
	
<?php include(PM_DIR . 'pm-includes/footer.php');