<?php 
/**
 * ProjectPress access control levels
 *
 * @package ProjectPress
 * @since 2.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

?>

		<div id="page-title">
			<h1 valign="middle"><?php _e( _( 'Select an Admin Function' ) ); ?> :: <a href="./"><?php _e( _( 'User/Permissions' ) ); ?></a> | <a href="acl.php"><?php _e( _( 'Settings' ) ); ?></a></h1>
		</div>

			<div id="middle">	
					<table class="static">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						
						<tbody> 
							<tr>
								<td><a href="users.php"><?php _e( _( 'Manage Users' ) ); ?></a></td>
								<td><a href="roles.php"><?php _e( _( 'Manage Roles' ) ); ?></a></td>
								<td><a href="perms.php"><?php _e( _( 'Manage Permissions' ) ); ?></a></td>
							</tr>
						</tbody>
						</table>
			</div>
	
<?php include(PM_DIR . 'pm-includes/footer.php');