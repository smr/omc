<?php 
/**
 * ProjectPress access control levels users
 *
 * @package ProjectPress
 * @since 2.1
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(dirname(__FILE__))) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

$pmACL = new ACL();
if (isset($_POST['action']))
{
	switch($_POST['action'])
	{
		case 'saveRoles':
			$redir = "?action=user&userID=" . $_POST['userID'];
			foreach ($_POST as $k => $v)
			{
				if (substr($k,0,5) == "role_")
				{
					$roleID = str_replace("role_","",$k);
					if ($v == '0' || $v == 'x') {
						$strSQL = sprintf("DELETE FROM `" . DB . "user_roles` WHERE `userID` = %u AND `roleID` = %u",$_POST['userID'],$roleID);
					} else {
						$strSQL = sprintf("REPLACE INTO `" . DB . "user_roles` SET `userID` = %u, `roleID` = %u, `addDate` = '%s'",$_POST['userID'],$roleID,date ("Y-m-d H:i:s"));
					}
					pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
				}
			}
			
		break;
		case 'savePerms':
			$redir = "?action=user&userID=" . $_POST['userID'];
			foreach ($_POST as $k => $v)
			{
				if (substr($k,0,5) == "perm_")
				{
					$permID = str_replace("perm_","",$k);
					if ($v == 'x')
					{
						$strSQL = sprintf("DELETE FROM `" . DB . "user_perms` WHERE `userID` = %u AND `permID` = %u",$_POST['userID'],$permID);
					} else {
						$strSQL = sprintf("REPLACE INTO `". DB . "user_perms` SET `userID` = %u, `permID` = %u, `value` = %u, `addDate` = '%s'",$_POST['userID'],$permID,$v,date ("Y-m-d H:i:s"));
					}
					pmdb::connect()->query($strSQL) or die(pmdb::connect()->is_error());
				}
			}
		break;
	}
	header("location: users.php" . $redir);
}

?>

		<div id="page-title">
			<h1 valign="middle"><?php _e('Access Control List System'); ?> :: <a href="./"><?php _e( _( 'User/Permissions' ) ); ?></a> | <a href="acl.php"><?php _e( _( 'Settings' ) ); ?></a></h1>
		</div>

			<div id="middle">
					<?php if ($_GET['action'] == '' ) { ?>
    			<h2 class="box_head grad_colour"><?php _e( _( 'Select a User to Manage:' ) ); ?></h2>
    				<table class="static">
    					<thead> 
							<tr> 
								<th class="first"><?php _e( _( 'Avatar' ) ); ?></th> 
								<th><?php _e( _( 'Username' ) ); ?></th> 
								<th class="last"><?php _e( _( 'Name' ) ); ?></th> 
							</tr> 
						</thead>
						
						<tbody>
        <?php 
		$strSQL = pmdb::connect()->query("SELECT * FROM ". DB . "members ORDER BY last_name ASC") or die(pmdb::connect()->is_error());
		while ($row = $strSQL->fetch_assoc())
		{
			_e( "<tr><td>" . get_user_avatar($row['username'],$row['email'],25) . "</td> <td><a href=\"?action=user&userID=" . $row['user_id'] . "\">" . $row['username'] . "</a></td> <td>" . User::instance()->get_name($row['username']) . "</td></tr>" );
		}
		_e( '</tr></tbody></table>' );
    } ?>
    <?php
    if ($_GET['action'] == 'user' ) { 
		$userACL = new ACL($_GET['userID']);
	?>
	
	<h2 class="box_head grad_colour"><?php _e( _( 'Managing' ) ); ?> <?php _e( User::instance()->get_name($pmACL->getUser($_GET['userID'])) ); ?></h2>
	<table class="static"><tr><td>
        ... Some form to edit user info ...
        <h3><?php _e( _( 'Roles for user:' ) ); ?>   (<a href="users.php?action=roles&userID=<?php _e( $_GET['userID'] ); ?>"><?php _e( _( 'Manage Roles' ) ); ?></a>)</h3>
        <ul>
        <?php $roles = $userACL->getUserRoles();
		foreach ($roles as $k => $v)
		{
			_e( "<li>" . $userACL->getRoleNameFromID($v) . "</li>" );
		}
		?>
        </ul>
        <h3><?php _e( _( 'Permissions for user:' ) ); ?>   (<a href="users.php?action=perms&userID=<?php _e( $_GET['userID'] ); ?>"><?php _e( _( 'Manage Permissions' ) ); ?></a>)</h3>
        <ul>
        <?php $perms = $userACL->perms;
		foreach ($perms as $k => $v)
		{
			if ($v['value'] === false) { continue; }
			echo "<li>" . $v['Name'];
			if ($v['inheritted']) { echo "  (inheritted)"; }
			echo "</li>";
		}
		?>
        </ul>
     <?php } ?>
 	</td></tr></table>
     <?php if ($_GET['action'] == 'roles') { ?>
     <h2 class="box_head grad_colour"><?php _e( _( 'Manage User Roles:' ) ); ?> (<?php _e( User::instance()->get_name($pmACL->getUser($_GET['userID'])) ); ?>)</h2>
     <form action="users.php" method="post">
        <table class="static">
        <thead><tr><th></th><th><?php _e( _( 'Member' ) ); ?></th><th><?php _e( _( 'Not Member' ) ); ?></th></tr></thead><tbody></tbody>
        <?php 
		$roleACL = new ACL($_GET['userID']);
		$roles = $roleACL->getAllRoles('full');
        foreach ($roles as $k => $v)
        {
            echo "<tr><td><label>" . $v['Name'] . "</label></td>";
            echo "<td><input type=\"radio\" name=\"role_" . $v['ID'] . "\" id=\"role_" . $v['ID'] . "_1\" value=\"1\"";
            if ($roleACL->userHasRole($v['ID'])) { echo " checked=\"checked\""; }
            echo " /></td>";
            echo "<td><input type=\"radio\" name=\"role_" . $v['ID'] . "\" id=\"role_" . $v['ID'] . "_0\" value=\"0\"";
            if (!$roleACL->userHasRole($v['ID'])) { echo " checked=\"checked\""; }
            echo " /></td>";
            echo "</tr>";
        }
    ?>
        </tbody></table>
        <input type="hidden" name="action" value="saveRoles" />
        <input type="hidden" name="userID" value="<?php _e( $_GET['userID'] ); ?>" />
        <input type="submit" name="Submit" id="sub_button" value="Submit" />
    </form>
    <form action="users.php" method="post">
    	<input type="button" name="Cancel" value="Cancel" onclick="window.location='?action=user&userID=<?php _e( $_GET['userID'] ); ?>'" id="sub_button" />
    </form>
     <?php } ?>
     <?php
    if ($_GET['action'] == 'perms' ) { 
	?>
    	<h2 class="box_head grad_colour"><?php _e( _( 'Manage User Permissions:' ) ); ?> (<?php _e( User::instance()->get_name($pmACL->getUser($_GET['userID'])) ); ?>)</h2>
        <form action="users.php" method="post">
            <table border="0" cellpadding="5" cellspacing="0">
            <thead><tr><th></th><th></th></tr></thead><tbody>
            <?php 
			$userACL = new ACL($_GET['userID']);
            $rPerms = $userACL->perms;
            $aPerms = $userACL->getAllPerms('full');
            foreach ($aPerms as $k => $v)
            {
                echo "<tr><td>" . $v['Name'] . "</td>";
				echo "<td><select name=\"perm_" . $v['ID'] . "\">";
				echo "<option value=\"1\"";
				if ($userACL->hasPermission($v['Key']) && $rPerms[$v['Key']]['inheritted'] != true) { echo " selected=\"selected\""; }
				echo ">Allow</option>";
				echo "<option value=\"0\"";
				if ($rPerms[$v['Key']]['value'] === false && $rPerms[$v['Key']]['inheritted'] != true) { echo " selected=\"selected\""; }
				echo ">Deny</option>";
				echo "<option value=\"x\"";
				if ($rPerms[$v['Key']]['inheritted'] == true || !array_key_exists($v['Key'],$rPerms))
				{
					echo " selected=\"selected\"";
					if ($rPerms[$v['Key']]['value'] === true )
					{
						$iVal = '(Allow)';
					} else {
						$iVal = '(Deny)';
					}
				}
				echo ">Inherit $iVal</option>";
                echo "</select></td></tr>";
            }
        ?>
    	</tbody></table>
    	<input type="hidden" name="action" value="savePerms" />
        <input type="hidden" name="userID" value="<?php _e( $_GET['userID'] ); ?>" />
    	<input type="submit" name="Submit" id="sub_button" value="Submit" />
    </form>
    <form action="users.php" method="post">
    	<input type="submit" name="Cancel" value="Cancel" onclick="window.location='?action=user&userID=<?php _e( $_GET['userID'] ); ?>'" id="sub_button" />
    </form>
    <?php } ?>

	</div><!--Ends middle-->
	
<?php include(PM_DIR . 'pm-includes/footer.php');