<?php
/**
 * ProjectPress download spreadsheet with members data.
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

// Enable for error checking and troubleshooting.
//display_errors();

   $csv_output = "Last Name,First Name,Email,DOB,Member"; 
   $csv_output .= "\n"; 
   $result = pmdb::connect()->query("SELECT * FROM ". DB ."members"); 

   while($row = $result->fetch_object()) { 
       $csv_output .= "$row->last_name,$row->first_name,$row->email,$row->dob,$row->member\n";
       } 

   header("Content-type: application/octet-stream");
   header("Content-disposition: attachment; filename=" . date("Y-m-d") . ".csv");
   print $csv_output;
   exit;