<?php 
/**
 * ProjectPress edit news form
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

if (isset($_POST['news']) && $_POST['news'] == 'Submit') {
	 
	 $sql = pmdb::connect()->update( DB . 'news', array( 'title' => pmdb::connect()->escape($_POST['title']), 'text' => pmdb::connect()->escape($_POST['text']) ), array( 'newsid', $_GET['id']), ' LIMIT 1' );
	 
	 if( $sql ) {
		$message = '<div class="success">' . PP::notices(17) . '</div>';
	} else {
		$message = '<div class="error">' . PP::notices(18) . '</div>';
	}
}

	$results = pmdb::connect()->get_row( "SELECT * FROM " . DB . "news WHERE newsid = '" . $_GET['id'] . "'" );

	/**
	 * Creates a new template for the edit news page.
	 */
	$editnews = new Template(PM_DIR . "pm-includes/tpl/edit_news.tpl");
	$editnews->set("pmurl", get_pm_option('siteurl'));
	$editnews->set("message", $message);
	$editnews->set("id", $_GET['id']);
	$editnews->set("newstitle", $results->title);
	$editnews->set("text", $results->text);
	
	/**
	 * Outputs the page with edit news form.
	 */
	echo $editnews->output();

include(PM_DIR . 'pm-includes/footer.php');