<?php 
/**
 * ProjectPress site settings
 *
 * @package ProjectPress
 * @since 2.1.4
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

if ( $_POST ) {
	$options = array( 'sitetitle', 'siteurl', 'admin_email', 'skin', 'enable_registration', 'contact_form_message', 'enable_query' );
	$checked_options = array( 'enable_registration' => 'no', 'enable_query' => 'no' );
	
	foreach ( $checked_options as $option_name => $option_unchecked_value ) {
		if ( ! isset( $_POST[$option_name] ) )
			$_POST[$option_name] = $option_unchecked_value;
	}
	foreach ( $options as $option_name ) {
		if ( ! isset($_POST[$option_name]) )
			continue;
		$value = pmdb::connect()->escape( $_POST[$option_name] );
		update_pm_option( $option_name, $value );
	}
	
	// Update more options here
	do_action( 'update_pm_options' );
	pm_redirect('settings.php');
	exit();
}
?>

<div id="page-title">
	<img src="<?php _e( PM_URI ); ?>/images/admin.png" alt="" /><h1><?php _e( _( 'Site Settings' ) ); ?></h1>
</div>
				
<div id="middle">
	<form name="options" method="post" action="settings.php">
		<table cellspacing="5" cellpadding="0">
			<tr><td><?php _e( _( 'Site Title:' ) ); ?></td> <td><input class="forminput" name="sitetitle" size="40" value="<?php _e(get_pm_option('sitetitle')); ?>" maxlength="255"></td></tr>
			<tr><td><?php _e( _( 'Site URI:' ) ); ?></td> <td><input class="forminput" name="siteurl" size="40" value="<?php _e(get_pm_option('siteurl')); ?>" maxlength="255"></td></tr>
			<tr><td><?php _e( _( 'Admin Email:' ) ); ?></td> <td><input class="forminput" name="admin_email" size="40" value="<?php _e(get_pm_option('admin_email')); ?>" maxlength="255"></td></tr>
			<tr><td><?php _e( _( 'Choose Theme:' ) ); ?></td> 
				<td>
					<select name="skin">
						<?php if ($skin_dir = opendir(get_pm_option('sitepath') . 'css/skins/')) {
    							while (false !== ($sDIR = readdir($skin_dir))) {
        							if ($sDIR != "." && $sDIR != "..") {
        								$skin = str_replace(".css", "", $sDIR);
							?>
						<option value="<?php _e( $skin ); ?>"<?php _e( selected($skin, get_pm_option('skin')) ); ?>><?php _e( ucfirst($skin)); ?></option>
						<?php 			}
									}
									closedir($skin_dir);
								} ?>
					</select>
				</td>
			</tr>
			<tr><td><?php _e( _( 'Enable Registration:' ) ); ?></td> <td><input type="checkbox" class="forminput" name="enable_registration" value="yes" <?php _e( checked( get_pm_option( 'enable_registration' ), 'yes' ) ); ?> /></td></tr>
			<tr><td><?php _e( _( 'Contact Form Message:' ) ); ?></td> <td><textarea class="forminput" id="contact_form" type="text" name="contact_form_message"><?php _e( get_pm_option('contact_form_message') ); ?></textarea></td></tr>
			<!-- <tr><td>Enable Query:</td> <td><input type="checkbox" class="forminput" name="enable_query" value="yes" <?php _e( checked( get_pm_option( 'enable_query' ), 'yes' ) ); ?> /></td></tr> -->
			<tr><td></td> <td><input type="submit" id="sub_button" name="submit" value="Update Options"></td>
      	</table>
      	<?php do_action( 'pm_options' ); // Add more options here ?>
     </form>
</div><!--Ends middle-->

<?php include(PM_DIR . 'pm-includes/footer.php');