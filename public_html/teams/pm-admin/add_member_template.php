<?php

$message = "
Dear OMC Partipant,

An account the 'Teams Platform' [i.e. http://thesponge.eu/teams] was created on your behalf.
Below are your login details. We strongly recommend you to change it as soon as possible from your account details.

User ID: ".$_POST[username]."\n
Email: ".$_POST[email]."\n
Password: ".$_POST[password]."\n

".$siteurl/pm-login.php."

The 'Teams Platform' is aiming to provide a common space for all project participants, where you can share,
discuss and elaborate any kind of details relevant for your project. This is where Team Leaders can provide
related project instructions and monitor progress.

Additionally, this will be the place where you can find regular updates from the event organizers and further
details, before they even reach other forums.

A more detailed 'Teams Platform' description will follow after you will gain access to it, and you can also send
your feedback and questions via the integrated contact form. More technical data will also be made public
for those interested.

Cheers,

The OMC Team

______________________________________________________
THIS IS AN AUTOMATED RESPONSE.
***DO NOT RESPOND TO THIS EMAIL****

";