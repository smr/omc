<?php
/* Deactivate page.
	This page is designed for activating
	an specified plugin
*/

// Starts the session.
session_start();
define('access',true);
// Require the principal class and config
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

// Enable for error checking and troubleshooting.
//display_errors();

set_plugins_dir(PM_DIR . 'pm-content/plugins');


// Now activate the specified plugin
activate_plugin($_GET['id']);


// Return to the principal page
pm_redirect(PM_URI . '/pm-admin/plugins.php');