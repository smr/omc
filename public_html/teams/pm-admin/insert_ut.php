<?php
/**
 * ProjectPress add user type/tag
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
include(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

// Enable for error checking and troubleshooting.
//display_errors();

if($_POST) {
	pmdb::connect()->insert( DB . "user_types", array('usrt_id', pmdb::connect()->escape($_POST['user_type']), date('Y-m-d H:i:s')) );
} else { }

?>

<li class="user_type"><?php _e( $_POST['user_type'] ); ?>&nbsp;&nbsp;&nbsp;&nbsp;<a style="float: right;" href="#" id="UTID-<?php _e( $row['usrt_id'] ); ?>" class="ut_delete"><img src="<?php _e( PM_URI ); ?>/images/delete.png" alt="Delete" title="Delete" /></a></li>