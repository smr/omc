<?php 
/**
 * ProjectPress admin info page
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

// User is logged in and is an admin.
is_admin();

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
//display_errors();

	/**
	 * Creates a new template for the add member page.
	 */
	$info = new Template(PM_DIR . "pm-includes/tpl/info.tpl");
	$info->set("pmurl", get_pm_option('siteurl'));
	
	/**
	 * Outputs the page with add member form.
	 */
	echo $info->output();

include(PM_DIR . 'pm-includes/footer.php');