<?php 
/**
 * ProjectPress manage news
 *
 * @package ProjectPress
 * @since 2.0
 */

// Starts the session.
session_start();
define('access',true);
include(dirname(dirname(__FILE__)) . '/config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

	userAccess::is_user_logged_in();

	if($current_user->hasPermission('add_news') != true) { pm_redirect(PM_URI . '/index.php'); }

include(PM_DIR . 'pm-includes/header.php');

// Enable for error checking and troubleshooting.
# display_errors();

?>

				<div id="page-title">
					<img src="<?php _e( PM_URI ); ?>/images/newspaper.png" alt="" /><h1><?php _e( _( 'Manage News' ) ); ?>
						<?php if($current_user->hasPermission('add_news') == true) {
							_e( '<span id="header-link"><a href="' . PM_URI . '/pm-admin/add_news.php">' . _( 'Add News' ) . '</a></span></h1>' ); } ?>
				</div>

				<div id="middle">
				<div id="groups">
						<table cellpadding="0" cellspacing="0" border="0" align="center">
							<th><?php _e( _( 'Title' ) ); ?></th>
							<th><?php _e( _( 'Edit' ) ); ?></th>
							<th><?php _e( _( 'Delete' ) ); ?></th>

<?php
$sql = "SELECT * FROM " . DB . "news ORDER BY newsid ASC";
$result = pmdb::connect()->query($sql);

//Create a PS_Pagination object
$pager = new PS_Pagination(pmdb::connect(),$sql,10,10);

//The paginate() function returns a mysql result set
$sql = $pager->paginate();

if($result->num_rows > 0) {
while($row = $result->fetch_object()) {
        
?>
        		
        <tr>
        	<td><a href="<?php _e( PM_URI ); ?>/news/news.php?id=<?php _e( $row->newsid ); ?>"><?php _e( $row->title ); ?></a></td> 
        	<td><a href="<?php _e( PM_URI ); ?>/pm-admin/edit_news.php?id=<?php _e( $row->newsid ); ?>"><?php _e( _( 'Edit' ) ); ?></a></td> 
        	<td><a href="<?php _e( PM_URI ); ?>/pm-admin/delete_news.php?id=<?php _e( $row->newsid ); ?>"><?php _e( _( 'Delete' ) ); ?></a></td>
        </tr>

<?php 
	}
} else {
  _e( _( '<p>You haven\'t posted any new items.</p>' ) );
}
?>
						</table>
						<!--Display the full navigation in one go-->
						<p>&nbsp;</p>

						<p align="center"><?php _e( $pager->renderFullNav() ); ?></p>

				</div>
				</div>


<?php include(PM_DIR . 'pm-includes/footer.php');