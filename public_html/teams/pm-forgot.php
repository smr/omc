<?php 
session_start(); //Starts the session.
define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');
if($_SESSION['logged'] == 1) //User is already logged in.
	pm_redirect(PM_URI . '/index.php'); //Goes to main page.


$hasher = new PasswordHash(8, FALSE);


/******************* ACTIVATION BY FORM**************************/
if ($_POST['doReset']=='Reset') {
$err = array();
$msg = array();

do_action( 'pm_forgot_form_script' );

foreach($_POST as $key => $value) {
	$data[$key] = pmdb::connect()->escape($value);
}
if(!is_valid_email($data['email'])) {
$err[] = "ERROR - Please enter a valid email"; 
}

$email = $data['email'];

//check if activ code and user is valid as precaution
$rs_check = pmdb::connect()->query("SELECT user_id FROM " . DB . "members WHERE email='$email'"); 
$num = $rs_check->num_rows;
  // Match row found with more than 1 results  - the user is authenticated. 
    if ( $num <= 0 ) { 
	$err[] = "Error - Sorry no such account exists or registered.";
	//header("Location: pm-forgot.php?msg=$msg");
	//exit();
	}


if(empty($err)) {

$new_pwd = generate_user_password();
$pwd_reset = $hasher->HashPassword(pmdb::connect()->escape($new_pwd));

$rs_activ = pmdb::connect()->query("UPDATE " . DB . "members SET password='$pwd_reset' WHERE 
						 email='$email'");
						 
$host  = $_SERVER['HTTP_HOST'];
$host_upper = strtoupper($host);						 
						 
//send email

$message = 
"Here are your new password details ...\n
User Email: $email \n
Passwd: $new_pwd \n

Thank You

Administrator
$host_upper
______________________________________________________
THIS IS AN AUTOMATED RESPONSE. 
***DO NOT RESPOND TO THIS EMAIL****
";

	$headers  = "From: \"ProjectPress Reset Password\" <auto-reply@$host>\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	
	pm_mail($email,"Reset Password",$message,$headers);					 
						 
$msg[] = "Your account password has been reset and a new password has been sent to your email address.";						 
						 
//$msg = urlencode();
//header("Location: pm-forgot.php?msg=$msg");						 
//exit();
 }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title><?php _e( _( 'Reset Password :: ' ) . get_pm_option( 'sitetitle' ) ); ?></title>
<link href="<?php _e( PM_URI ) ?>/css/form.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery.validate.js"></script>
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
  <script>
  $(document).ready(function(){
    $("#actForm").validate();
  });
  </script>
</head>

<body>

<!-- Box Start-->
<div id="box_bg">
	<div id="pp-logo"><a href="<?php _e( PM_URI ); ?>"><?php _e( pp_logo( 80 ) ); ?></a></div>
<div id="content">
	<h1>Reset Password</h1>
	
	<?php do_action( 'pm_forgot_form_top' ); ?>
	
	<p>&nbsp;</p>
	<p> 
        <?php
	  /******************** ERROR MESSAGES*************************************************
	  This code is to show error messages 
	  **************************************************************************/
	if(!empty($err))  {
	   echo "<p>&nbsp;</p><div class=\"msg\">";
	  foreach ($err as $e) {
	    echo "* $e <br>";
	    }
	  echo "</div>";	
	   }
	   if(!empty($msg))  {
	    echo "<p>&nbsp;</p><div class=\"msg\">" . $msg[0] . "</div>";

	   }
	  /******************************* END ********************************/	  
	  ?>
      </p>
      <p>&nbsp;</p>
      <p>If you have forgot the account password, you can <strong>reset your password</strong> 
        and a new password will be sent to your email address.</p>
	
	<!-- Login Fields -->
	<form action="pm-forgot.php" method="post" name="actForm" id="actForm" >
	<div id="login">
	<input type="text" name='email' onblur="if(this.value=='')this.value='Email';" onfocus="if(this.value=='Email')this.value='';" value="Email" class="login user"/>
	</div>
	
	<br />
	
	<?php do_action( 'pm_forgot_form_bottom' ); ?>
	
	<!-- Green Button -->
	<div class="button input"><input class="button green" type="submit" value="Reset" name="doReset" /></div>
	
	<!-- Checkbox -->
	<div class="checkbox">
	<li>
	<fieldset>
	<![if !IE | (gte IE 8)]><legend id="title2" class="desc"></legend><![endif]>
	<!--[if lt IE 8]><label id="title2" class="desc"></label><![endif]-->
	<div></div>
	</fieldset>
	</li>
	</div>
	</form>

</div>
</div>

<!-- Text Under Box -->
<div id="bottom_text">
	<?php if(get_pm_option('enable_registration') == 'yes') { ?>
	<?php _e( _( 'Don\'t have an account?' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-register.php"><?php _e( _( 'Sign Up' ) ); ?></a><br/>
	<?php } ?>
	<?php _e( _( 'Have an account?' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-login.php"><?php _e( _( 'Login' ) ); ?></a>
</div>

</body>
</html>