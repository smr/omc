﻿<?php
/**
 * ProjectPress main page
 *
 * @package ProjectPress
 * @since 2.0
 */

define('access',true);
include('config.inc.php');
include(PM_DIR . 'pm-includes/global.inc.php');
require(PM_DIR . 'pm-includes/functions.php');

if(get_pm_option('enable_registration') != 'yes') { pm_die( '<a href="pm-login.php">Click here</a> to login.' , 'Registration disabled'); die(); }

$hasher = new PasswordHash(8, FALSE);
	
$err = array();
					 
if($_POST['doRegister'] == 'Register') { 
/******************* Filtering/Sanitizing Input *****************************
This code filters harmful script code and escapes data of all POST data
from the user submitted form.
*****************************************************************/
foreach($_POST as $key => $value) {
	$data[$key] = $value;
}

do_action( 'pm_reg_form_script' );

/************************ SERVER SIDE VALIDATION **************************************/

/********** This validation is useful if javascript is disabled in the browswer ***/

if(empty($data['first_name']) || strlen($data['first_name']) < 4 || $data['first_name'] == 'First Name') {
$err[] = "ERROR - Invalid First Name. Please enter at least 3 or more characters for your first name";
//header("Location: pm-register.php?msg=$err");
//exit();
}

if(empty($data['last_name']) || strlen($data['last_name']) < 4 || $data['last_name'] == 'Last Name') {
$err[] = "ERROR - Invalid Last Name. Please enter at least 3 or more characters for your last name";
//header("Location: pm-register.php?msg=$err");
//exit();
}

// Validate Email
if(!is_valid_email($data['email'])) {
$err[] = "ERROR - Invalid email address.";
//header("Location: pm-register.php?msg=$err");
//exit();
}

// Validate User Name
if (!is_valid_username($data['username']) || $data['username'] == 'Username') {
$err[] = "ERROR - Invalid username. It can contain alphabet, number and underscore.";
//header("Location: pm-register.php?msg=$err");
//exit();
}

// Check User Passwords
if (!is_valid_password($data['pwd'],$data['pwd2'])) {
$err[] = "ERROR - Invalid Password or mismatch. Enter 5 chars or more";
//header("Location: pm-register.php?msg=$err");
//exit();
}
	  
$user_ip = $_SERVER['REMOTE_ADDR'];

// stores sha1 of password
$phpass = pm_hash_password(pmdb::connect()->escape($data['pwd']));

// Automatically collects the hostname or domain  like example.com) 
$host  = $_SERVER['HTTP_HOST'];
$host_upper = strtoupper($host);
$path   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

// Generates activation code simple 4 digit number
$activ_code = rand(1000,9999);

$email = $data['email'];
$username = strtolower($data['username']);

/************ USER EMAIL CHECK ************************************
This code does a second check on the server side if the email already exists. It 
queries the database and if it has any existing email it throws user email already exists
*******************************************************************/

$rs_duplicate = pmdb::connect()->query("select count(*) as total from " . DB . "members where email='$email' OR username='$username'");
list($total) = $rs_duplicate->fetch_row();

if ($total > 0) {
$err[] = "ERROR - The username/email already exists. Please try again with different username and email.";
//header("Location: pm-register.php?msg=$err");
//exit();
}
/***************************************************************************/

if(empty($err)) {

$sql_insert = "INSERT INTO " . DB . "members
  			(`user_id`,`username`,`first_name`,`last_name`,`email`,`password`,`users_ip`,`activation_code`,`date`
			)
		    VALUES
		    (LAST_INSERT_ID(),'$username','$data[first_name]','$data[last_name]','$email','$phpass','$user_ip','$activ_code',NOW()
			)
			";
			
pmdb::connect()->query($sql_insert);  
$md5_id = md5($user_id);
pmdb::connect()->query("UPDATE " . DB . "members SET md5_id='$md5_id' WHERE user_id = LAST_INSERT_ID()");
pmdb::connect()->query("INSERT INTO " . DB . "user_roles (userID,roleID,addDate) VALUES (LAST_INSERT_ID(),'2',NOW())");
//	echo "<h3>Thank You</h3> We received your submission.";

if($user_registration)  {
$a_link = "
*****ACTIVATION LINK*****\n
http://$host$path/pm-activate.php?user=$md5_id&activ_code=$activ_code
"; 
} else {
$a_link = 
"Your account is *PENDING APPROVAL* and will be soon activated the administrator.
";
}

$message = 
"Hello \n
Thank you for registering with us. Here are your login details...\n

User ID: $username
Email: $email \n 
Password: $data[pwd] \n

$a_link

Thank You

Administrator
$host_upper
______________________________________________________
THIS IS AN AUTOMATED RESPONSE. 
***DO NOT RESPOND TO THIS EMAIL****
";

$headers  = "From: \"ProjectPress Member Registration\" <auto-reply@$host>\r\n";
$headers .= "X-Mailer: PHP/" . phpversion();

pm_mail($email,"Login Details",$message,$headers);

  pm_redirect(PM_URI . '/thankyou.php');
	 
	 } 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<title><?php _e( _( 'Registration Form :: ' ) . get_pm_option( 'sitetitle' ) ); ?></title>
<link href="<?php _e( PM_URI ) ?>/css/form.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php _e( PM_URI ); ?>/js/jquery.validate.js"></script>

  <script>
  $(document).ready(function(){
    $.validator.addMethod("username", function(value, element) {
        return this.optional(element) || /^[a-z0-9\_]+$/i.test(value);
    }, "Username must contain only letters, numbers, or underscore.");

    $("#regForm").validate();
  });
  </script>
</head>

<body>
<!-- Box Start-->
<div id="box_bg">
	<div id="pp-logo"><a href="<?php _e( PM_URI ); ?>"><?php _e( pp_logo( 80 ) ); ?></a></div>
<div id="content">
	<h1>Create An Account</h1>
	
	<?php do_action( 'pm_reg_form_top' ); ?>
	
	<p>&nbsp;</p>
	<?php 
	 if (isset($_GET['done'])) { ?>
	  <h2>Thank you</h2> Your registration is now complete and you can <a href="pm-login.php">login here</a>";
	 <?php exit();
	  }
	?>
	
	<?php	
	 if(!empty($err))  {
	   echo "<p>&nbsp;</p><div class=\"msg\">";
	  foreach ($err as $e) {
	    echo "* $e <br>";
	    }
	  echo "</div>";	
	   }
	 ?>

	<form name="regForm" id="regForm" action="pm-register.php" method="POST">
	<div id="login">
	<input type="text" name="first_name" onblur="if(this.value=='')this.value='First Name';" onfocus="if(this.value=='First Name')this.value='';" value="First Name" class="login user" />
	<input type="text" name="last_name" onblur="if(this.value=='')this.value='Last Name';" onfocus="if(this.value=='Last Name')this.value='';" value="Last Name" class="login user" />
	<input type="text" name="email" onblur="if(this.value=='')this.value='Email';" onfocus="if(this.value=='Email')this.value='';" value="Email" class="login user" />
	<input type="text" name='username' onblur="if(this.value=='')this.value='Username';" onfocus="if(this.value=='Username')this.value='';" value="Username" class="login user"/>
	<input type="text" name='pwd' value="Password" onblur="if(this.value=='')this.value='Password';" onfocus="if(this.value=='Password')this.value='';" class="login password"/>
	<input type='text' name='pwd2' value='Confirm Password'  onblur="if(this.value=='')this.value='Confirm Password';" onfocus="if(this.value=='' || this.value == 'Confirm Password') {this.value='';this.type='password'}" class="login password"/><br />
	<?php do_action( 'pm_reg_form_fields' ); ?>
	</div>
	
	<?php do_action( 'pm_reg_form_bottom' ); ?>
	
	<!-- Green Button -->
	<div class="button input"><input class="button green" type="submit" value="Register" name="doRegister" /></div>
	
	<!-- Checkbox -->
	<div class="checkbox">
	<li>
	<fieldset>
	<![if !IE | (gte IE 8)]><legend id="title2" class="desc"></legend><![endif]>
	<!--[if lt IE 8]><label id="title2" class="desc"></label><![endif]-->
	<div></div>
	</fieldset>
	</li>
	</div>
	</form>

</div>
</div>

<!-- Text Under Box -->
<div id="bottom_text">
	<?php _e( _( 'Have an account?' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-login.php"><?php _e( _( 'Login' ) ); ?></a><br/>
	<?php _e( _( 'Remind' ) ); ?> <a href="<?php _e( PM_URI ); ?>/pm-forgot.php"><?php _e( _( 'Password' ) ); ?></a>
</div>

</body>
</html>