<?
//print_r($_POST);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php echo $CAD->SEO->DISPLAY(); ?>
    <title>
        <?php
        if(strlen($CAD->SEO->tag_TITLE)>1)
            echo $CAD->SEO->tag_TITLE;
        else
            echo "Open Media Challenge Hackathon " . $CAD->name;
        ?>
    </title>

    <base href="http://<?php echo $_SERVER['SERVER_NAME']; ?>" />

    <link rel="stylesheet" href="/fw/GENERAL/css/style.css"  />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="description" content="<?php echo $CAD->desc; ?>" />
    <meta name="keywords" content="open media challenge, hackathon, knight mozilla, hack event, romania hackathon" />

    <?php echo $CAD->INC_css; ?>

    <link rel="icon" type="image/png" href="/fw/GENERAL/css/img/favico.png" />

    <script type="text/javascript"  src="/fw/GENERAL/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript"  src="/fw/GENERAL/js/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script type="text/javascript"  src="/fw/GENERAL/js/jquery-ui-1.8.19.custom/js/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript"  src="/fw/GENERAL/js/jquery.ui.nestedSortable.js" type="text/javascript"></script>


</head>



<body >
    <?php echo $CAD->ctrlDISPLAY('TOOLbar'); ?>
    <input type='hidden' name='current_idT' value='<?php echo $CAD->idT; ?>'  />
    <input type='hidden' name='current_idC' value='<?php echo $CAD->idC; ?>'  />
    <input type='hidden' name='lang'        value='<?php echo $CAD->lang; ?>' />
    <input type='hidden' name='lang2'       value='<?php echo $CAD->lang2; ?>'/>
    <div id='header'>

        <img class="hackathon"    src="/fw/GENERAL/css/img/hackathon.png"    alt="hackathon" />
        <img class="sponge"       src="/fw/GENERAL/css/img/sponge.png"       alt="Sponge" />
        <img class="baloon_north" src="/fw/GENERAL/css/img/baloon_north.png" alt="Come hack with us!" />
        <div id="menu"><?php echo $CAD->MENUhorizontal->DISPLAY(); ?></div>
    </div>




