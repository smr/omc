<div id="footer">

    <div class="partner">
   		<a href="http://mozillaopennews.org/" target="_blank">
               <img src="/fw/GENERAL/css/img/km_logo.png" alt="Mozilla Open News" class="kmoz" />
   		</a>
   	</div>
    <div class="partner">
   		<a href="http://crji.org" target="_blank">
               <img src="/fw/GENERAL/css/img/crji_logo.png" alt="CRJI" class="crji" />
   		</a>
   	</div>
    <div class="partner">
   		<a href="http://geo-spatial.org" target="_blank">
               <img src="/fw/GENERAL/css/img/geospatial.png" alt="geospatial.org" class="geospatial" />
   		</a>
   	</div>
    <div class="partner">
   		<a href="http://ceata.org" target="_blank">
               <img src="/fw/GENERAL/css/img/ceata_logo.png" alt="Ceata" class="ceata" />
   		</a>
   	</div>
    <div class="partner">
   		<a href="http://rosedu.org" target="_blank">
               <img src="/fw/GENERAL/css/img/rosedu_logo.png" alt="ROSEdu" class="rosedu" />
   		</a>
   	</div>
    <div class="partner">
   		<a href="http://apti.ro" target="_blank">
               <img src="/fw/GENERAL/css/img/apti_logo.png" alt="ApTI" class="apti" />
   		</a>
   	</div>
	<!-- <div class="fancybar"> </div> -->
    <a id='submit' href='/entry-list'></a>
</div>
<div id="license">
    <p>
        This web application is Free Software
        (<a href="http://www.gnu.org/licenses/agpl-3.0.html" target="_blank">AGPLv3+</a>) built by
        Victor Nițu (<a href="http://ceata.org" target="_blank">Ceata</a>)
        and Ioana Cristea (<a href="http://serenitymedia.ro" target="_blank">Serenity Media</a>)
        <br />
        Source code is <a href="http://git.ceata.org/cgit.cgi/open-media-challenge.git/" target="_blank">available</a> and waiting for contributions
    </p>
</div>
<?php echo $CAD->INC_js; ?>