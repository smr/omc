/**
 * -- SETTINGS--
 *
 *  EDsel => EDname = new SELoptions(Array_ro,Array_en);
 *  extras => EDname_extra = functionName(id);
 */
role      = new SELoptions(Array('coder','journalist','activist','designer'),Array('coder','journalist','activist','designer'),'');  // selname.getHTMLoptions();
roleM     = new SELoptions({0:'coder',1:'journalist',2:'activist',3:'designer'},{0:'coder',1:'journalist',2:'activist',3:'designer'},'');  // selname.getHTMLoptions();
statusmod = new SELoptions(Array('unmoderated','rejected','approved'),Array('unmoderated','rejected','approved'),'');  // selname.getHTMLoptions();
teamM     = new SELoptions(Array('1','2'),
       {'1':'RICJn',
        '2':'Media Watch',
        '3':'Publishing solution',
        '4':'EU money for agriculture',
        '5':'Political colours of Romania interactively mapped',
        '6':'Public records database',
        '7':'Map of arrests',
        '8':'Mapping the stray dogs in Bucharest',
        '9':'Harta Politicii',
        '999': 'Unaffiliated participants'
    },'id');  // selname.getHTMLoptions();

proc = new SELoptions(Array('0','10','20','30','40','50','60','70','80','90','100'),Array('0','10','20','30','40','50','60','70','80','90','100'),'');
