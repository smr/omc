function tables(i){
    //$('.submission_table').hide();
    $('div[id='+i+'_submission_en]  table').fadeToggle(300);
    $('div[id='+i+'_submission_en]  div.submission_top').toggleClass('submission_top_selected');
    $('div[id='+i+'_submission_en]  div.submission_top a.preview').toggleClass('preview_up');
}

function hideTables(id){
    //$('.submission_table').hide();
    $('div[id$=_submission_en]  table').slideUp('slow');
    $('div[id$=_submission_en]  div.submission_top').removeClass('submission_top_selected');
}

function showTables(i){
    //$('.submission_table').hide();
    $('div[id='+i+'_submission_en]  table').slideDown('slow');
    $('div[id='+i+'_submission_en]  div.submission_top').toggleClass('submission_top_selected');
}

